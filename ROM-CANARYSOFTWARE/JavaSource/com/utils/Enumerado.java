package com.utils;

public class Enumerado {
	
	public static enum Estado_enum {
		Activo,
		Inactivo
	}
	
	public static enum TipoDocumento_enum {
		CI,
		Pasaporte,
		Carta_Ciudadania,
		Otro
	}
	
	public static enum TipoUsuario_enum {
		Administrador,
		Ciudadano_comun,
		Validador,
		ONG,
		Organismo_Privado,
		Otro
	}
	
}
