package com.capa2_businessLogic.services;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.ObsCaracteristicasDTO;
import com.capa1_presentacion.bean.ObservacionesDTO;
import com.capa3_persistence.dao.CaracteristicasBean;
import com.capa3_persistence.dao.LocalidadesBean;
import com.capa3_persistence.dao.ObsCaracteristicasBean;
import com.capa3_persistence.dao.ObservacionesBean;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Criticidad;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Localidades;
import com.capa3_persistence.entities.ObsCaracteristicas;
import com.capa3_persistence.entities.ObsCaracteristicasPK;
import com.capa3_persistence.entities.Observaciones;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class ObsCaracteristicasBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	ObservacionesBean observacionesPersistenciaDAO;
	@EJB
	CaracteristicasBean caracteristicasPersistenciaDAO;
	@EJB
	ObsCaracteristicasBean obsCaracteristicasPersistenciaDAO;

	public ObsCaracteristicas obsCaractDTOtoObsCaracteristicas(ObsCaracteristicasDTO e) throws ServiciosException {
		System.out.println ("en obsCaractDTOtoObsCaracteristicas en ObsCaracteristicasBO e.toString() " + e.toString());
		ObsCaracteristicas obsCaracteristicas=new ObsCaracteristicas();
		obsCaracteristicas.setNumerico(e.getNumero());
		obsCaracteristicas.setTexto(e.getTexto());
		ObsCaracteristicasPK obsCarPK = new ObsCaracteristicasPK(e.getIdObs(),e.getIdCaract());
		obsCaracteristicas.setId(obsCarPK);
		System.out.println ("en obsCaractDTOtoObsCaracteristicas en ObsCaracteristicasBO e.getIdObs() " + e.getIdObs());
		Observaciones obs = new Observaciones();
		//obs = observacionesPersistenciaDAO.buscarPorId(e.getIdObs());
		obs.setIdObservacion(e.getIdObs());
		//Caracteristicas caract = (Caracteristicas) caracteristicasPersistenciaDAO.buscarCodigo(e.getIdCaract());
		Caracteristicas caract = new Caracteristicas();
		caract.setIdCaracteristica(e.getIdCaract());
		obsCaracteristicas.setCaracteristica(caract);
		obsCaracteristicas.setObservacion(obs);
		return obsCaracteristicas;
	}	
	
	
	public ObsCaracteristicasDTO obsCaracteristicasToObsCaractDTO(ObsCaracteristicas e) throws ServiciosException {
		ObsCaracteristicasDTO obsCaracteristicasDTO=new ObsCaracteristicasDTO();
		obsCaracteristicasDTO.setNumero(e.getNumerico());
		obsCaracteristicasDTO.setTexto(e.getTexto());
		obsCaracteristicasDTO.setIdCaract(e.getCaracteristica().getIdCaracteristica());
		obsCaracteristicasDTO.setIdCaractNom(e.getCaracteristica().getNombre());
		obsCaracteristicasDTO.setIdObs(e.getObservacion().getIdObservacion());
		obsCaracteristicasDTO.setIdObsNom(e.getObservacion().getDescripcion());
		return obsCaracteristicasDTO;
	}

	public void borrarObsCaract(long idObs, long idCaract) {
		obsCaracteristicasPersistenciaDAO.borrarObsCaract(idObs, idCaract);
	}
	
}
