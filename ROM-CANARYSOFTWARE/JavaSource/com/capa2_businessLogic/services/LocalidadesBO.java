package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa1_presentacion.bean.LocalidadesDTO;
import com.capa3_persistence.dao.DepartamentosBean;
import com.capa3_persistence.dao.FenomenosBean;
import com.capa3_persistence.dao.LocalidadesBean;
import com.capa3_persistence.entities.Departamentos;
import com.capa3_persistence.entities.Estado;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Localidades;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class LocalidadesBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	LocalidadesBean localidadesPersistenciaDAO;
	DepartamentosBean dptoPersistenciaDAO;
	
	public void actualizarLocalidad(LocalidadesDTO localidadSeleccionado) throws PersistenciaException, ServiciosException   {
		boolean e = localidadesPersistenciaDAO.modificarLocalidad(toLocalidades(localidadSeleccionado));
	}
	
	public LocalidadesDTO agregarLocalidad(LocalidadesDTO localidadSeleccionado) throws PersistenciaException, ServiciosException   {
		Localidades e = localidadesPersistenciaDAO.agregarLocalidad(toLocalidades(localidadSeleccionado));
		return fromLocalidades(e);
	}
	
	public void agregarLocalidadRest(LocalidadesDTO localidad) throws PersistenciaException, ServiciosException   {		
		localidadesPersistenciaDAO.crearLocalidad(localidadDTOtoLocalidades(localidad));		
	}
	
	public LocalidadesDTO buscarLocalidad(Long id) throws ServiciosException {
		Localidades e = localidadesPersistenciaDAO.buscarCodigo(id);
		return fromLocalidades(e);
	}
	
	public List<LocalidadesDTO> obtenerLocalidadesTodos() throws PersistenciaException   {	
		List<LocalidadesDTO> ret = new ArrayList<LocalidadesDTO>();
		List<Localidades> loc = localidadesPersistenciaDAO.obtenerTodos();
		
		for(Localidades e: loc) {
			ret.add(fromLocalidades(e));
		}
		return ret;
	}
	
	public List<LocalidadesDTO> seleccionarLocalidades() throws PersistenciaException {
		
		List<LocalidadesDTO> ret = new ArrayList<LocalidadesDTO>();
		List<Localidades> emps = localidadesPersistenciaDAO.obtenerTodos();
		
		for(Localidades e: emps) {
			ret.add(fromLocalidades(e));
		}
		return ret;
	}
	
	public List<LocalidadesDTO> seleccionarPorNombre(String nombre) throws PersistenciaException {
		List<LocalidadesDTO> ret = new ArrayList<LocalidadesDTO>();
		List<Localidades> emps = localidadesPersistenciaDAO.obtenerTodosPorNombre(nombre);
		
		for(Localidades e: emps) {
			ret.add(fromLocalidades(e));
		}
		return ret;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	public Localidades localidadDTOtoLocalidades(LocalidadesDTO e) throws ServiciosException {
		Localidades localidad=new Localidades();
		localidad.setIdLocalidad(e.getId());
		localidad.setNombre(e.getNombre());
		Departamentos dpto = dptoPersistenciaDAO.buscarCodigo(e.getDepartamento());
		localidad.setIdDepartamento(dpto);
		return localidad;
	}

	
	public LocalidadesDTO fromLocalidades(Localidades e) {
		LocalidadesDTO localidad=new LocalidadesDTO();
		localidad.setId(e.getIdLocalidad());
		localidad.setNombre(e.getNombre());
		localidad.setDepartamento(e.getIdDepartamento().getIdDepartamento());
		localidad.setDepartamentoNombre(e.getIdDepartamento().getNombre());
		return localidad;
	}
	
	public Localidades toLocalidades(LocalidadesDTO e) throws ServiciosException {
		Localidades localidad=new Localidades();
		localidad.setIdLocalidad(e.getId());
		localidad.setNombre(e.getNombre());
		Departamentos dpto = dptoPersistenciaDAO.buscarCodigo(e.getDepartamento());
		localidad.setIdDepartamento(dpto);
		return localidad;
	}

}
