package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa1_presentacion.bean.ObsCaracteristicasDTO;
import com.capa1_presentacion.bean.ObservacionesDTO;
import com.capa2_businessLogic.login.UsuarioLogueado;
import com.capa3_persistence.dao.CriticidadBean;
import com.capa3_persistence.dao.FenomenosBean;
import com.capa3_persistence.dao.LocalidadesBean;
import com.capa3_persistence.dao.ObsCaracteristicasBean;
import com.capa3_persistence.dao.ObservacionesBean;
import com.capa3_persistence.dao.UsuariosBean;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Criticidad;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Localidades;
import com.capa3_persistence.entities.ObsCaracteristicas;
import com.capa3_persistence.entities.Observaciones;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;


@Stateless
@LocalBean
public class ObservacionesBO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	ObservacionesBean observacionesPersistenciaDAO;
	@EJB
	FenomenosBean fenomenosPersistenciaDAO;
	@EJB
	LocalidadesBean localidadesPersistenciaDAO;
	@EJB
	UsuariosBean usuariosPersistenciaDAO;
	@EJB
	CriticidadBean criticidadPersistenciaDAO;
	@EJB
	ObsCaracteristicasBean obsCaracPersistenciaDAO;
	
	public void borrarObservacion(ObservacionesDTO observacionSeleccionado) throws PersistenciaException, ServiciosException   {
						
		if (toObservaciones(observacionSeleccionado).getIdUsuarioVali() != null) {
			throw new ServiciosException("No es posible eliminar la observacion porque ya ha sido validada");
		}
		
		try {
			obsCaracPersistenciaDAO.borrarPorObs(toObservaciones(observacionSeleccionado).getIdObservacion());
			observacionesPersistenciaDAO.borrarObservacion(toObservaciones(observacionSeleccionado).getIdObservacion());
		} catch (ServiciosException e) {
		    	System.out.println ("No se pudo eliminar la observacion");
		    }	
	}
	
	public ObservacionesDTO agregarObservacion(ObservacionesDTO observacionSeleccionado) throws PersistenciaException, ServiciosException   {
		observacionSeleccionado.setIdUsuarioAlta(UsuarioLogueado.getUsuarioLogueado().getUsuarioLogueadoDTO().getId());
		Observaciones e = observacionesPersistenciaDAO.agregarObservacion(toObservaciones(observacionSeleccionado));
		return fromObservaciones(e);
		
	}
	
	public void actualizarObservacion(ObservacionesDTO observacionSeleccionado) throws PersistenciaException, ServiciosException   {
		boolean e = observacionesPersistenciaDAO.modificarObservacion(toObservaciones(observacionSeleccionado));
	}
	
	public ObservacionesDTO agregarObservacionRest(ObservacionesDTO observacion) throws PersistenciaException, ServiciosException   {
		Observaciones obs = observacionesPersistenciaDAO.agregarObservacion(observacionDTOtoObservacionesREST(observacion));	
		ObservacionesDTO obsDTO = new ObservacionesDTO();
		obsDTO.setIdObservacion(obs.getIdObservacion());
		return obsDTO;
	}
	public ObservacionesDTO buscarObservacion(Long id) throws ServiciosException {
		Observaciones e = observacionesPersistenciaDAO.buscarCodigo(id);
		return fromObservaciones(e);
	}
	public ObservacionesDTO fromObservaciones(Observaciones e) {
		ObservacionesDTO observacion=new ObservacionesDTO();
		observacion.setIdObservacion(e.getIdObservacion());
		observacion.setDescripcion(e.getDescripcion());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		observacion.setFechaHora(e.getFechaHora()); 
		String fecha = sdf.format(e.getFechaHora()); 
		observacion.setFechaHoraString(fecha);
		observacion.setAltitud(e.getAltitud());
		observacion.setLatitud(e.getLatitud());
		observacion.setLongitud(e.getLongitud());
		observacion.setIdCriticidad(e.getIdCriticidad().getIdCriticidad());
		observacion.setIdCriticidadNombre(e.getIdCriticidad().getNombre());
		observacion.setIdFenomeno(e.getIdFenomeno().getIdFenomeno());
		observacion.setIdFenomenoNombre(e.getIdFenomeno().getNombre());
		observacion.setIdLocalidad(e.getIdLocalidad().getIdLocalidad());
		observacion.setIdLocalidadNombre(e.getIdLocalidad().getNombre());
		observacion.setIdUsuarioAlta(e.getIdUsuarioAlta().getIdUsuario());
		observacion.setIdUsuarioAltaNombre(e.getIdUsuarioAlta().getNombreUsuario());
		
			try{
				observacion.setIdUsuarioVali(e.getIdUsuarioVali().getIdUsuario());
				observacion.setIdUsuarioValiNombre(e.getIdUsuarioVali().getNombreUsuario());
			}catch (Exception ex) {
				observacion.setIdUsuarioVali(0);
				observacion.setIdUsuarioValiNombre("-");
			}
		
		if (e.getValidFecha()==null) {
			observacion.setValidFechaString("-");
		}else {
			observacion.setValidFecha(e.getValidFecha());
			String fechaVali = sdf.format(e.getFechaHora());
			observacion.setValidFechaString(fechaVali);
		}
		if (e.getValidComents()==null) {
			observacion.setValidComents("-");
		}else {
			observacion.setValidComents(e.getValidComents());
		}
		//verifico que tengo caracteristicas
		if (e.getObsCaracteristica().size()>0) {
			ObsCaracteristicasBO gestion = new ObsCaracteristicasBO();
			for(ObsCaracteristicas o: e.getObsCaracteristica()) {
				System.out.println ("fromObservaciones en OBSERVACIONES-BO "+ o.toString() );
				try {
					observacion.getListaCaract().add(gestion.obsCaracteristicasToObsCaractDTO(o));
				} catch (ServiciosException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		return observacion;
	}
	

		
		public Observaciones observacionDTOtoObservaciones(ObservacionesDTO e) throws ServiciosException {
			Observaciones observacion=new Observaciones();
			observacion.setIdObservacion(e.getIdObservacion());
			observacion.setDescripcion(e.getDescripcion());
			observacion.setFechaHora(e.getFechaHora());
			observacion.setAltitud(e.getAltitud());
			observacion.setLatitud(e.getLatitud());
			observacion.setLongitud(e.getLongitud());
			Criticidad criti = (Criticidad) criticidadPersistenciaDAO.buscarCodigo(e.getIdCriticidad());
			observacion.setIdCriticidad(criti);
			Fenomenos feno = fenomenosPersistenciaDAO.buscarCodigo(e.getIdFenomeno());
			observacion.setIdFenomeno(feno);
			Localidades loca = localidadesPersistenciaDAO.buscarCodigo(e.getIdLocalidad());
			observacion.setIdLocalidad(loca);
			Usuarios usu = usuariosPersistenciaDAO.buscarUsuario(e.getIdUsuarioAlta());
			observacion.setIdUsuarioAlta(usu);
			Usuarios usuVali = usuariosPersistenciaDAO.buscarUsuario(e.getIdUsuarioVali());
			observacion.setIdUsuarioVali(usuVali);
			observacion.setValidFecha(e.getValidFecha());
			observacion.setValidComents(e.getValidComents());
			return observacion;
		}
		// servicios para capa de Presentacion

		
		public List<ObservacionesDTO> obtenerObservacionesTodos() throws PersistenciaException   {	
			List<ObservacionesDTO> ret = new ArrayList<ObservacionesDTO>();
			List<Observaciones> emps = observacionesPersistenciaDAO.obtenerTodos();
			
			for(Observaciones e: emps) {
//				System.out.println ("obtenerObservacionesTodos() = Descripcon observacion=" +" "+ e.getDescripcion());
				ret.add(fromObservaciones(e));
//				System.out.println ("obtenerObservacionesTodos() = ToString=" +" "+ fromObservaciones(e).toString());
			}
			return ret;
		}

		public List<ObservacionesDTO> seleccionarObservaciones(String criterioBusqueda) throws PersistenciaException {
			
			List<ObservacionesDTO> ret = new ArrayList<ObservacionesDTO>();
			List<Observaciones> emps = observacionesPersistenciaDAO.seleccionarObservaciones(criterioBusqueda);
			
			for(Observaciones e: emps) {
				ret.add(fromObservaciones(e));
			}
			return ret;
		}
		
		public List<ObservacionesDTO> seleccionarPorBusqueda(String criterioBusqueda) throws PersistenciaException {
			
			List<ObservacionesDTO> ret = new ArrayList<ObservacionesDTO>();
			List<Observaciones> emps = observacionesPersistenciaDAO.seleccionarObservaciones(criterioBusqueda);
			
			for(Observaciones e: emps) {
				ret.add(fromObservaciones(e));
			}
			return ret;
		}
		
		public Observaciones toObservaciones(ObservacionesDTO e) throws ServiciosException {
			Observaciones observacion=new Observaciones();
			observacion.setIdObservacion(!Objects.isNull(e.getIdObservacion())?e.getIdObservacion():null);
			observacion.setDescripcion(e.getDescripcion());
			observacion.setFechaHora(e.getFechaHora());
			observacion.setAltitud(e.getAltitud());
			observacion.setLatitud(e.getLatitud());
			observacion.setLongitud(e.getLongitud());
			Criticidad criti = criticidadPersistenciaDAO.buscarCodigo(e.getIdCriticidad());
			observacion.setIdCriticidad(criti);
			Fenomenos feno = fenomenosPersistenciaDAO.buscarCodigo(e.getIdFenomeno());
			observacion.setIdFenomeno(feno);
			Localidades loca = localidadesPersistenciaDAO.buscarCodigo(e.getIdLocalidad());
			observacion.setIdLocalidad(loca);
			Usuarios usu = usuariosPersistenciaDAO.buscarUsuario(e.getIdUsuarioAlta());
			observacion.setIdUsuarioAlta(usu);
			Usuarios usuVali = usuariosPersistenciaDAO.buscarUsuario(e.getIdUsuarioVali());
			observacion.setIdUsuarioVali(usuVali);
			observacion.setValidFecha(e.getValidFecha());
			observacion.setValidComents(e.getValidComents());
			return observacion;
		}
		
		public List<ObservacionesDTO> obtenerTodasObservaciones(){
			List<ObservacionesDTO> ret = new ArrayList<ObservacionesDTO>();
			List<Observaciones> emps = observacionesPersistenciaDAO.obtenerTodos();
			
			for(Observaciones e: emps) {
				ret.add(fromObservaciones(e));
			}
			return ret;
		}
		public List<ObservacionesDTO> seleccionarPorFenomeno(long idFenomeno) throws PersistenciaException {
			
			List<ObservacionesDTO> ret = new ArrayList<ObservacionesDTO>();
			List<Observaciones> emps = observacionesPersistenciaDAO.obtenerPorFenomeno(idFenomeno);
			
			for(Observaciones e: emps) {
				ret.add(fromObservaciones(e));
			}
			return ret;
		}

		public List<ObservacionesDTO> seleccionarPorCriterios(String criterioDescripcion, long criterioFenomeno,
				long criterioCriticidad, Date criterioFechaIni, Date criterioFechaFin, int criterioValidado) throws PersistenciaException, ParseException, ServiciosException {
			
			if((criterioFechaIni!=null)&&(criterioFechaFin!=null)){
				if(criterioFechaFin.before(criterioFechaIni)) {
					throw new ServiciosException("La fecha Desde debe ser anterior a la fecha Hasta");
				}
			}
			

			List<ObservacionesDTO> ret = new ArrayList<ObservacionesDTO>();
			System.out.println ("seleccionarPorCriterios en OBSERVACIONES-BO" );
			List<Observaciones> emps = observacionesPersistenciaDAO.seleccionarPorCriterios( criterioDescripcion,  criterioFenomeno,
					 criterioCriticidad,  criterioFechaIni,  criterioFechaFin,  criterioValidado);
			
			for(Observaciones e: emps) {
				ret.add(fromObservaciones(e));
			}
			return ret;
		}
		
		// para trabajar con CARACTERISTICAS 
		public void modificarObservacionCompleta(ObservacionesDTO observacionSeleccionado) throws ServiciosException, PersistenciaException {
			System.out.println ("en modificarObservacionCompleta en ObservacionesBO" );
			System.out.println ("en modificarObservacionCompleta en ObservacionesBO toSgting de listan SICE /*/*/*/ = " +observacionSeleccionado.getListaCaract().size());
			Observaciones obs = toObservaciones(observacionSeleccionado);
			List<ObsCaracteristicasDTO> caractListDTO = observacionSeleccionado.getListaCaract();
			System.out.println ("en modificarObservacionCompleta en ObservacionesBO toString de lista caractListDTO/*/*/*/" +caractListDTO.get(0).toString());

			List<ObsCaracteristicas> caractList = new ArrayList<ObsCaracteristicas>();
			ObsCaracteristicasBO obsCaracteristicasBO = new ObsCaracteristicasBO();
			for(ObsCaracteristicasDTO item: caractListDTO) {
				System.out.println ("en for de modificarObservacionCompleta en ObservacionesBO" );
				caractList.add(obsCaracteristicasBO.obsCaractDTOtoObsCaracteristicas(item));
			}
			obs.setObsCaracteristica(caractList);
			Observaciones e = observacionesPersistenciaDAO.modificarObservacionCompleta(obs);
		
		}
		
		public ObservacionesDTO agregarObsCaract(Observaciones obs, Caracteristicas caract, long num, String texto, Date fecha) {
			Observaciones e = observacionesPersistenciaDAO.agregarObsCaract(obs, caract, num, texto, fecha);
			return fromObservaciones(e);
		}
		

		public Observaciones observacionDTOtoObservacionesREST(ObservacionesDTO e) throws ServiciosException {
			Criticidad cri = new Criticidad();
			cri.setIdCriticidad(e.getIdCriticidad());
			Fenomenos fen = new Fenomenos();
			fen.setIdFenomeno(e.getIdFenomeno());
			Localidades loc = new Localidades();
			loc.setIdLocalidad(e.getIdLocalidad());
			Usuarios usu = new Usuarios();
			usu.setIdUsuario(e.getIdUsuarioAlta());
			Observaciones observacion=new Observaciones();
			observacion.setIdObservacion(e.getIdObservacion());
			observacion.setDescripcion(e.getDescripcion());
			observacion.setFechaHora(e.getFechaHora());
			observacion.setAltitud(e.getAltitud());
			observacion.setLatitud(e.getLatitud());
			observacion.setLongitud(e.getLongitud());
			//Criticidad criti = (Criticidad) criticidadPersistenciaDAO.buscarCodigo(e.getIdCriticidad());
			observacion.setIdCriticidad(cri);
			//Fenomenos feno = fenomenosPersistenciaDAO.buscarCodigo(e.getIdFenomeno());
			observacion.setIdFenomeno(fen);
			//Localidades loca = localidadesPersistenciaDAO.buscarCodigo(e.getIdLocalidad());
			observacion.setIdLocalidad(loc);
			//Usuarios usu = usuariosPersistenciaDAO.buscarUsuario(e.getIdUsuarioAlta());
			observacion.setIdUsuarioAlta(usu);
//			Usuarios usuVali = usuariosPersistenciaDAO.buscarUsuario(e.getIdUsuarioVali());
//			observacion.setIdUsuarioVali(usuVali);
//			observacion.setValidFecha(e.getValidFecha());
//			observacion.setValidComents(e.getValidComents());
			return observacion;
		}
}
