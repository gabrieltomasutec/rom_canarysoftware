package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.capa1_presentacion.bean.TipoCaractDTO;
import com.capa3_persistence.dao.*;
import com.capa3_persistence.entities.TipoCaract;
import com.capa3_persistence.entities.TipoUsuario;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;


@Stateless
@LocalBean
public class TipoCaractBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	TipoCaractBean TipoCaractPersistenciaDAO;

	

	public TipoCaractDTO fromTipoCaract(TipoCaract e) {
		TipoCaractDTO tipoCaract = new TipoCaractDTO();
		tipoCaract.setId(e.getIdTipoCaract());
		tipoCaract.setNombre(e.getNombre());
		return tipoCaract;
	}

	public TipoCaract toTipoUsuario(TipoCaractDTO e) throws ServiciosException {
		TipoCaract tipoCaract = new TipoCaract();
		tipoCaract.setIdTipoCaract(e.getId());
		tipoCaract.setNombre(e.getNombre());
		return tipoCaract;
	}
	
		public List<TipoCaractDTO> obtenerTipoCaractTodos() throws PersistenciaException, ServiciosException   {	
			List<TipoCaractDTO> ret = new ArrayList<TipoCaractDTO>();
			List<TipoCaract> tipoCaract = TipoCaractPersistenciaDAO.obtenerTodos();
			
			for(TipoCaract e: tipoCaract) {
				ret.add(fromTipoCaract(e));
			}
			return ret;
		}
		
}
