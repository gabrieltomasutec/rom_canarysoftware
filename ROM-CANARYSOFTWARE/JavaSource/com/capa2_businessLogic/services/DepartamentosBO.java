package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.DepartamentosDTO;
import com.capa3_persistence.dao.CaracteristicasBean;
import com.capa3_persistence.dao.DepartamentosBean;
import com.capa3_persistence.dao.TipoCaractBean;
import com.capa3_persistence.dao.ZonasBean;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Departamentos;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.TipoCaract;
import com.capa3_persistence.entities.Zonas;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class DepartamentosBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	DepartamentosBean departamentosPersistenciaDAO;
	@EJB
	ZonasBean zonasPersistenciaDAO;
	
	public Departamentos departamentoDTOtoDepartamentos(DepartamentosDTO e) throws ServiciosException {
		Departamentos departamento=new Departamentos();
		departamento.setIdDepartamento(e.getId());
		departamento.setNombre(e.getNombre());
		Zonas zona = zonasPersistenciaDAO.buscarCodigo(e.getZona());
		departamento.setIdZona(zona);
		return departamento;
		
	}
	public DepartamentosDTO fromDepartamentos(Departamentos e) {
		DepartamentosDTO departamento=new DepartamentosDTO();
		departamento.setId(e.getIdDepartamento());
		departamento.setNombre(e.getNombre());
		departamento.setZona(e.getIdZona().getIdZona());
		return departamento;
	}
	public Departamentos toDepartamentos(DepartamentosDTO e) throws ServiciosException {
		Departamentos departamento=new Departamentos();
		departamento.setIdDepartamento(e.getId());
		departamento.setNombre(e.getNombre());
		Zonas zona = zonasPersistenciaDAO.buscarCodigo(e.getZona());
		departamento.setIdZona(zona);
		return departamento;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	
	public List<DepartamentosDTO> seleccionarDepartamentos() throws PersistenciaException {
		
		List<DepartamentosDTO> ret = new ArrayList<DepartamentosDTO>();
		List<Departamentos> emps = departamentosPersistenciaDAO.obtenerTodos();
		
		for(Departamentos e: emps) {
			ret.add(fromDepartamentos(e));
		}
		return ret;

	}
	
	public List<DepartamentosDTO> seleccionarDepartamentosPorNombre(String nombre) throws PersistenciaException {

		List<DepartamentosDTO> ret = new ArrayList<DepartamentosDTO>();
		List<Departamentos> emps = departamentosPersistenciaDAO.obtenerTodosPorNombre(nombre);
		
		for(Departamentos e: emps) {
			ret.add(fromDepartamentos(e));
		}
		return ret;

	}

	
	public DepartamentosDTO buscarDepartamento(Long id) throws ServiciosException {
		Departamentos e = departamentosPersistenciaDAO.buscarCodigo(id);
		return fromDepartamentos(e);
	}

	
	public DepartamentosDTO agregarDepartamento(DepartamentosDTO departamentoSeleccionado) throws PersistenciaException, ServiciosException   {
		Departamentos e = departamentosPersistenciaDAO.agregarDepartamento(toDepartamentos(departamentoSeleccionado));
		return fromDepartamentos(e);
	}


	public void actualizarDepartamento(DepartamentosDTO departamentoSeleccionado) throws PersistenciaException, ServiciosException   {
		departamentosPersistenciaDAO.modificarDepartamento(toDepartamentos(departamentoSeleccionado));
	}
	
	public void agregarDepartamentoRest(DepartamentosDTO departamento) throws PersistenciaException, ServiciosException   {		
		departamentosPersistenciaDAO.agregarDepartamento(departamentoDTOtoDepartamentos(departamento));		
	}
	
	public List<DepartamentosDTO> obtenerDepartamentosTodos() throws PersistenciaException   {	
		List<DepartamentosDTO> ret = new ArrayList<DepartamentosDTO>();
		List<Departamentos> emps = departamentosPersistenciaDAO.obtenerTodos();
		
		for(Departamentos e: emps) {
			ret.add(fromDepartamentos(e));
		}
		return ret;
	}
}
