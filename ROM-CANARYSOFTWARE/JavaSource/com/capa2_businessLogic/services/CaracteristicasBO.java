package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa3_persistence.dao.*;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.TipoCaract;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;


@Stateless
@LocalBean
public class CaracteristicasBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	CaracteristicasBean caracteristicasPersistenciaDAO;
	@EJB
	TipoCaractBean tipoCaractDAO;
	@EJB
	FenomenosBean fenomenosDAO;
	
	public Caracteristicas caracteristicaDTOtoCaracteristicas(CaracteristicasDTO e) throws ServiciosException {
		Caracteristicas caracteristica=new Caracteristicas();
		caracteristica.setIdCaracteristica(e.getId());
		caracteristica.setEtiqueta(e.getEtiqueta());
		caracteristica.setNombre(e.getNombre());
		TipoCaract cara = (TipoCaract) tipoCaractDAO.buscarCodigo(e.getTipoCaract());
		caracteristica.setIdTipoCaract(cara);
		Fenomenos fen = (Fenomenos) fenomenosDAO.buscarCodigo(e.getFenomeno());
		caracteristica.setIdFenomeno(fen);
		return caracteristica;
		
	}
	public CaracteristicasDTO fromCaracteristicas(Caracteristicas e) {
		CaracteristicasDTO caracteristica=new CaracteristicasDTO();
		caracteristica.setId(e.getIdCaracteristica());
		caracteristica.setEtiqueta(e.getEtiqueta());
		caracteristica.setNombre(e.getNombre());
		caracteristica.setTipoCaract(e.getIdTipoCaract().getIdTipoCaract());
		caracteristica.setTipoCaractNombre(e.getIdTipoCaract().getNombre());
		caracteristica.setFenomeno(e.getIdFenomeno().getIdFenomeno());
		caracteristica.setFenomenoNombre(e.getIdFenomeno().getNombre());
		return caracteristica;
	}
//	public CaracteristicaDTO fromCaracteristicas2DTO(Caracteristicas e) {
//		CaracteristicaDTO caracteristica=new CaracteristicaDTO();
//		caracteristica.setId(e.getIdCaracteristica());
//		caracteristica.setEtiqueta(e.getEtiqueta());
//		caracteristica.setNombre(e.getNombre());
//		//caracteristica.setTipoCaract(e.getIdTipoCaract());
//		//caracteristica.setFenomeno(e.getIdFenomeno());
//		return caracteristica;
//	}
	public Caracteristicas toCaracteristicas(CaracteristicasDTO e) throws ServiciosException {
		Caracteristicas caracteristica=new Caracteristicas();
		caracteristica.setIdCaracteristica(!Objects.isNull(e.getId())?e.getId():null);
		caracteristica.setNombre(e.getNombre());
		caracteristica.setEtiqueta(e.getEtiqueta());
		TipoCaract cara = (TipoCaract) tipoCaractDAO.buscarCodigo(e.getTipoCaract());
		caracteristica.setIdTipoCaract(cara);
		Fenomenos fen = (Fenomenos) fenomenosDAO.buscarCodigo(e.getFenomeno());
		caracteristica.setIdFenomeno(fen);
		return caracteristica;
	}
	
	// servicios para capa de Presentacion (NO ESTAN) FUERON DEFINIDOS EN CARACTERISTICASBEAN

	
		public List<CaracteristicasDTO> seleccionarCaracteristicas() throws PersistenciaException {
			
			List<CaracteristicasDTO> ret = new ArrayList<CaracteristicasDTO>();
			List<Caracteristicas> emps = caracteristicasPersistenciaDAO.obtenerTodos();
			
			for(Caracteristicas e: emps) {
				ret.add(fromCaracteristicas(e));
			}
			return ret;

		}

		
		public CaracteristicasDTO buscarCaracteristica(Long id) throws ServiciosException {
			Caracteristicas e = caracteristicasPersistenciaDAO.buscarCodigo(id);
			return fromCaracteristicas(e);
		}

		
		public CaracteristicasDTO agregarCaracteristica(CaracteristicasDTO caracteristicaSeleccionado) throws PersistenciaException, ServiciosException   {
			Caracteristicas e = caracteristicasPersistenciaDAO.agregarCaracteristica(toCaracteristicas(caracteristicaSeleccionado));
			return fromCaracteristicas(e);
		}


		public void actualizarCaracteristica(CaracteristicasDTO caracteristicaSeleccionado) throws PersistenciaException, ServiciosException   {
			caracteristicasPersistenciaDAO.modificarCaracteristica(toCaracteristicas(caracteristicaSeleccionado));
		}
		
		public void agregarCaracteristicaRest(CaracteristicasDTO caracteristica) throws PersistenciaException, ServiciosException   {		
			caracteristicasPersistenciaDAO.agregarCaracteristica(caracteristicaDTOtoCaracteristicas(caracteristica));		
		}
		
		public List<CaracteristicasDTO> obtenerCaracteristicasTodos() throws PersistenciaException   {	
			List<CaracteristicasDTO> ret = new ArrayList<CaracteristicasDTO>();
			List<Caracteristicas> emps = caracteristicasPersistenciaDAO.obtenerTodos();
			
			for(Caracteristicas e: emps) {
				ret.add(fromCaracteristicas(e));
			}
			return ret;
		}
		
		public List<CaracteristicasDTO> obtenerCaracteristicasPorFenomeno(long idFenomeno) throws PersistenciaException, ServiciosException   {	
			List<CaracteristicasDTO> ret = new ArrayList<CaracteristicasDTO>();
			//List<Caracteristicas> emps = caracteristicasPersistenciaDAO.obtenerTodos();
			List<Caracteristicas> emps = caracteristicasPersistenciaDAO.obtenerCaracteristicasPorFenomeno(idFenomeno);
			for(Caracteristicas e: emps) {
				ret.add(fromCaracteristicas(e));
			}
			return ret;
		}
		
}
