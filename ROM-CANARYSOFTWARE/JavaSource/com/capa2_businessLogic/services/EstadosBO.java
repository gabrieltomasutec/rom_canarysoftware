package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.EstadoDTO;
import com.capa1_presentacion.bean.EstadoDTO;
import com.capa3_persistence.dao.*;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Estado;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.TipoCaract;
import com.capa3_persistence.entities.TipoDoc;
import com.capa3_persistence.entities.TipoUsuario;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;


@Stateless
@LocalBean
public class EstadosBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	EstadoBean estadoPersistenciaDAO;

	public Estado estadoDTOtoEstados(EstadoDTO e) throws ServiciosException {
		Estado estado=new Estado();
		estado.setIdEstado(e.getId());
		estado.setNombre(e.getNombre());
		return estado;
		
	}

	public EstadoDTO fromEstado(Estado e) {
		EstadoDTO estado = new EstadoDTO();
		estado.setId(e.getIdEstado());
		estado.setNombre(e.getNombre());
		return estado;
	}

	public Estado toEstado(EstadoDTO e) throws ServiciosException {
		Estado estado = new Estado();
		estado.setIdEstado(e.getId());
		estado.setNombre(e.getNombre());
		return estado;
	}
	
		public List<EstadoDTO> obtenerEstadosTodos() throws PersistenciaException   {	
			List<EstadoDTO> ret = new ArrayList<EstadoDTO>();
			List<Estado> estado = estadoPersistenciaDAO.obtenerTodos();
			
			for(Estado e: estado) {
				ret.add(fromEstado(e));
			}
			return ret;
		}
		
//		public List<EstadoDTO> obtenerEstadosTodos() throws PersistenciaException   {	
//			List<EstadoDTO> ret = new ArrayList<EstadoDTO>();
//			List<Estado> emps = estadoPersistenciaDAO.obtenerTodos();
//			
//			for(Estado e: emps) {
//				ret.add(fromEstado(e));
//			}
//			return ret;
//		}
//		
		public EstadoDTO agregarEstado(EstadoDTO estadoSeleccionado) throws PersistenciaException, ServiciosException   {
			Estado e = estadoPersistenciaDAO.agregarEstado(toEstado(estadoSeleccionado));
			return fromEstado(e);
		}
		public void agregarEstadoRest(EstadoDTO estado) throws PersistenciaException, ServiciosException   {		
			estadoPersistenciaDAO.agregarEstado(estadoDTOtoEstados(estado));		
		}
}
