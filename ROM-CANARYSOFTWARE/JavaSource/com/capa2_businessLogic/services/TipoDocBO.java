package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.capa1_presentacion.bean.TipoDocDTO;
import com.capa3_persistence.dao.*;
import com.capa3_persistence.entities.TipoDoc;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;


@Stateless
@LocalBean
public class TipoDocBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	TipoDocBean tipoDocPersistenciaDAO;

	

	public TipoDocDTO fromTipoDoc(TipoDoc e) {
		TipoDocDTO TipoDoc = new TipoDocDTO();
		TipoDoc.setId(e.getIdTipoDoc());
		TipoDoc.setNombre(e.getNombre());
		return TipoDoc;
	}

	public TipoDoc toTipoDoc(TipoDocDTO e) throws ServiciosException {
		TipoDoc TipoDoc = new TipoDoc();
		TipoDoc.setIdTipoDoc(e.getId());
		TipoDoc.setNombre(e.getNombre());
		return TipoDoc;
	}
	
		public List<TipoDocDTO> obtenerTipoDocTodos() throws PersistenciaException, ServiciosException   {	
			List<TipoDocDTO> ret = new ArrayList<TipoDocDTO>();
			List<TipoDoc> tipoDoc = tipoDocPersistenciaDAO.obtenerDocs();
			
			for(TipoDoc e: tipoDoc) {
				ret.add(fromTipoDoc(e));
			}
			return ret;
		}
		
}
