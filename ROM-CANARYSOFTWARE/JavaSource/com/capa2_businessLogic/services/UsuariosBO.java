package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import com.capa1_presentacion.bean.UsuarioDTO;
import com.capa2_businessLogic.login.LoginBean;
import com.capa3_persistence.dao.EstadoBean;
import com.capa3_persistence.dao.TipoDocBean;
import com.capa3_persistence.dao.TipoUsuarioBean;
import com.capa3_persistence.dao.UsuariosBean;
import com.capa3_persistence.entities.Estado;
import com.capa3_persistence.entities.TipoDoc;
import com.capa3_persistence.entities.TipoUsuario;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean

public class UsuariosBO implements Serializable{

	private static final long serialVersionUID = 1L;

	@EJB
	UsuariosBean usuariosPersistenciaDAO;
	@EJB
	EstadoBean estadoPersistenciaDAO;
	@EJB
	TipoUsuarioBean tipoUsuarioDAO;
	@EJB
	TipoDocBean tipoDocDAO;

	public String actualizarUsuario(UsuarioDTO usuarioSeleccionado) throws PersistenciaException, ServiciosException   {
		String respuesta=validarIngresosModificar(usuarioSeleccionado);
		if(respuesta=="OK") {
			boolean ret = usuariosPersistenciaDAO.modificarUsuario(toUsuarios(usuarioSeleccionado));
			if(!ret) {
				respuesta = "No se pudo modificar el usuario. Si el error persiste, consulte al administrador.";
			}
		}
		return respuesta;
	}
	
	public UsuarioDTO agregarUsuario(UsuarioDTO usuarioSeleccionado) throws PersistenciaException, ServiciosException   {
		String respuesta=validarIngresosNuevo(usuarioSeleccionado);
		if(respuesta!="OK") {
			throw new ServiciosException(respuesta);
		}
		Usuarios e = usuariosPersistenciaDAO.agregarUsuario(toUsuarios(usuarioSeleccionado));
		return fromUsuarios(e);
	}
	
	public void agregarUsuarioRest(UsuarioDTO usuario) throws PersistenciaException, ServiciosException   {		
		usuariosPersistenciaDAO.agregarUsuario(toUsuarios(usuario));		
	}
	public UsuarioDTO buscarUsuario(Long id) throws ServiciosException {
		Usuarios e = usuariosPersistenciaDAO.buscarUsuario(id);
		return fromUsuarios(e);
	}
	public UsuarioDTO buscarNombreUsuario(String nombreUsuario) throws ServiciosException {
		Usuarios e = usuariosPersistenciaDAO.buscarNombreUsuario(nombreUsuario);
		return fromUsuarios(e);
	}

	
	// servicios para capa de Presentacion

	
	public UsuarioDTO fromUsuarios(Usuarios e) {
		UsuarioDTO usuario=new UsuarioDTO();
		usuario.setId(e.getIdUsuario());
		usuario.setNombreUsuario(e.getNombreUsuario());
		usuario.setPassword(e.getPassword());
		usuario.setEstado(e.getEstado().getIdEstado());
		usuario.setEstadoNombre(e.getEstado().getNombre());
		usuario.setTipoUsuario(e.getTipoUsuario().getIdTipoUsuario());
		usuario.setTipoUsuarioNombre(e.getTipoUsuario().getNombre());
		usuario.setNombre(e.getNombre());
		usuario.setApellido(e.getApellido());
		usuario.setTipoDocumento(e.getTipoDoc().getIdTipoDoc());
		usuario.setTipoDocumentoNombre(e.getTipoDoc().getNombre());
		usuario.setNumeroDocumento(e.getNumDoc());
		usuario.setDireccion(e.getDireccion());
		usuario.setCorreo(e.getCorreo());
		return usuario;
	}


	
	//------------------loginUsuario
	public UsuarioDTO loginUsuario(String usuario, String pwd) throws PersistenciaException   {
		//System.out.println(" - ***/////----*********login=[LOGINuSUARIO- USAURIObo]");
		UsuarioDTO ret = null;
		List<Usuarios> emps = usuariosPersistenciaDAO.loginUsuario(usuario,pwd);
		
		for(Usuarios e: emps) {
			ret = fromUsuarios(e);
			System.out.println(" Usuario logueado***** "+ ret.getNombre() +" "+ ret.getApellido());
		}
		return ret;
	}


	public List<UsuarioDTO> obtenerUsuariosTodos() throws PersistenciaException   {	
		List<UsuarioDTO> ret = new ArrayList<UsuarioDTO>();
		List<Usuarios> emps = usuariosPersistenciaDAO.buscarUsuarios();
		
		for(Usuarios e: emps) {
			ret.add(fromUsuarios(e));
		}
		return ret;
	}
	
	public List<UsuarioDTO> seleccionarUsuarios(String criterioNombre,String criterioDepartamento,Boolean criterioActivo) throws PersistenciaException {
		
		List<UsuarioDTO> ret = new ArrayList<UsuarioDTO>();
		List<Usuarios> emps = usuariosPersistenciaDAO.seleccionarUsuarios(criterioNombre,criterioDepartamento,criterioActivo);
		
		for(Usuarios e: emps) {
			ret.add(fromUsuarios(e));
		}
		return ret;

	}
	
	public Usuarios toUsuarios(UsuarioDTO e) throws ServiciosException {
		Usuarios usuario=new Usuarios();
		usuario.setIdUsuario(!Objects.isNull(e.getId())?e.getId():null);
		usuario.setNombreUsuario(e.getNombreUsuario());
		usuario.setPassword(e.getPassword());
		Estado estado = estadoPersistenciaDAO.buscarCodigo(e.getEstado());
		usuario.setEstado(estado);
		TipoUsuario tipoUsu = tipoUsuarioDAO.obtenerTipoUsu(e.getTipoUsuario());
		usuario.setTipoUsuario(tipoUsu);
		usuario.setNombre(e.getNombre());
		usuario.setApellido(e.getApellido());
		TipoDoc tipoDoc = tipoDocDAO.obtenerDoc(e.getTipoDocumento());
		usuario.setTipoDoc(tipoDoc);
		usuario.setNumDoc(e.getNumeroDocumento());
		usuario.setDireccion(e.getDireccion());
		usuario.setCorreo(e.getCorreo());
		return usuario;
	}


	public UsuarioDTO loguearUsuario(String usuario, String pwd) throws PersistenciaException, ServiciosException   {
		UsuarioDTO ret = null;
		Usuarios e = usuariosPersistenciaDAO.loguearUsuario(usuario,pwd);
			ret = fromUsuarios(e);
		return ret;
	}
	
	   @SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public boolean loginAD(String username, String password) {
		   try {

		         Hashtable env = new Hashtable();

		         env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

		         env.put(Context.PROVIDER_URL, "ldap://192.168.0.100:389/DC=PredioA,DC=com");

		         env.put(Context.SECURITY_AUTHENTICATION, "simple");

		         //env.put(Context.SECURITY_PRINCIPAL, "CN="+username.toLowerCase()+ ", cn=Users, DC=PredioA,DC=com");
			     env.put(Context.SECURITY_PRINCIPAL, username + "@PredioA.com");
			     
		         env.put(Context.SECURITY_CREDENTIALS, password);

	             DirContext ctx = new InitialDirContext(env);

	             return true;

	         } catch (NamingException ex) {

	             Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);

	         }

	         return false;


     }
	   
       public boolean validMail(String email){
           Pattern pattern = Pattern
               .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                       + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

               Matcher mather = pattern.matcher(email);

               if (mather.find() == true) {
                   System.out.println("El email ingresado es valido.");
               } else {
                   //JOptionPane.showMessageDialog(null, "Correo especificado tiene formato incorrecto!", "Error!", JOptionPane.ERROR_MESSAGE);
                   return false;
               }
               return true;
       }
       
       public boolean validCedula (String cedula){
         char clave; 
         byte  contNumero = 0;
         for (byte i = 0; i < cedula.length(); i++) {
              clave = cedula.charAt(i);
              String passValue = String.valueOf(clave);
              if (!(passValue.matches("[0-9]"))) {
                    contNumero++;
              }
          }
          if (contNumero==0){
              System.out.println("La c�dula es v�lida");
          }else{
               return false;
          }    
          return true;
      }
       
       public boolean validPassword(String password){
       	//String password = Cliente.desencriptarContrase�a(psw);
       	int longitud = password.length(); 
              if (!(longitud>=4 && longitud<=8)) {
                  //JOptionPane.showMessageDialog(null, "Longitud de password debe ser en 4 y 8 caracteres!", "Error!", JOptionPane.ERROR_MESSAGE);
                  return false;
              }

              char clave; 
              byte  contNumero = 0, contLetraMay = 0, contLetraMin=0;
              for (byte i = 0; i < password.length(); i++) {
                       clave = password.charAt(i);
                       String passValue = String.valueOf(clave);
                       if (passValue.matches("[A-Z]")) {
                             contLetraMay++;
                       } 
                       else if (passValue.matches("[a-z]")) {
                             contLetraMin++;
                       } 
                       else if (passValue.matches("[0-9]")) {
                             contNumero++;
                       }
               }
               if (contLetraMay>=1 && contLetraMin>=1 && contNumero>=1){
                   System.out.println("La clave es v�lida");
               }else{
                    //JOptionPane.showMessageDialog(null, "La clave debe contener una Mayuscula, una minuscula y un n�mero!", "Error!", JOptionPane.ERROR_MESSAGE);
                    return false;
               }    
               return true;
       }
       
       private String validarIngresosNuevo(UsuarioDTO usu) {
    	   if(usu.getPassword().isEmpty()) {
    		   return "Error: el password es un dato obligatorio.";
    	   }
    	   if(!validPassword(usu.getPassword())) {
    		   return "Error: La clave debe contener una may�scula, una min�scula y un n�mero, y largo entre 4 y 8 caracteres.";
    	   }
    	   if(usu.getCorreo().isEmpty()) {
    		   return "Error: el correo es un dato obligatorio.";
    	   }
    	   if(!validMail(usu.getCorreo())) {
    		   return "Error: el correo especificado tiene formato incorrecto.";
    	   }
    	   if(!validCedula(usu.getNumeroDocumento())) {
    		   return "Error: el n�mero de documento solamente puede contener d�gitos.";
    	   }
    	   return "OK";
       }
       
       private String validarIngresosModificar(UsuarioDTO usu) {
    	   if(!usu.getPassword().isEmpty()) {
        	   if(!validPassword(usu.getPassword())) {
        		   return "Error: La clave debe contener una may�scula, una min�scula y un n�mero, y largo entre 4 y 8 caracteres.";
        	   }  
    	   }

    	   if(usu.getCorreo().isEmpty()) {
    		   return "Error: el password es un dato obligatorio.";
    	   }
    	   if(!validMail(usu.getCorreo())) {
    		   return "Error: el correo especificado tiene formato incorrecto.";
    	   }
    	   if(!validCedula(usu.getNumeroDocumento())) {
    		   return "Error: el n�mero de documento solamente puede contener d�gitos";
    	   }
    	   return "OK";
       }
	   
	   
	
}
