package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.capa1_presentacion.bean.TipoUsuarioDTO;
import com.capa3_persistence.dao.*;
import com.capa3_persistence.entities.TipoUsuario;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;


@Stateless
@LocalBean
public class TipoUsuarioBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	TipoUsuarioBean TipoUsuarioPersistenciaDAO;

	

	public TipoUsuarioDTO fromTipoUsuario(TipoUsuario e) {
		TipoUsuarioDTO TipoUsuario = new TipoUsuarioDTO();
		TipoUsuario.setId(e.getIdTipoUsuario());
		TipoUsuario.setNombre(e.getNombre());
		return TipoUsuario;
	}

	public TipoUsuario toTipoUsuario(TipoUsuarioDTO e) throws ServiciosException {
		TipoUsuario TipoUsuario = new TipoUsuario();
		TipoUsuario.setIdTipoUsuario(e.getId());
		TipoUsuario.setNombre(e.getNombre());
		return TipoUsuario;
	}
	
		public List<TipoUsuarioDTO> obtenerTipoUsuarioTodos() throws PersistenciaException, ServiciosException   {	
			List<TipoUsuarioDTO> ret = new ArrayList<TipoUsuarioDTO>();
			List<TipoUsuario> TipoUsuario = TipoUsuarioPersistenciaDAO.obtenerTipoUsuarios();
			
			for(TipoUsuario e: TipoUsuario) {
				ret.add(fromTipoUsuario(e));
			}
			return ret;
		}
		
}
