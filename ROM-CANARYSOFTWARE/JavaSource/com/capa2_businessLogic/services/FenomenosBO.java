package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;

import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa3_persistence.dao.FenomenosBean;
import com.capa3_persistence.dao.ObservacionesBean;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Observaciones;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;


@Stateless
@LocalBean
public class FenomenosBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	FenomenosBean fenomenosPersistenciaDAO;
	
	@EJB
	ObservacionesBean observacionesPersistenciaDAO;
	
	public void borrarFenomeno(FenomenosDTO fenomenoSeleccionado) throws PersistenciaException, ServiciosException   {
		
		List<Observaciones> obsAsociadas = observacionesPersistenciaDAO.obtenerPorFenomeno(fenomenoSeleccionado.getIdFenomeno());
		
		System.out.println("Lista de Observaciones asociadas"+obsAsociadas);
		
		if (obsAsociadas.size() != 0) {
			throw new ServiciosException("No es posible eliminar el fenomeno porque tiene observaciones asociadas");
		}
				
		try {
			fenomenosPersistenciaDAO.borrarFenomeno(toFenomenos(fenomenoSeleccionado).getIdFenomeno());
		} catch (ServiciosException e) {
		    	System.out.println ("No se pudo eliminar el fenomeno");
		    }	
	}
	
	public void actualizarFenomeno(FenomenosDTO fenomenoSeleccionado) throws PersistenciaException, ServiciosException   {
		
		List<Observaciones> obsAsociadas = observacionesPersistenciaDAO.obtenerPorFenomeno(fenomenoSeleccionado.getIdFenomeno());
		
		System.out.println("Lista de Observaciones asociadas"+obsAsociadas);
		
		if (obsAsociadas.size() != 0) {
			throw new ServiciosException("No es posible modificar el fenomeno porque tiene observaciones asociadas");
		}
			
		boolean e = fenomenosPersistenciaDAO.modificarFenomeno(toFenomenos(fenomenoSeleccionado));

	}
	
	public FenomenosDTO agregarFenomeno(FenomenosDTO fenomenoSeleccionado) throws PersistenciaException, ServiciosException   {		
		Fenomenos e = fenomenosPersistenciaDAO.agregarFenomeno(toFenomenos(fenomenoSeleccionado));
		return fromFenomenos(e);
	}
	
	public void agregarFenomenoRest(FenomenosDTO fenomeno) throws PersistenciaException, ServiciosException   {		
		fenomenosPersistenciaDAO.crearFenomeno(fenomenoDTOtoFenomenos(fenomeno));		
	}
	
	public FenomenosDTO buscarFenomeno(Long id) throws ServiciosException {
		Fenomenos e = fenomenosPersistenciaDAO.buscarCodigo(id);
		return fromFenomenos(e);
	}
	
	// servicios para capa de Presentacion
	
	public Fenomenos fenomenoDTOtoFenomenos(FenomenosDTO e) throws ServiciosException {
		Fenomenos fenomeno=new Fenomenos();
		fenomeno.setIdFenomeno(e.getIdFenomeno());
		fenomeno.setNombre(e.getNombre());
		fenomeno.setDescripcion(e.getDescripcion());
		fenomeno.setTelefono(e.getTelefono());
		return fenomeno;
	}

	
	public FenomenosDTO fromFenomenos(Fenomenos e) {
		FenomenosDTO fenomeno=new FenomenosDTO();
		fenomeno.setIdFenomeno(e.getIdFenomeno());
		fenomeno.setNombre(e.getNombre());
		fenomeno.setDescripcion(e.getDescripcion());
		fenomeno.setTelefono(e.getTelefono());
		return fenomeno;
	}

	
//	public FenomenosDTOws fromFenomenos2DTO(Fenomenos e) {
//		FenomenosDTO fenomeno=new FenomenosDTO();
//		fenomeno.setId(e.getIdFenomeno());
//		fenomeno.setNombre(e.getNombre());
//		fenomeno.setDescripcion(e.getDescripcion());
//		fenomeno.setTelefono(e.getTelefono());
//		return fenomeno;
//	}


	public List<FenomenosDTO> obtenerFenomenosTodos() throws PersistenciaException   {	
		List<FenomenosDTO> ret = new ArrayList<FenomenosDTO>();
		List<Fenomenos> fen = fenomenosPersistenciaDAO.obtenerTodos();
		
		for(Fenomenos e: fen) {
			ret.add(fromFenomenos(e));
		}
		return ret;
	}
	
	public List<FenomenosDTO> seleccionarFenomenos() throws PersistenciaException {
		
		List<FenomenosDTO> ret = new ArrayList<FenomenosDTO>();
		List<Fenomenos> emps = fenomenosPersistenciaDAO.obtenerTodos();
		
		for(Fenomenos e: emps) {
			ret.add(fromFenomenos(e));
		}
		return ret;
	}
	
	public Fenomenos toFenomenos(FenomenosDTO e) throws ServiciosException {
		Fenomenos fenomeno=new Fenomenos();
		fenomeno.setIdFenomeno(!Objects.isNull(e.getIdFenomeno())?e.getIdFenomeno():null);
		fenomeno.setNombre(e.getNombre());
		fenomeno.setDescripcion(e.getDescripcion());
		fenomeno.setTelefono(e.getTelefono());
		return fenomeno;
	}
	
	public List<FenomenosDTO> seleccionarPorNombre(String nombre) throws PersistenciaException {
		List<FenomenosDTO> ret = new ArrayList<FenomenosDTO>();
		List<Fenomenos> emps = fenomenosPersistenciaDAO.obtenerTodosPorNombre(nombre);
		
		for(Fenomenos e: emps) {
			ret.add(fromFenomenos(e));
		}
		return ret;
	}
}
