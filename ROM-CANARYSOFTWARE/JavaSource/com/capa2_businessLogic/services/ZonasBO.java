package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.ZonasDTO;
import com.capa3_persistence.dao.ZonasBean;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Zonas;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class ZonasBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	ZonasBean zonasPersistenciaDAO;
	
	public Zonas zonasDTOtoZonas(ZonasDTO e) throws ServiciosException {
		Zonas zona=new Zonas();
		zona.setIdZona(e.getIdZona());
		zona.setNombre(e.getNombre());
				
		return zona;
		
	}

	public ZonasDTO fromZonas(Zonas e) {
		ZonasDTO zona = new ZonasDTO();
		zona.setIdZona(e.getIdZona());
		zona.setNombre(e.getNombre());
		return zona;
	}

	public Zonas toZonas(ZonasDTO e) throws ServiciosException {
		Zonas zona = new Zonas();
		zona.setIdZona(e.getIdZona());
		zona.setNombre(e.getNombre());
		return zona;
	}
	
		public List<ZonasDTO> obtenerZonasTodos() throws PersistenciaException, ServiciosException   {	
			List<ZonasDTO> ret = new ArrayList<ZonasDTO>();
			List<Zonas> zona = zonasPersistenciaDAO.obtenerTodos();
			
			for(Zonas e: zona) {
				ret.add(fromZonas(e));
			}
			return ret;
		}
		
		public List<ZonasDTO> seleccionarZonas(String criterioBusqueda) throws PersistenciaException {
			
			List<ZonasDTO> ret = new ArrayList<ZonasDTO>();
			List<Zonas> emps = zonasPersistenciaDAO.seleccionarZonas(criterioBusqueda);
			
			for(Zonas e: emps) {
				ret.add(fromZonas(e));
			}
			return ret;

		}
		public ZonasDTO agregarZona(ZonasDTO zonaSeleccionado) throws PersistenciaException, ServiciosException   {
			Zonas e = zonasPersistenciaDAO.agregarZona(toZonas(zonaSeleccionado));
			return fromZonas(e);
		}
		
		public void agregarZonaRest(ZonasDTO zona) throws PersistenciaException, ServiciosException   {		
			zonasPersistenciaDAO.agregarZona(zonasDTOtoZonas(zona));		
		}
		
		public ZonasDTO buscarZona(Long id) throws ServiciosException {
			Zonas e = zonasPersistenciaDAO.buscarCodigo(id);
			return fromZonas(e);
		}
		public void actualizarZona(ZonasDTO zonaSeleccionado) throws PersistenciaException, ServiciosException   {
			zonasPersistenciaDAO.modificarZona(toZonas(zonaSeleccionado));
		}
		public List<ZonasDTO> seleccionarZonas() throws PersistenciaException {
			
			List<ZonasDTO> ret = new ArrayList<ZonasDTO>();
			List<Zonas> emps = zonasPersistenciaDAO.obtenerTodos();
			
			for(Zonas e: emps) {
				ret.add(fromZonas(e));
			}
			return ret;

		}
	

}
