package com.capa2_businessLogic.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.CriticidadDTO;
import com.capa1_presentacion.bean.UsuarioDTO;
import com.capa3_persistence.dao.CriticidadBean;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Criticidad;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.TipoCaract;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class CriticidadBO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@EJB
	CriticidadBean criticidadPersistenciaDAO;

	public Criticidad criticidadDTOtoCriticidad(CriticidadDTO e) throws ServiciosException {
		Criticidad criticidad=new Criticidad();
		criticidad.setIdCriticidad(e.getIdCriticidad());
		criticidad.setNombre(e.getNombre());
		
		return criticidad;
		
	}

	public CriticidadDTO fromCriticidad(Criticidad e) {
		CriticidadDTO criticidad = new CriticidadDTO();
		criticidad.setIdCriticidad(e.getIdCriticidad());
		criticidad.setNombre(e.getNombre());
		return criticidad;
	}

	public Criticidad toCriticidad(CriticidadDTO e) throws ServiciosException {
		Criticidad criticidad = new Criticidad();
		criticidad.setIdCriticidad(e.getIdCriticidad());
		criticidad.setNombre(e.getNombre());
		return criticidad;
	}
	
		public List<CriticidadDTO> obtenerCriticidadTodos() throws PersistenciaException, ServiciosException   {	
			List<CriticidadDTO> ret = new ArrayList<CriticidadDTO>();
			List<Criticidad> criticidad = criticidadPersistenciaDAO.obtenerTodos();
			
			for(Criticidad e: criticidad) {
				ret.add(fromCriticidad(e));
			}
			return ret;
		}
		
		public List<CriticidadDTO> seleccionarCriticidad(String criterioBusqueda) throws PersistenciaException {
			
			List<CriticidadDTO> ret = new ArrayList<CriticidadDTO>();
			List<Criticidad> emps = criticidadPersistenciaDAO.seleccionarCriticidad(criterioBusqueda);
			
			for(Criticidad e: emps) {
				ret.add(fromCriticidad(e));
			}
			return ret;

		}
		
		public void agregarCriticidadRest(CriticidadDTO criticidad) throws PersistenciaException, ServiciosException   {		
			criticidadPersistenciaDAO.agregarCriticidad(criticidadDTOtoCriticidad(criticidad));		
		}
	

}
