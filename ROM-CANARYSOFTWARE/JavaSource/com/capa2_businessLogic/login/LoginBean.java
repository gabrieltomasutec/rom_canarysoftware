package com.capa2_businessLogic.login;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import org.primefaces.context.PrimeRequestContext;
import com.capa1_presentacion.bean.UsuarioDTO;
//@Named(value="loginBean")		//JEE8
//@SessionScoped				        //JEE8
import com.capa2_businessLogic.services.UsuariosBO;


@Stateless
@LocalBean
public class LoginBean implements Serializable {
	
	@EJB
	private UsuariosBO persistenciaBean;
  private static final long serialVersionUID = -2152389656664659476L;

  private String nombre;
  private String clave;
  private boolean logeado = false;
  

	public boolean estaLogeado() {
	    return logeado;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getClave() {
    return clave;
  }

  public void setClave(String clave) {
    this.clave = clave;
  }

  public void login(ActionEvent actionEvent) {
    //RequestContext context = RequestContext.getCurrentInstance();
	  PrimeRequestContext context = PrimeRequestContext.getCurrentInstance();
    FacesMessage msg = null;

    try {
    	if(nombre.contains("@")) {
    		String nom = nombre.substring(1);
    		//VERIFICO QUE TENGA @ EN EL STRING. SI TIENE ESE CARACTER  HAGO EL LOGIN CON EL ACTIVE DIRECTORY 
        	Boolean respuesta = persistenciaBean.loginAD(nom,clave);
        	if(respuesta) {
        		
        		UsuarioDTO usu = persistenciaBean.loginUsuario(nombre,clave);
	        	if(usu !=null) {
	        		//System.out.println("LOGIN CORRECT con el AD: la respuesta a la solicitud de acceso es: "+usu.toString()+ " con usuario: "+nom+" y clave: "+usuarioDTO.getPassword());
	        		if(usu.getEstado()==1) {
	        			//System.out.println("LOGIN CORRECT pero usaurio de AD no esta activo");
	            		logeado = true;
	    		    	msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenid@", nombre);
	            		UsuarioLogueado.getUsuarioLogueado().setUsuarioLogueadoDTO(UsuarioLogueado.getUsuarioLogueado().toUsuariosLogueadoDTO(usu));

	        		}else {
	            		logeado = false;
	                    msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error","Usuario no activo");
	        		}
	        	}
        		
//        		System.out.println("LOGIN CORRECT con el AD: la respuesta a la solicitud de acceso es: "+respuesta.toString()+ " con usuario: "+nombre+" y clave: "+clave);
		    	        	}else {
//        		System.out.println("LOGIN INCORRECT con el AD: la respuesta a la solicitud de acceso es: INCORRECTO con usuario: "+nombre+" y clave: "+clave);
        		logeado = false;
                msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error","Credenciales no v�lidas");
        	}
    	}else {	// DE LO CONTRARIO, HAGO EL LOGIN CON LA BASE DE DATOS
    		UsuarioDTO usu = persistenciaBean.loginUsuario(nombre, clave);
        	if(usu !=null) {
//        		System.out.println("LOGIN CORRECT con el AD: la respuesta a la solicitud de acceso es: "+usu.toString()+ " con usuario: "+nombre+" y clave: "+clave);
        		if(usu.getEstado()==1) {
            		logeado = true;
            		msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenid@", nombre);
            		UsuarioLogueado.getUsuarioLogueado().setUsuarioLogueadoDTO(UsuarioLogueado.getUsuarioLogueado().toUsuariosLogueadoDTO(usu)); 
        		}else {
            		logeado = false;
                    msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error","Usuario no activo.");
        		}

        	}else {
//        		System.out.println("LOGIN INCORRECT con el AD: la respuesta a la solicitud de acceso es: INCORRECTO con usuario: "+nombre+" y clave: "+clave);
        		logeado = false;
                msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error", "Credenciales no v�lidas");
        	}
    	}
    	  	

    }catch(Exception ex) {
    	logeado = false;
        msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error",
                               "Credenciales no v�lidas");
    }

    FacesContext.getCurrentInstance().addMessage(null, msg);
    //context.addCallbackParam("estaLogeado", logeado);
     context.getCallbackParams().put("estaLogeado", logeado);
    if (logeado)
      //context.addCallbackParam("view", "gauge.xhtml");
    	context.getCallbackParams().put("view", "index.xhtml");
    
   // System.out.println(" - ******************logeado=[" + logeado + "]");
  }

  public void logout() {
    HttpSession session = (HttpSession) FacesContext.getCurrentInstance() 
                                        .getExternalContext().getSession(false);
    session.invalidate();
    logeado = false;
    UsuarioLogueado.getUsuarioLogueado().setUsuarioLogueadoDTO(null);
  }
  
	
  public UsuarioDTO loginUsuario(UsuarioDTO usuarioDTO) {

	    try {
	    	if(usuarioDTO.getNombreUsuario().contains("@")) {
	    		String nom = usuarioDTO.getNombreUsuario().substring(1);
	    		//VERIFICO QUE TENGA @ EN EL STRING. SI TIENE ESE CARACTER  HAGO EL LOGIN CON EL ACTIVE DIRECTORY 
	    		boolean estaLogeado = persistenciaBean.loginAD(nom,usuarioDTO.getPassword());
	    		if(estaLogeado) {
		    		UsuarioDTO usu = persistenciaBean.loginUsuario(usuarioDTO.getNombreUsuario(),usuarioDTO.getPassword());
		        	if(usu !=null) {
		        		System.out.println("LOGIN CORRECT con el AD: la respuesta a la solicitud de acceso es: "+usu.toString()+ " con usuario: "+nom+" y clave: "+usuarioDTO.getPassword());
		        		if(usu.getEstado()==1) {
		        			System.out.println("LOGIN CORRECT pero usaurio de AD no esta activo");
		            		return usu; 
		        		}else {
		            		return null;
		        		}
		        	}
	    		}else {
	    			return null;
	    		}

	    	}else {	// DE LO CONTRARIO, HAGO EL LOGIN CON LA BASE DE DATOS
	    		UsuarioDTO usu = persistenciaBean.loginUsuario(usuarioDTO.getNombreUsuario(),usuarioDTO.getPassword());
	        	if(usu !=null) {
//	        		System.out.println("LOGIN CORRECT con el AD: la respuesta a la solicitud de acceso es: "+usu.toString()+ " con usuario: "+nombre+" y clave: "+clave);
	        		if(usu.getEstado()==1) {
	        			System.out.println("LOGIN CORRECT y usaurio de BD esta actiuvo");
	            		return usu; 
	        		}else {
	        			System.out.println("LOGIN CORRECT pero usaurio de BD no esta actiuvo");
	            		return null;
	        		}

	        	}else {
	        		System.out.println("LOGIN INCORRECT con el BD: la respuesta a la solicitud de acceso es: INCORRECTO con usuario: "+usuarioDTO.getNombreUsuario()+" y clave: "+usuarioDTO.getPassword());
	        		return null;
	        	}
	    	}
	    	  	
	    	return null;
	    }catch(Exception ex) {
	    	return null;
	    }


	  }

	

	
}
