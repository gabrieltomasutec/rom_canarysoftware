package com.capa2_businessLogic.login;

import com.capa1_presentacion.bean.UsuarioDTO;
import com.capa1_presentacion.bean.UsuarioLogueadoDTO;


public class UsuarioLogueado {
	
	private static UsuarioLogueado usuarioLogueado;
	private UsuarioLogueadoDTO usuarioLogueadoDTO;
	
	private UsuarioLogueado() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static UsuarioLogueado getUsuarioLogueado() {
		if(usuarioLogueado==null) {
			usuarioLogueado = new UsuarioLogueado();
		}
		return usuarioLogueado;
	}

	public UsuarioLogueadoDTO getUsuarioLogueadoDTO() {
		return usuarioLogueadoDTO;
	}
	public void setUsuarioLogueadoDTO(UsuarioLogueadoDTO usuarioLogueadoDTO) {
		this.usuarioLogueadoDTO = usuarioLogueadoDTO;
	}
	
	
	public UsuarioLogueadoDTO toUsuariosLogueadoDTO(UsuarioDTO e) {
		UsuarioLogueadoDTO usuario=new UsuarioLogueadoDTO();
		usuario.setId(e.getId());
		usuario.setNombreUsuario(e.getNombreUsuario());
		usuario.setPassword(e.getPassword());
		usuario.setEstado(e.getEstado());
		usuario.setEstadoNombre(e.getEstadoNombre());
		usuario.setTipoUsuario(e.getTipoUsuario());
		usuario.setTipoUsuarioNombre(e.getTipoUsuarioNombre());
		usuario.setNombre(e.getNombre());
		usuario.setApellido(e.getApellido());
		usuario.setTipoDocumento(e.getTipoDocumento());
		usuario.setTipoDocumentoNombre(e.getTipoDocumentoNombre());
		usuario.setNumeroDocumento(e.getNumeroDocumento());
		usuario.setDireccion(e.getDireccion());
		usuario.setCorreo(e.getCorreo());
		return usuario;
	}
	
	public UsuarioLogueadoDTO toUsuariosLogueadoDTO_AD(UsuarioDTO e) {
		UsuarioLogueadoDTO usuario=new UsuarioLogueadoDTO();
//		usuario.setId(e.getId());
//		usuario.setNombreUsuario(e.getNombreUsuario());
//		usuario.setPassword(e.getPassword());
//		usuario.setEstado(e.getEstado());
//		usuario.setEstadoNombre(e.getEstadoNombre());
//		usuario.setTipoUsuario(e.getTipoUsuario());
//		usuario.setTipoUsuarioNombre(e.getTipoUsuarioNombre());
//		usuario.setNombre(e.getNombre());
//		usuario.setApellido(e.getApellido());
//		usuario.setTipoDocumento(e.getTipoDocumento());
//		usuario.setTipoDocumentoNombre(e.getTipoDocumentoNombre());
//		usuario.setNumeroDocumento(e.getNumeroDocumento());
//		usuario.setDireccion(e.getDireccion());
//		usuario.setCorreo(e.getCorreo());
		return usuario;
	}
	
	
	

}
