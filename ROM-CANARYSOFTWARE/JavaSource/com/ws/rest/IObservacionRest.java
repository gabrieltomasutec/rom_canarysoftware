package com.ws.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.ObservacionesDTO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Path("observaciones/")
public interface IObservacionRest {

	@POST
    @Path("agregar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addObservacion(ObservacionesDTO observacion) throws ServiciosException;
      
    @GET
    @Path("echo")
    @Produces({MediaType.TEXT_PLAIN})	//Para probar que este funcionando el WS
    public String echo();
    
    @GET
    @Path("todos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObservaciones(); 
    
    @POST
    @Path("eliminar")
    @Produces(MediaType.APPLICATION_JSON)
	public Response deleteObservacion(ObservacionesDTO observacion) throws PersistenciaException; 
    
    
}
