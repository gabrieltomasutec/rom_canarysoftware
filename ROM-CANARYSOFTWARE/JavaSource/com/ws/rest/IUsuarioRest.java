package com.ws.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.UsuarioDTO;
import com.capa3_persistence.exception.ServiciosException;

@Path("usuario/")
public interface IUsuarioRest {
	
    @POST
    @Path("agregar")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUsuario(UsuarioDTO usuario) throws ServiciosException;
      
    @GET
    @Path("echo")
    @Produces({MediaType.TEXT_PLAIN})	//Para probar que este funcionando el WS
    public String echo();
    
    @GET
    @Path("todos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsuarios(); 
    
    @GET
    @Path("nombreUsuario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsuario(UsuarioDTO usuario) throws ServiciosException; 
    
    @POST
    @Path("login")
    @Produces(MediaType.TEXT_PLAIN)
    public String login(UsuarioDTO usuario);

    @POST
    @Path("loginUsuario")
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginUsuario(UsuarioDTO usuario) throws ServiciosException;
}