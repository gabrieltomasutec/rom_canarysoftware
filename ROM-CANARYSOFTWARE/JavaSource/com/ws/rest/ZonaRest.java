package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.ZonasDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.ZonasBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class ZonaRest implements IZonaRest{
	@EJB
	private ZonasBO persistenciaBean;
	
	@Override
	public Response addZona(ZonasDTO zona) throws ServiciosException {			

		try {
			persistenciaBean.agregarZonaRest(zona);
			return  Response.ok().build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Caracteristicas Disponible";
    }

	@Override
	public Response getZonas() throws ServiciosException {
		try {
			List<ZonasDTO> ret = persistenciaBean.obtenerZonasTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}

}
