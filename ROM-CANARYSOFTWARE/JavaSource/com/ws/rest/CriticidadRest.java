package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.CriticidadDTO;
import com.capa2_businessLogic.services.CriticidadBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class CriticidadRest implements ICriticidadRest{
	@EJB
	private CriticidadBO persistenciaBean;
	
	@Override
	public Response addCriticidad(CriticidadDTO criticidad) throws ServiciosException {			

		try {
			persistenciaBean.agregarCriticidadRest(criticidad);
			return  Response.ok().build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Criticidad Disponible";
    }

	@Override
	public Response getCriticidad() throws Exception{
		try {
			List<CriticidadDTO> ret = persistenciaBean.obtenerCriticidadTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}

}
