package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.EstadoDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.EstadosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class EstadoRest implements IEstadoRest {
	@EJB
	private EstadosBO persistenciaBean;
	
	@Override
	public Response addEstado(EstadoDTO estado) throws ServiciosException {			

		try {
			persistenciaBean.agregarEstadoRest(estado);
			return  Response.ok().build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Estado Disponible";
    }

	@Override
	public Response getEstados() {
		try {
			List<EstadoDTO> ret = persistenciaBean.obtenerEstadosTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}

}
