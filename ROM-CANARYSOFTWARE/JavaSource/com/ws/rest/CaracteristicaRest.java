package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.FenomenosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class CaracteristicaRest implements ICaracteristicaRest{

	@EJB
	private CaracteristicasBO persistenciaBean;
	
	@Override
	public Response addCaracteristica(CaracteristicasDTO caracteristica) throws ServiciosException {			

		try {
			persistenciaBean.agregarCaracteristicaRest(caracteristica);
			return  Response.ok().build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Caracteristicas Disponible";
    }

	@Override
	public Response getCaracteristicas() {
		try {
			List<CaracteristicasDTO> ret = persistenciaBean.obtenerCaracteristicasTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}
}
