package com.ws.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.capa3_persistence.exception.ServiciosException;

@Path("tipoDoc/")
public interface ITipoDocRest {
	@GET
    @Path("echo")
    @Produces({MediaType.TEXT_PLAIN})	//Para probar que este funcionando el WS
    public String echo();

	@GET
	@Path("todos")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTipoDoc() throws ServiciosException; 

}
