package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa2_businessLogic.services.FenomenosBO;
import com.capa2_businessLogic.services.UsuariosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class FenomenoRest implements IFenomenoRest{

	@EJB
	private FenomenosBO persistenciaBean;
	
	@Override
	public Response addFenomeno(FenomenosDTO fenomeno) throws ServiciosException {			

		try {
			persistenciaBean.agregarFenomenoRest(fenomeno);
			return  Response.ok().build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Fenomenos Disponible";
    }

	@Override
	public Response getFenomenos() {
		try {
			List<FenomenosDTO> ret = persistenciaBean.obtenerFenomenosTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}
}
