package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.LocalidadesDTO;
import com.capa2_businessLogic.services.LocalidadesBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class LocalidadRest implements ILocalidadRest{
	@EJB
	private LocalidadesBO persistenciaBean;
	
	@Override
	public Response addLocalidad(LocalidadesDTO localidad) throws ServiciosException {			

		try {
			persistenciaBean.agregarLocalidadRest(localidad);
			return  Response.ok().build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Localidad Disponible";
    }

	@Override
	public Response getLocalidades() {
		try {
			List<LocalidadesDTO> ret = persistenciaBean.obtenerLocalidadesTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}

}
