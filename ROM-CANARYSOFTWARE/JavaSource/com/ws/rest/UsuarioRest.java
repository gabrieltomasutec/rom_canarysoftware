package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.UsuarioDTO;
import com.capa2_businessLogic.login.LoginBean;
import com.capa2_businessLogic.services.UsuariosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class UsuarioRest implements IUsuarioRest {
		
		@EJB
		private UsuariosBO persistenciaBean;
		@EJB
		private LoginBean persistenciaLogin;
		
		@Override
		public Response addUsuario(UsuarioDTO usuario) throws ServiciosException {			

			try {
				persistenciaBean.agregarUsuarioRest(usuario);
				return  Response.ok().build();
			} catch (PersistenciaException e) {				
				e.printStackTrace();
				return  Response.serverError().build();
			}
		}

		@Override
	    public String echo() {
	        return "Servicio Usuarios Disponible";
	    }

		@Override
		public Response getUsuarios() {
			try {
				List<UsuarioDTO> ret = persistenciaBean.obtenerUsuariosTodos();				
				return  Response.ok().entity(ret).build();				
				
			} catch (PersistenciaException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return  Response.serverError().build();
			}
			
		}
		@Override
		public Response getUsuario(UsuarioDTO usuario) throws ServiciosException {
			try {
				persistenciaBean.buscarNombreUsuario(usuario.getNombreUsuario());
				return  Response.ok().build();
			} catch (ServiciosException e) {				
				e.printStackTrace();
				return  Response.serverError().build();
			}
		}

		@Override
		public String login(UsuarioDTO usuario) {
			try {
				UsuarioDTO ret = persistenciaBean.loginUsuario(usuario.getNombreUsuario(), usuario.getPassword());				
				if(ret != null) {
					return "El usuario logueado es : "+ret.toString();
				}else {
					return "Error: verifique nombre de usuario y/o clave.";
				}
				
			} catch (PersistenciaException e) {
				e.printStackTrace();
				return "algo sali� mal";
			}
		}
		
		@Override
		public Response loginUsuario(UsuarioDTO usuario) throws ServiciosException {
			try {
					UsuarioDTO ret = persistenciaLogin.loginUsuario(usuario);				
					return  Response.ok().entity(ret).build();			
				} catch (Exception e) {
					e.printStackTrace();
					return Response.serverError().build();
				}

		}
	    
	    
}
