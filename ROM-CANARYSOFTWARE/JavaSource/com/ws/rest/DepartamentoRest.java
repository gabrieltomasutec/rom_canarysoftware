package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.DepartamentosDTO;
import com.capa2_businessLogic.services.DepartamentosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class DepartamentoRest implements IDepartamentoRest {
	@EJB
	private DepartamentosBO persistenciaBean;
	
	@Override
	public Response addDepartamento(DepartamentosDTO departamento) throws ServiciosException {			

		try {
			persistenciaBean.agregarDepartamentoRest(departamento);
			return  Response.ok().build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Departamento Disponible";
    }

	@Override
	public Response getDepartamentos() {
		try {
			List<DepartamentosDTO> ret = persistenciaBean.obtenerDepartamentosTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}

}
