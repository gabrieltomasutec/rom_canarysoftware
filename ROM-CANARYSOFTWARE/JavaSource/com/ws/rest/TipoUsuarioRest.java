package com.ws.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.TipoDocDTO;
import com.capa1_presentacion.bean.TipoUsuarioDTO;
import com.capa2_businessLogic.services.TipoDocBO;
import com.capa2_businessLogic.services.TipoUsuarioBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class TipoUsuarioRest implements ITipoUsuarioRest{
	@EJB
	private TipoUsuarioBO persistenciaBean;
	
	@Override
    public String echo() {
        return "Servicio TipoUsuario Disponible";
    }

	@Override
	public Response getTipoUsuario() throws ServiciosException{
		try {
			List<TipoUsuarioDTO> ret = persistenciaBean.obtenerTipoUsuarioTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}
		
	}

}
