package com.ws.rest;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.Response;

import com.capa1_presentacion.bean.ObservacionesDTO;
import com.capa2_businessLogic.services.ObservacionesBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Stateless
@LocalBean
public class ObservacionRest implements IObservacionRest {

	@EJB
	private ObservacionesBO persistenciaBean;
	
	@Override
	public Response addObservacion(ObservacionesDTO observacion) throws ServiciosException {
		System.out.println ("public Response addObservacion obs.toString() " + observacion.toString());
		java.util.Date fecha = new Date();
		observacion.setFechaHora(fecha);
		try {
			ObservacionesDTO obs = persistenciaBean.agregarObservacionRest(observacion);
			observacion.setIdObservacion(obs.getIdObservacion());
			if(observacion.getListaCaract().size()>0) {
				for(int i = 0 ; i < observacion.getListaCaract().size(); i++) {
					observacion.getListaCaract().get(i).setIdObs(obs.getIdObservacion());
				}
		
				System.out.println ("public Response addObservacion observacion.toString() " + observacion.toString());
				System.out.println ("public Response addObservacion obs.toString() " + obs.toString());
				persistenciaBean.modificarObservacionCompleta(observacion);	
			}
			return  Response.ok().entity(observacion).build();
		} catch (PersistenciaException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

	@Override
    public String echo() {
        return "Servicio Observaciones Disponible";
    }

	@Override
	public Response getObservaciones() {
		try {
			List<ObservacionesDTO> ret = persistenciaBean.obtenerObservacionesTodos();				
			return  Response.ok().entity(ret).build();				
			
		} catch (PersistenciaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return  Response.serverError().build();
		}		
	}
	
	@Override
	public Response deleteObservacion(ObservacionesDTO observacion) throws PersistenciaException {		
		try {
			persistenciaBean.borrarObservacion(observacion);
			return  Response.ok().build();
		} catch (ServiciosException e) {				
			e.printStackTrace();
			return  Response.serverError().build();
		}
	}

}
