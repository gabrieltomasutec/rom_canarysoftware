package com.capa1_presentacion.gestion;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.DepartamentosDTO;
import com.capa2_businessLogic.services.DepartamentosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

@Named(value="gestionDepartamento")		//JEE8
@SessionScoped
public class GestionDepartamentoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	DepartamentosBO persistenciaBean;
	private Long id;
	private String modalidad;
	private DepartamentosDTO departamentoSeleccionado;
	
	private boolean modoEdicion=false;

	public GestionDepartamentoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	//Pasar a modo 
			public String salvarCambios() {
				
				if (departamentoSeleccionado.getId()==0) {
					
					
					
					DepartamentosDTO departamentoNuevo;
					try {
						departamentoNuevo = (DepartamentosDTO) persistenciaBean.agregarDepartamento(departamentoSeleccionado);
						this.id=departamentoNuevo.getId();
					
						//mensaje de actualizacion correcta
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado un nuevo Departamento", "");
						FacesContext.getCurrentInstance().addMessage(null, facesMsg);
						
						this.modalidad="view";
					
						
					} catch (PersistenciaException | ServiciosException e) {
						
						Throwable rootException=ExceptionsTools.getCause(e);
						String msg1=e.getMessage();
						String msg2=ExceptionsTools.formatedMsg(rootException);
						
						if (msg2.contains("NAME_DEPTO")) {
							msg1 = "No se pudo agregar el Departamento. Departamento ya existente.";
						}

						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
						FacesContext.getCurrentInstance().addMessage(null, facesMsg);
					
						this.modalidad="update";
						
						e.printStackTrace();
					}
					
					
					 
				} else if (modalidad.equals("update")) {
					
					try {
						persistenciaBean.actualizarDepartamento(departamentoSeleccionado);

						FacesContext.getCurrentInstance().addMessage(null, 
								new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado el Departamento.",""));
						
					} catch (PersistenciaException | ServiciosException e) {

						Throwable rootException=ExceptionsTools.getCause(e);
						String msg1=e.getMessage();
						String msg2=ExceptionsTools.formatedMsg(e.getCause());
						//mensaje de actualizacion correcta
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
						FacesContext.getCurrentInstance().addMessage(null, facesMsg);
					
						this.modalidad="update";
					
						e.printStackTrace();
					}
				}
				return "";
			}

			
			public void preRenderViewListener() {
				
				if (id!=null) {
					try {
						departamentoSeleccionado=persistenciaBean.buscarDepartamento(id);
					} catch (ServiciosException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					departamentoSeleccionado=new DepartamentosDTO();
				}
				if (modalidad.contentEquals("view")) {
					modoEdicion=false;
				}else if (modalidad.contentEquals("update")) {
					modoEdicion=true;
				}else if (modalidad.contentEquals("insert")) {
					modoEdicion=true;
				}else {
					modoEdicion=false;
					modalidad="view";
			
				}
			}
			//acciones
			public String cambiarModalidadUpdate() throws CloneNotSupportedException {
				
				return "DatosDepartamento?faces-redirect=true&includeViewParams=true";
				
			}

			public DepartamentosBO getPersistenciaBean() {
				return persistenciaBean;
			}

			public void setPersistenciaBean(DepartamentosBO persistenciaBean) {
				this.persistenciaBean = persistenciaBean;
			}

			public Long getId() {
				return id;
			}

			public void setId(Long id) {
				this.id = id;
			}

			public String getModalidad() {
				return modalidad;
			}

			public void setModalidad(String modalidad) {
				this.modalidad = modalidad;
			}

			public DepartamentosDTO getDepartamentoSeleccionado() {
				return departamentoSeleccionado;
			}

			public void setDepartamentoSeleccionado(DepartamentosDTO departamentoSeleccionado) {
				this.departamentoSeleccionado = departamentoSeleccionado;
			}

			public boolean isModoEdicion() {
				return modoEdicion;
			}

			public void setModoEdicion(boolean modoEdicion) {
				this.modoEdicion = modoEdicion;
			}
			
			

}
