package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.ObsCaracteristicasDTO;
import com.capa1_presentacion.bean.ObservacionesDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.ObsCaracteristicasBO;
import com.capa2_businessLogic.services.ObservacionesBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

@Named(value="gestionObservacion")		//JEE8
@SessionScoped
public class GestionObservacionDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	ObservacionesBO persistenciaBean;
	@Inject
	ObsCaracteristicasBO persistenciaBeanObsCaract;
	@Inject
	CaracteristicasBO persistenciaBeanCaracteristicas;
	
	private Long idObservacion;
	private String modalidad;
	
	private ObservacionesDTO observacionSeleccionado;
	private boolean modoEdicion=false;
	
	private ObsCaracteristicasDTO obsCaractSeleccionado;
	private ArrayList<ObsCaracteristicasDTO> listaCaractObservMostrar;
	//private ArrayList<ObsCaracteristicasDTO> listaCaractObservGuardar;
	
	
	public GestionObservacionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
		//acciones
		public String cambiarModalidadUpdate() throws CloneNotSupportedException {
			//this.modalidad="update";
			return "DatosObservacion?faces-redirect=true&includeViewParams=true";
			
		}
		public Long getIdObservacion() {
			return idObservacion;
		}
		public String getModalidad() {
			return modalidad;
		}
		public ObservacionesDTO getObservacionSeleccionado() {
			return observacionSeleccionado;
		}
		
		public ObservacionesBO getPersistenciaBean() {
			return persistenciaBean;
		}
		public boolean isModoEdicion() {
			return modoEdicion;
		}
		
		//se ejecuta antes de desplegar la vista
		public void preRenderViewListener() {
			
			if (idObservacion!=null) {
				try {
					observacionSeleccionado=persistenciaBean.buscarObservacion(idObservacion);
					//seteo las caracteristicas de la observacion
					listaCaractObservMostrar = observacionSeleccionado.getListaCaract();
				} catch (ServiciosException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				observacionSeleccionado=new ObservacionesDTO();
			}
			if (modalidad.contentEquals("view")) {
				modoEdicion=false;
			}else if (modalidad.contentEquals("update")) {
				modoEdicion=true;
			}else if (modalidad.contentEquals("insert")) {
				modoEdicion=true;
			}else if (modalidad.contentEquals("updateCaract")) {
				modoEdicion=true;
			}else {
				modoEdicion=false;
				modalidad="view";
		
			}
		}
		
		public String borrar() {
			
			try {
				persistenciaBean.borrarObservacion(observacionSeleccionado);

				FacesContext.getCurrentInstance().addMessage(null, 
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha eliminado la observacion.",""));
				
				this.idObservacion = null;			
				this.modalidad="insert";				
				
			} catch (PersistenciaException | ServiciosException e) {

				String msg1=e.getMessage();

				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1,"");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
				this.modalidad="update";
			
				e.printStackTrace();
			}
			return "";
		}		
		
		
		
		
		//Pasar a modo 
		public String salvarCambios() {
			
			if (observacionSeleccionado.getIdObservacion()==0) {
				
								
				ObservacionesDTO observacionNuevo;
				try {
					java.util.Date fecha = new Date();
					observacionSeleccionado.setFechaHora(fecha);
					observacionNuevo = (ObservacionesDTO) persistenciaBean.agregarObservacion(observacionSeleccionado);
					this.idObservacion=observacionNuevo.getIdObservacion();
					
					//mensaje de actualizacion correcta
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado una nueva Observacion", "");
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
					
					this.modalidad="view";
				
					
				} catch (PersistenciaException | ServiciosException e) {
					
					Throwable rootException=ExceptionsTools.getCause(e);
					String msg1=e.getMessage();
					String msg2=ExceptionsTools.formatedMsg(rootException);
					//mensaje de actualizacion correcta
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
					this.modalidad="update";
					
					e.printStackTrace();
				}
				
				
				 
			} else if (modalidad.equals("update")) {
				System.out.println ("} else if (modalidad.equals(\"update\")) { en GestionObservacionDTO" );
				try {
					java.util.Date fecha = new Date();
					observacionSeleccionado.setValidFecha(fecha);
					persistenciaBean.actualizarObservacion(observacionSeleccionado);

					FacesContext.getCurrentInstance().addMessage(null, 
							new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado observacion.",""));
					
				} catch (PersistenciaException | ServiciosException e) {

					Throwable rootException=ExceptionsTools.getCause(e);
					String msg1=e.getMessage();
					String msg2=ExceptionsTools.formatedMsg(e.getCause());
					//mensaje de actualizacion correcta
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
					this.modalidad="update";
				
					e.printStackTrace();
				}
			}else if (modalidad.equals("updateCaract")) {
				System.out.println ("}else if (modalidad.equals(\"updateCaract\")) { en GestionObservacionDTO" );
				try {
					System.out.println("lista observacionSeleccionado.getListaCaract() en \"argregarObsCaract() en "
							+ "GestionObservacionDTO\""+ observacionSeleccionado.getListaCaract().size());
					
					persistenciaBean.modificarObservacionCompleta(observacionSeleccionado);

					FacesContext.getCurrentInstance().addMessage(null, 
							new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado una caracteristica en modalidad updateCaract.",""));
					
				} catch (PersistenciaException | ServiciosException e) {

					Throwable rootException=ExceptionsTools.getCause(e);
					String msg1=e.getMessage();
					String msg2=ExceptionsTools.formatedMsg(e.getCause());
					//mensaje de actualizacion correcta
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
					this.modalidad="updateCaract";
				
					e.printStackTrace();
				}
			}
			return "";
		}
		public void setIdObservacion(Long id) {
			this.idObservacion = id;
		}
		public void setModalidad(String modalidad) {
			this.modalidad = modalidad;
		}
		public void setModoEdicion(boolean modoEdicion) {
			this.modoEdicion = modoEdicion;
		}
		public void setObservacionSeleccionado(ObservacionesDTO observacionSeleccionado) {
			this.observacionSeleccionado = observacionSeleccionado;
		}
		public void setPersistenciaBean(ObservacionesBO persistenciaBean) {
			this.persistenciaBean = persistenciaBean;
		}
		
		
		public ObsCaracteristicasDTO getObsCaractSeleccionado() {
			return obsCaractSeleccionado;
		}
		public void setObsCaractSeleccionado(ObsCaracteristicasDTO obsCaractSeleccionado) {
			this.obsCaractSeleccionado = obsCaractSeleccionado;
		}
				
		
		public ArrayList<ObsCaracteristicasDTO> getListaCaractObservMostrar() {
			if(this.listaCaractObservMostrar == null) {
				listaCaractObservMostrar = new ArrayList<ObsCaracteristicasDTO>(); 
			}		
			return listaCaractObservMostrar;
		}
		public void setListaCaractObservMostrar(ArrayList<ObsCaracteristicasDTO> listaCaractObserv) {
			this.listaCaractObservMostrar = listaCaractObserv;
		}
		
				
//		public ArrayList<ObsCaracteristicasDTO> getListaCaractObservGuardar() {
//			if(this.listaCaractObservGuardar == null) {
//				listaCaractObservGuardar = new ArrayList<ObsCaracteristicasDTO>(); 
//			}	
//			return listaCaractObservGuardar;
//		}
//		public void setListaCaractObservGuardar(ArrayList<ObsCaracteristicasDTO> listaCaractObservGuardar) {
//			this.listaCaractObservGuardar = listaCaractObservGuardar;
//		}
		public void argregarObsCaract() {
			System.out.println ("argregarObsCaract() en GestionObservacionDTO" );
				
					ObsCaracteristicasDTO obsCaractDTO = new ObsCaracteristicasDTO(observacionSeleccionado.getDatoNumeroCaract(),
							observacionSeleccionado.getDatoTextoCaract(), observacionSeleccionado.getIdCaract(), 
							"-", observacionSeleccionado.getIdObservacion(),observacionSeleccionado.getDescripcion());
					
					System.out.println ("lista obsCaractDTO en \"argregarObsCaract() en GestionObservacionDTO\""+ obsCaractDTO.toString());
					observacionSeleccionado.setDatoNumeroCaract(0);
					observacionSeleccionado.setDatoTextoCaract("");
					System.out.println ("lista observacionSeleccionado.getListaCaract() en \"argregarObsCaract() en GestionObservacionDTO\""+ observacionSeleccionado.getListaCaract().size());
					
					boolean yaEsta = false;
					for(ObsCaracteristicasDTO o: getListaCaractObservMostrar()) {
						if(o.getIdCaract() ==observacionSeleccionado.getIdCaract()) {
							yaEsta = true;
						}
					}
					
					//-------------**************************************
					
					if(!yaEsta) {
						observacionSeleccionado.getListaCaract().add(obsCaractDTO);
						try {
							System.out.println("lista observacionSeleccionado.getListaCaract() en \"argregarObsCaract() en "
									+ "GestionObservacionDTO\""+ observacionSeleccionado.getListaCaract().size());
							
							persistenciaBean.modificarObservacionCompleta(observacionSeleccionado);
							//getListaCaractObservGuardar().add(obsCaractDTO);
							getListaCaractObservMostrar().add(obsCaractDTO);
							FacesContext.getCurrentInstance().addMessage(null, 
									new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado una caracteristica.",""));
							
						} catch (PersistenciaException | ServiciosException e) {

							Throwable rootException=ExceptionsTools.getCause(e);
							String msg1=e.getMessage();
							String msg2=ExceptionsTools.formatedMsg(e.getCause());
							//mensaje de actualizacion correcta
							FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
							FacesContext.getCurrentInstance().addMessage(null, facesMsg);
						
							this.modalidad="updateCaract";
						
							e.printStackTrace();
						}
					}else {
						FacesContext.getCurrentInstance().addMessage(null, 
								new FacesMessage(FacesMessage.SEVERITY_ERROR, "La caracter�stica ya est� utilizada.",""));
						
					}
		}
		
		
		public void borrarObsCaract() {
			//persistenciaBeanObsCaract.borrarObsCaract(observacionSeleccionado.getIdObservacion(), observacionSelec) ;
		}
		
		private List<SelectItem> caractItemDeFenomeno = new ArrayList<SelectItem>();


		public List<SelectItem> getCaractItemDeFenomeno() throws PersistenciaException, ServiciosException {
			//List<CaracteristicasDTO> f = persistenciaBean.obtenerCaracteristicasTodos();
			List<CaracteristicasDTO> f = persistenciaBeanCaracteristicas.obtenerCaracteristicasPorFenomeno(observacionSeleccionado.getIdFenomeno());
			caractItemDeFenomeno.clear();
			for(int i=0; i<f.size(); i++) {
				SelectItem item = new SelectItem(f.get(i).getId(),f.get(i).getNombre());
				caractItemDeFenomeno.add(item);
			}
			return caractItemDeFenomeno;
		}
		public void setCaractItemDeFenomeno(List<SelectItem> caractItemDeFenomeno) {
			this.caractItemDeFenomeno = caractItemDeFenomeno;
		}
		
			
		
}
