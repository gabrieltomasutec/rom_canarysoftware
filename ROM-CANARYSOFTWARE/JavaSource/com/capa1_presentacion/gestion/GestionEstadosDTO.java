package com.capa1_presentacion.gestion;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.EstadoDTO;
import com.capa2_businessLogic.services.EstadosBO;
import com.capa3_persistence.exception.PersistenciaException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;	//JEE8
import javax.faces.model.SelectItem;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;


//@ManagedBean(name="usuario")

@Named(value="gestionEstados")		//JEE8
@SessionScoped				        //JEE8
public class GestionEstadosDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	EstadosBO persistenciaBean;
	/**
	 * 
	 */
	
//	private String criterioNombre;
//	private String criterioDepartamento;
//	private Boolean criterioActivo;
//	
	private List<EstadoDTO> estadosSeleccionados;
	private EstadoDTO estadoSeleccionado;
	
	public GestionEstadosDTO() {
		super();
	}
	
//	public Boolean getCriterioActivo() {
//		return criterioActivo;
//	}
//	
//	
//	public String getCriterioDepartamento() {
//		return criterioDepartamento;
//	}
	
	// ******** Getters & Setters****************************
	
//	public String getCriterioNombre() {
//		return criterioNombre;
//	}
	public EstadosBO getPersistenciaBean() {
		return persistenciaBean;
	}
	public EstadoDTO getestadoSeleccionado() {
		return estadoSeleccionado;
	}
	public List<EstadoDTO> getestadosSeleccionados() {
		return estadosSeleccionados;
	}
	// ********Acciones****************************
	public String seleccionarEstados() throws PersistenciaException {
		//estadosSeleccionados=persistenciaBean.seleccionarEstados(criterioNombre, criterioDepartamento, criterioActivo);
		return "";
	}
//	public void setCriterioActivo(Boolean criterioActivo) {
//		this.criterioActivo = criterioActivo;
//	}
//	public void setCriterioDepartamento(String criterioDepartamento) {
//		this.criterioDepartamento = criterioDepartamento;
//	}
//	public void setCriterioNombre(String criterioNombre) {
//		this.criterioNombre = criterioNombre;
//	}
	public void setPersistenciaBean(EstadosBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}
	public void setestadoSeleccionado(EstadoDTO estadoSeleccionado) {
		this.estadoSeleccionado = estadoSeleccionado;
	}

	public void setestadosSeleccionados(List<EstadoDTO> estadosSeleccionados) {
		this.estadosSeleccionados = estadosSeleccionados;
	}

//	public String verDatosUsuario() {
//		//Navegamos a datos usuario
//		return "DatosUsuario";
//	}
	
	private List<SelectItem> estadosItem = new ArrayList<SelectItem>();

//	@PostConstruct
//	public void init() {
//		List<EstadoDTO> l;
//		try {
//			l = persistenciaBean.obtenerEstadosTodos();
//
//			for(int i=0; i<l.size(); i++) {
//				SelectItem item = new SelectItem(l.get(i).getId(),l.get(i).getNombre());
//				estadosItem.add(item);
//			}
//		} catch (PersistenciaException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} 
//	}
	public List<SelectItem> getEstadosItem() throws PersistenciaException {
		List<EstadoDTO> l = persistenciaBean.obtenerEstadosTodos(); 
		estadosItem.clear();
		for(int i=0; i<l.size(); i++) {
			SelectItem item = new SelectItem(l.get(i).getId(),l.get(i).getNombre());
			estadosItem.add(item);
		}
		return estadosItem;
	}

	public void setEstadosItem(List<SelectItem> estadosItem) {
		this.estadosItem = estadosItem;
	}
	
	
	
	
}
