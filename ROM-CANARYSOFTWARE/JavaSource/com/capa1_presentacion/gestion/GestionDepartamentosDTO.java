package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.DepartamentosDTO;
import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa1_presentacion.bean.LocalidadesDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.DepartamentosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Named(value="gestionDepartamentos")		//JEE8
@SessionScoped
public class GestionDepartamentosDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	DepartamentosBO persistenciaBean;
	private List<DepartamentosDTO> departamentosSeleccionados;
	
	private DepartamentosDTO departamentoSeleccionado;
	
	private String criterioNombre;
	
	
	
	private List<SelectItem> departamentosItem = new ArrayList<SelectItem>();
	
	// ********Acciones****************************
		public String seleccionarDepartamentos() throws PersistenciaException {
			departamentosSeleccionados=persistenciaBean.seleccionarDepartamentos();
			return "";
		}
		
		public String seleccionarDepartamentosPorNombre() throws PersistenciaException {
			departamentosSeleccionados=persistenciaBean.seleccionarDepartamentosPorNombre(criterioNombre);
			return "";
		}
		
		public String verDatosDepartamento() {
			//Navegamos a datos 
			return "DatosDepartamento";
		}

		public GestionDepartamentosDTO() {
			super();
			// TODO Auto-generated constructor stub
		}

		public List<SelectItem> getDepartamentosItem() throws PersistenciaException{
			List<DepartamentosDTO> f = persistenciaBean.obtenerDepartamentosTodos(); 
			departamentosItem.clear();
			for(int i=0; i<f.size(); i++) {
				SelectItem item = new SelectItem(f.get(i).getId(),f.get(i).getNombre());
				departamentosItem.add(item);
			}
			return departamentosItem;
		}
		public void setDepartamentosItem(List<SelectItem> departamentosItem) {
			this.departamentosItem = departamentosItem;
		}

		
		
	////////////////////////////////////////////////////////////////////////////////////////	
		public DepartamentosBO getPersistenciaBean() {
			return persistenciaBean;
		}

		public void setPersistenciaBean(DepartamentosBO persistenciaBean) {
			this.persistenciaBean = persistenciaBean;
		}

		public List<DepartamentosDTO> getDepartamentosSeleccionados() {
			return departamentosSeleccionados;
		}

		public void setDepartamentosSeleccionados(List<DepartamentosDTO> departamentosSeleccionados) {
			this.departamentosSeleccionados = departamentosSeleccionados;
		}

		public DepartamentosDTO getDepartamentoSeleccionado() {
			return departamentoSeleccionado;
		}

		public void setDepartamentoSeleccionado(DepartamentosDTO departamentoSeleccionado) {
			this.departamentoSeleccionado = departamentoSeleccionado;
		}

		public String getCriterioNombre() {
			return criterioNombre;
		}

		public void setCriterioNombre(String criterioNombre) {
			this.criterioNombre = criterioNombre;
		}
	

}
