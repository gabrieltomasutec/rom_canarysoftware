package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa1_presentacion.bean.LocalidadesDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.LocalidadesBO;
import com.capa3_persistence.exception.PersistenciaException;

@Named(value="gestionLocalidades")		//JEE8
@SessionScoped
public class GestionLocalidadesDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	LocalidadesBO persistenciaBean;
	private List<LocalidadesDTO> localidadesSeleccionados;
	
	private LocalidadesDTO localidadSeleccionado;
	
	private String criterioNombre;
	
	private List<SelectItem> localidadesItem = new ArrayList<SelectItem>();
	
	// ********Acciones****************************
		public String seleccionarLocalidades() throws PersistenciaException {
			localidadesSeleccionados=persistenciaBean.seleccionarLocalidades();
			return "";
		}
		
		public String seleccionarLocalidadesPorNombre() throws PersistenciaException {
			localidadesSeleccionados = persistenciaBean.seleccionarPorNombre(criterioNombre);
			return "";
		}
		public String verDatosLocalidad() {
			//Navegamos a datos 
			return "DatosLocalidad";
		}
		//////////////////////////////////
		public GestionLocalidadesDTO() {
			super();
			// TODO Auto-generated constructor stub
		}
		public LocalidadesBO getPersistenciaBean() {
			return persistenciaBean;
		}
		public void setPersistenciaBean(LocalidadesBO persistenciaBean) {
			this.persistenciaBean = persistenciaBean;
		}
		public List<LocalidadesDTO> getLocalidadesSeleccionados() {
			return localidadesSeleccionados;
		}
		public void setLocalidadesSeleccionados(List<LocalidadesDTO> localidadesSeleccionados) {
			this.localidadesSeleccionados = localidadesSeleccionados;
		}
		public LocalidadesDTO getLocalidadSeleccionado() {
			return localidadSeleccionado;
		}
		public void setLocalidadSeleccionado(LocalidadesDTO localidadSeleccionado) {
			this.localidadSeleccionado = localidadSeleccionado;
		}
		public String getCriterioNombre() {
			return criterioNombre;
		}
		public void setCriterioNombre(String criterioNombre) {
			this.criterioNombre = criterioNombre;
		}
		public List<SelectItem> getLocalidadesItem() throws PersistenciaException{
			List<LocalidadesDTO> f = persistenciaBean.obtenerLocalidadesTodos(); 
			localidadesItem.clear();
			for(int i=0; i<f.size(); i++) {
				SelectItem item = new SelectItem(f.get(i).getId(),f.get(i).getNombre());
				localidadesItem.add(item);
			}
			return localidadesItem;
		}
		public void setLocalidadesItem(List<SelectItem> localidadesItem) {
			this.localidadesItem = localidadesItem;
		}
		
		

}
