package com.capa1_presentacion.gestion;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.LocalidadesDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.LocalidadesBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

@Named(value="gestionLocalidad")		//JEE8
@SessionScoped
public class GestionLocalidadDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	LocalidadesBO persistenciaBean;
	private Long id;
	private String modalidad;
	private LocalidadesDTO localidadSeleccionado;
	
	private boolean modoEdicion=false;

	public GestionLocalidadDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	//Pasar a modo 
			public String salvarCambios() {
				
				if (localidadSeleccionado.getId()==0) {
					
										
					LocalidadesDTO localidadNuevo;
					try {
						localidadNuevo = (LocalidadesDTO) persistenciaBean.agregarLocalidad(localidadSeleccionado);
						this.id=localidadNuevo.getId();
					
						//mensaje de actualizacion correcta
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado una nueva Localidad", "");
						FacesContext.getCurrentInstance().addMessage(null, facesMsg);
						
						this.modalidad="view";
					
						
					} catch (PersistenciaException | ServiciosException e) {
						
						Throwable rootException=ExceptionsTools.getCause(e);
						String msg1=e.getMessage();
						String msg2=ExceptionsTools.formatedMsg(rootException);
						//mensaje de actualizacion correcta
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
						FacesContext.getCurrentInstance().addMessage(null, facesMsg);
					
						this.modalidad="update";
						
						e.printStackTrace();
					}
					
					
					 
				} else if (modalidad.equals("update")) {
					
					try {
						persistenciaBean.actualizarLocalidad(localidadSeleccionado);

						FacesContext.getCurrentInstance().addMessage(null, 
								new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado la Localidad.",""));
						
					} catch (PersistenciaException | ServiciosException e) {

						Throwable rootException=ExceptionsTools.getCause(e);
						String msg1=e.getMessage();
						String msg2=ExceptionsTools.formatedMsg(e.getCause());
						//mensaje de actualizacion correcta
						FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
						FacesContext.getCurrentInstance().addMessage(null, facesMsg);
					
						this.modalidad="update";
					
						e.printStackTrace();
					}
				}
				return "";
			}

			//(NO TIENE) SE AGREGO ABM DEFINIDO EN CARACTERISTICASBEAN
			public void preRenderViewListener() {
				
				if (id!=null) {
					try {
						localidadSeleccionado=persistenciaBean.buscarLocalidad(id);
					} catch (ServiciosException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					localidadSeleccionado=new LocalidadesDTO();
				}
				if (modalidad.contentEquals("view")) {
					modoEdicion=false;
				}else if (modalidad.contentEquals("update")) {
					modoEdicion=true;
				}else if (modalidad.contentEquals("insert")) {
					modoEdicion=true;
				}else {
					modoEdicion=false;
					modalidad="view";
			
				}
			}
			//acciones
			public String cambiarModalidadUpdate() throws CloneNotSupportedException {
				
				return "DatosLocalidad?faces-redirect=true&includeViewParams=true";
				
			}
			
			//getters y setters

			public LocalidadesBO getPersistenciaBean() {
				return persistenciaBean;
			}

			public void setPersistenciaBean(LocalidadesBO persistenciaBean) {
				this.persistenciaBean = persistenciaBean;
			}

			public Long getId() {
				return id;
			}

			public void setId(Long id) {
				this.id = id;
			}

			public String getModalidad() {
				return modalidad;
			}

			public void setModalidad(String modalidad) {
				this.modalidad = modalidad;
			}

			public LocalidadesDTO getLocalidadSeleccionado() {
				return localidadSeleccionado;
			}

			public void setLocalidadSeleccionado(LocalidadesDTO localidadSeleccionado) {
				this.localidadSeleccionado = localidadSeleccionado;
			}

			public boolean isModoEdicion() {
				return modoEdicion;
			}

			public void setModoEdicion(boolean modoEdicion) {
				this.modoEdicion = modoEdicion;
			}
			
			
			

}
