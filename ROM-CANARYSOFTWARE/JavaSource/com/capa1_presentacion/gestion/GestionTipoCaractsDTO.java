package com.capa1_presentacion.gestion;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.TipoCaractDTO;
import com.capa2_businessLogic.services.TipoCaractBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

import javax.enterprise.context.SessionScoped;	//JEE8
import javax.faces.model.SelectItem;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;


//@ManagedBean(name="usuario")

@Named(value="gestionTipoCaracts")		//JEE8
@SessionScoped				        //JEE8
public class GestionTipoCaractsDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	TipoCaractBO persistenciaBean;
	/**
	 * 
	 */

	
	private List<TipoCaractDTO> tipoCaractSeleccionados;
	private TipoCaractDTO tipoCaractSeleccionado;
	private List<SelectItem> tipoCaractsItem = new ArrayList<SelectItem>();
	
	public GestionTipoCaractsDTO() {
		super();
	}
	
	
	// ******** Getters & Setters****************************
	
	
	public List<SelectItem> gettipoCaractsItem() throws PersistenciaException, ServiciosException {
		List<TipoCaractDTO> l = persistenciaBean.obtenerTipoCaractTodos(); 
		tipoCaractsItem.clear();
		for(int i=0; i<l.size(); i++) {
			SelectItem item = new SelectItem(l.get(i).getId(),l.get(i).getNombre());
			tipoCaractsItem.add(item);
		}
		return tipoCaractsItem;
	}

	public TipoCaractBO getPersistenciaBean() {
		return persistenciaBean;
	}

	public void setPersistenciaBean(TipoCaractBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}

	public List<TipoCaractDTO> gettipoCaractSeleccionados() {
		return tipoCaractSeleccionados;
	}

	public void settipoCaractSeleccionados(List<TipoCaractDTO> tipoCaractSeleccionados) {
		this.tipoCaractSeleccionados = tipoCaractSeleccionados;
	}

	public TipoCaractDTO gettipoCaractSeleccionado() {
		return tipoCaractSeleccionado;
	}

	public void settipoCaractSeleccionado(TipoCaractDTO tipoCaractSeleccionado) {
		this.tipoCaractSeleccionado = tipoCaractSeleccionado;
	}


	public void setTipoCaractsItem(List<SelectItem> tipoCaractsItem) {
		this.tipoCaractsItem = tipoCaractsItem;
	}
	
	
	
}
