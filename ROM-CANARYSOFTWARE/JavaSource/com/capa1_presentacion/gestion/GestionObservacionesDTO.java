package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.ObservacionesDTO;
import com.capa2_businessLogic.services.ObservacionesBO;
import com.capa2_businessLogic.services.UsuariosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

@Named(value="gestionObservaciones")		//JEE8
@SessionScoped
public class GestionObservacionesDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	ObservacionesBO persistenciaBean;
	
	private List<ObservacionesDTO> observacionesSeleccionados;
	private ObservacionesDTO observacionSeleccionado;
	private long criterioId;
	private String criterioDescripcion;
	private long criterioFenomeno;
	private long criterioCriticidad;
	private Date criterioFechaIni;
	private Date criterioFechaFin;
	private int criterioValidado;
	
	public GestionObservacionesDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	// ******** Getters & Setters****************************
	public ObservacionesBO getPersistenciaBean() {
		return persistenciaBean;
	}



	public void setPersistenciaBean(ObservacionesBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}



	public List<ObservacionesDTO> getObservacionesSeleccionados() {
		return observacionesSeleccionados;
	}



	public void setObservacionesSeleccionados(List<ObservacionesDTO> observacionesSeleccionados) {
		this.observacionesSeleccionados = observacionesSeleccionados;
	}



	public ObservacionesDTO getObservacionSeleccionado() {
		return observacionSeleccionado;
	}



	public void setObservacionSeleccionado(ObservacionesDTO observacionSeleccionado) {
		this.observacionSeleccionado = observacionSeleccionado;
	}



	public long getCriterioId() {
		return criterioId;
	}



	public void setCriterioId(long criterioId) {
		this.criterioId = criterioId;
	}



	public String getCriterioDescripcion() {
		return criterioDescripcion;
	}



	public void setCriterioDescripcion(String criterioDescripcion) {
		this.criterioDescripcion = criterioDescripcion;
	}



	public long getCriterioFenomeno() {
		return criterioFenomeno;
	}



	public void setCriterioFenomeno(long criterioFenomeno) {
		this.criterioFenomeno = criterioFenomeno;
	}



	public long getCriterioCriticidad() {
		return criterioCriticidad;
	}



	public void setCriterioCriticidad(long criterioCriticidad) {
		this.criterioCriticidad = criterioCriticidad;
	}



	public Date getCriterioFechaIni() {
		return criterioFechaIni;
	}



	public void setCriterioFechaIni(Date criterioFechaIni) {
		this.criterioFechaIni = criterioFechaIni;
	}



	public Date getCriterioFechaFin() {
		return criterioFechaFin;
	}



	public void setCriterioFechaFin(Date criterioFechaFin) {
		this.criterioFechaFin = criterioFechaFin;
	}



	public int getCriterioValidado() {
		return criterioValidado;
	}



	public void setCriterioValidado(int criterioValidado) {
		this.criterioValidado = criterioValidado;
	}

	
	
		// ********Acciones****************************
		public String seleccionarObservaciones() throws PersistenciaException {
			//observacionesSeleccionados=persistenciaBean.seleccionarObservaciones(criterioId);
			return "";
		}

		public String seleccionarObservacionesPorBusqueda() throws PersistenciaException {
			//observacionesSeleccionados = persistenciaBean.seleccionarPorBusqueda(criterioBusqueda);
			observacionesSeleccionados = persistenciaBean.obtenerObservacionesTodos();
			return "";
		}
		public String seleccionarObservacionesPorFenomeno() throws PersistenciaException {
			
			observacionesSeleccionados = persistenciaBean.seleccionarPorFenomeno(criterioFenomeno);
			return "";
		}

		public String verDatosObservacion() {
			//Navegamos a datos 
			return "DatosObservacion";
		}
		
		public String seleccionarObservacionesPorCriterios() throws PersistenciaException, ParseException, ServiciosException {
//			DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			Date convertido = fechaHora.parse(criterioFechaIni);
			//System.out.println(convertido);
			
			try {
			
				System.out.println ("seleccionarObservacionesPorCriterios en GESTIONOBSERVACIONES-DTO" );
				observacionesSeleccionados = persistenciaBean.seleccionarPorCriterios(criterioDescripcion,criterioFenomeno,
						criterioCriticidad,criterioFechaIni,criterioFechaFin, criterioValidado);
				
			} catch (PersistenciaException | ServiciosException e) {
				
				Throwable rootException=ExceptionsTools.getCause(e);
				String msg1=e.getMessage();
				//String msg2=ExceptionsTools.formatedMsg(rootException);
				
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1,"");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
				e.printStackTrace();
			}
								
			return "";	
			
		}
		
		
}
