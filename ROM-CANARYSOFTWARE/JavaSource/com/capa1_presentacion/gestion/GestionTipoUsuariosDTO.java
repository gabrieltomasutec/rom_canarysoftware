package com.capa1_presentacion.gestion;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.TipoUsuarioDTO;
import com.capa2_businessLogic.services.TipoUsuarioBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;	//JEE8
import javax.faces.model.SelectItem;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;


//@ManagedBean(name="usuario")

@Named(value="gestionTipoUsuarios")		//JEE8
@SessionScoped				        //JEE8
public class GestionTipoUsuariosDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	TipoUsuarioBO persistenciaBean;
	/**
	 * 
	 */
	
//	private String criterioNombre;
//	private String criterioDepartamento;
//	private Boolean criterioActivo;
//	
	private List<TipoUsuarioDTO> tipoUsuariosSeleccionados;
	private TipoUsuarioDTO tipoUsuarioSeleccionado;
	private List<SelectItem> tipoUsuariosItem = new ArrayList<SelectItem>();
	
	public GestionTipoUsuariosDTO() {
		super();
	}
	
	// ******** Getters & Setters****************************
	

	public TipoUsuarioBO getPersistenciaBean() {
		return persistenciaBean;
	}
	public TipoUsuarioDTO gettipoUsuarioSeleccionado() {
		return tipoUsuarioSeleccionado;
	}
	public List<TipoUsuarioDTO> gettipoUsuariosSeleccionados() {
		return tipoUsuariosSeleccionados;
	}
	// ********Acciones****************************
	public String seleccionarEstados() throws PersistenciaException {
		//tipoUsuariosSeleccionados=persistenciaBean.seleccionarEstados(criterioNombre, criterioDepartamento, criterioActivo);
		return "";
	}

	public void setPersistenciaBean(TipoUsuarioBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}
	public void setTipoUsuarioSeleccionado(TipoUsuarioDTO tipoUsuarioSeleccionado) {
		this.tipoUsuarioSeleccionado = tipoUsuarioSeleccionado;
	}

	public void setTipoUsuariosSeleccionados(List<TipoUsuarioDTO> tipoUsuariosSeleccionados) {
		this.tipoUsuariosSeleccionados = tipoUsuariosSeleccionados;
	}


	public List<SelectItem> gettipoUsuariosItem() throws PersistenciaException, ServiciosException {
		List<TipoUsuarioDTO> l = persistenciaBean.obtenerTipoUsuarioTodos(); 
		tipoUsuariosItem.clear();
		for(int i=0; i<l.size(); i++) {
			SelectItem item = new SelectItem(l.get(i).getId(),l.get(i).getNombre());
			tipoUsuariosItem.add(item);
		}
		return tipoUsuariosItem;
	}

	public void settipoUsuariosItem(List<SelectItem> tipoUsuariosItem) {
		this.tipoUsuariosItem = tipoUsuariosItem;
	}
	
	
	
	
}
