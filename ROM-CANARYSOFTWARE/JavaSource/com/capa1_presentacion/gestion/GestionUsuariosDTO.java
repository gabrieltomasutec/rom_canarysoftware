package com.capa1_presentacion.gestion;


import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.UsuarioDTO;
import com.capa2_businessLogic.services.UsuariosBO;
import com.capa3_persistence.exception.PersistenciaException;

import javax.enterprise.context.SessionScoped;	//JEE8

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;


//@ManagedBean(name="usuario")

@Named(value="gestionUsuarios")		//JEE8
@SessionScoped				        //JEE8
public class GestionUsuariosDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	UsuariosBO persistenciaBean;
	/**
	 * 
	 */
	
	private String criterioNombre;
	private String criterioDepartamento;
	private Boolean criterioActivo;
	
	private List<UsuarioDTO> usuariosSeleccionados;
	private UsuarioDTO usuarioSeleccionado;
	
	public GestionUsuariosDTO() {
		super();
	}
	
	public Boolean getCriterioActivo() {
		return criterioActivo;
	}
	
	
	public String getCriterioDepartamento() {
		return criterioDepartamento;
	}
	
	// ******** Getters & Setters****************************
	
	public String getCriterioNombre() {
		return criterioNombre;
	}
	public UsuariosBO getPersistenciaBean() {
		return persistenciaBean;
	}
	public UsuarioDTO getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}
	public List<UsuarioDTO> getUsuariosSeleccionados() {
		return usuariosSeleccionados;
	}
	// ********Acciones****************************
	public String seleccionarUsuarios() throws Exception {
		usuariosSeleccionados=persistenciaBean.seleccionarUsuarios(criterioNombre, criterioDepartamento, criterioActivo);
		return "";
	}
	public void setCriterioActivo(Boolean criterioActivo) {
		this.criterioActivo = criterioActivo;
	}
	public void setCriterioDepartamento(String criterioDepartamento) {
		this.criterioDepartamento = criterioDepartamento;
	}
	public void setCriterioNombre(String criterioNombre) {
		this.criterioNombre = criterioNombre;
	}
	public void setPersistenciaBean(UsuariosBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}
	public void setUsuarioSeleccionado(UsuarioDTO usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}

	public void setUsuariosSeleccionados(List<UsuarioDTO> usuariosSeleccionados) {
		this.usuariosSeleccionados = usuariosSeleccionados;
	}

	public String verDatosUsuario() {
		//Navegamos a datos usuario
		return "DatosUsuario";
	}
	
	
	
	
}
