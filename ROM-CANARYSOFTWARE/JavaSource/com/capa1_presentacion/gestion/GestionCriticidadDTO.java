package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CriticidadDTO;
import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa2_businessLogic.services.CriticidadBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Named(value="gestionCriticidad")		//JEE8
@SessionScoped
public class GestionCriticidadDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	CriticidadBO persistenciaBean;
	
	private List<CriticidadDTO> criticidadSeleccionados;
	private CriticidadDTO criticidadSeleccionado;
	private List<SelectItem> criticidadItem = new ArrayList<SelectItem>();
	private List<SelectItem> tipoCriticidadItem = new ArrayList<SelectItem>();
	private String criterioBusqueda;
	
	public GestionCriticidadDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String seleccionarCriticidad() throws Exception {
		criticidadSeleccionados=persistenciaBean.seleccionarCriticidad(criterioBusqueda);
		return "";
	}

	public List<SelectItem> getTipoCriticidadItem() {
		return tipoCriticidadItem;
	}

	public void setTipoCriticidadItem(List<SelectItem> tipoCriticidadItem) {
		this.tipoCriticidadItem = tipoCriticidadItem;
	}

	public String getCriterioBusqueda() {
		return criterioBusqueda;
	}

	public void setCriterioBusqueda(String criterioBusqueda) {
		this.criterioBusqueda = criterioBusqueda;
	}

	public CriticidadBO getPersistenciaBean() {
		return persistenciaBean;
	}

	public void setPersistenciaBean(CriticidadBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}

	public List<CriticidadDTO> getCriticidadSeleccionados() {
		return criticidadSeleccionados;
	}

	public void setCriticidadSeleccionados(List<CriticidadDTO> criticidadSeleccionados) {
		this.criticidadSeleccionados = criticidadSeleccionados;
	}

	public CriticidadDTO getCriticidadSeleccionado() {
		return criticidadSeleccionado;
	}

	public void setCriticidadSeleccionado(CriticidadDTO criticidadSeleccionado) {
		this.criticidadSeleccionado = criticidadSeleccionado;
	}

	public List<SelectItem> getCriticidadItem() throws PersistenciaException, ServiciosException{
		List<CriticidadDTO> f = persistenciaBean.obtenerCriticidadTodos(); 
		criticidadItem.clear();
		for(int i=0; i<f.size(); i++) {
			SelectItem item = new SelectItem(f.get(i).getIdCriticidad(),f.get(i).getNombre());
			criticidadItem.add(item);
		}
		return criticidadItem;
	}

	public void setCriticidadItem(List<SelectItem> criticidadItem) {
		this.criticidadItem = criticidadItem;
	}
	
	

	
	public List<SelectItem> gettipoCriticidadItem() throws PersistenciaException, ServiciosException {
		List<CriticidadDTO> l = persistenciaBean.obtenerCriticidadTodos(); 
		tipoCriticidadItem.clear();
		for(int i=0; i<l.size(); i++) {
			SelectItem item = new SelectItem(l.get(i).getIdCriticidad(),l.get(i).getNombre());
			tipoCriticidadItem.add(item);
		}
		return tipoCriticidadItem;
	}

}
