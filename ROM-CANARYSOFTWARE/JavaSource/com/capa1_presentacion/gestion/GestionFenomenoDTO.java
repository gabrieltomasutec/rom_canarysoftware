package com.capa1_presentacion.gestion;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.FenomenosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

@Named(value="gestionFenomeno")		//JEE8
@SessionScoped
public class GestionFenomenoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	FenomenosBO persistenciaBean;
	
	private Long idFenomeno;
	private String modalidad;
	private FenomenosDTO fenomenoSeleccionado;
	private boolean modoEdicion=false;
	
	public GestionFenomenoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public FenomenosBO getPersistenciaBean() {
		return persistenciaBean;
	}



	public void setPersistenciaBean(FenomenosBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}



	public Long getIdFenomeno() {
		return idFenomeno;
	}



	public void setIdFenomeno(Long idFenomeno) {
		this.idFenomeno = idFenomeno;
	}



	public String getModalidad() {
		return modalidad;
	}



	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}



	public FenomenosDTO getFenomenoSeleccionado() {
		return fenomenoSeleccionado;
	}



	public void setFenomenoSeleccionado(FenomenosDTO fenomenoSeleccionado) {
		this.fenomenoSeleccionado = fenomenoSeleccionado;
	}



	public boolean isModoEdicion() {
		return modoEdicion;
	}



	public void setModoEdicion(boolean modoEdicion) {
		this.modoEdicion = modoEdicion;
	}



	//acciones
	public String cambiarModalidadUpdate() throws CloneNotSupportedException {
	
		return "DatosFenomeno?faces-redirect=true&includeViewParams=true";
	
	}

	public void preRenderViewListener() {
			
			if (idFenomeno!=null) {
				try {
					fenomenoSeleccionado=persistenciaBean.buscarFenomeno(idFenomeno);
				} catch (ServiciosException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				fenomenoSeleccionado=new FenomenosDTO();
			}
			if (modalidad.contentEquals("view")) {
				modoEdicion=false;
			}else if (modalidad.contentEquals("update")) {
				modoEdicion=true;
			}else if (modalidad.contentEquals("insert")) {
				modoEdicion=true;
			}else {
				modoEdicion=false;
				modalidad="view";
		
			}
		}
	
	public String borrar() {
		
		try {
			persistenciaBean.borrarFenomeno(fenomenoSeleccionado);

			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha eliminado el fenomeno.",""));
			
			this.idFenomeno = null;			
			this.modalidad="insert";				
			
		} catch (PersistenciaException | ServiciosException e) {

			String msg1=e.getMessage();

			FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1,"");
			FacesContext.getCurrentInstance().addMessage(null, facesMsg);
		
			this.modalidad="update";
		
			e.printStackTrace();
		}
		return "";
	}
	
	//Pasar a modo 
	public String salvarCambios() {
		
		if (fenomenoSeleccionado.getIdFenomeno()==0) {
			
				
			FenomenosDTO fenomenoNuevo;
			
			try {
				fenomenoNuevo = (FenomenosDTO) persistenciaBean.agregarFenomeno(fenomenoSeleccionado);
				this.idFenomeno=fenomenoNuevo.getIdFenomeno();
			
				//mensaje de actualizacion correcta
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado un nuevo fenomeno", "");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
				this.modalidad="view";
			
				
			} catch (PersistenciaException | ServiciosException e) {
				
				Throwable rootException=ExceptionsTools.getCause(e);
				String msg1=e.getMessage();
				String msg2=ExceptionsTools.formatedMsg(rootException);
				
				if (msg2.contains("NAME_FENOMENO")) {
					msg1 = msg1 + " Fenomeno ya existente.";
				}
				
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
				this.modalidad="update";
				
				e.printStackTrace();
			}
			
			
			 
		} else if (modalidad.equals("update")) {
			
			try {
				persistenciaBean.actualizarFenomeno(fenomenoSeleccionado);

				FacesContext.getCurrentInstance().addMessage(null, 
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado el fenomeno.",""));
				
			} catch (PersistenciaException | ServiciosException e) {

				//Throwable rootException=ExceptionsTools.getCause(e);
				String msg1=e.getMessage();
				//String msg2=ExceptionsTools.formatedMsg(e.getCause());
				String msg2="";
				//mensaje de actualizacion correcta
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
				this.modalidad="update";
			
				e.printStackTrace();
			}
		}
		return "";
	}
}
