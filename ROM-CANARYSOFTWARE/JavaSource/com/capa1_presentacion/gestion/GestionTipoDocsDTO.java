package com.capa1_presentacion.gestion;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.TipoDocDTO;
import com.capa2_businessLogic.services.TipoDocBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;	//JEE8
import javax.faces.model.SelectItem;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;


//@ManagedBean(name="usuario")

@Named(value="gestionTipoDocs")		//JEE8
@SessionScoped				        //JEE8
public class GestionTipoDocsDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	TipoDocBO persistenciaBean;
	/**
	 * 
	 */

	
	private List<TipoDocDTO> tipoDocSeleccionados;
	private TipoDocDTO tipoDocSeleccionado;
	private List<SelectItem> tipoDocsItem = new ArrayList<SelectItem>();
	
	public GestionTipoDocsDTO() {
		super();
	}
	
	
	// ******** Getters & Setters****************************
	
	
	public List<SelectItem> getTipoDocsItem() throws PersistenciaException, ServiciosException {
		List<TipoDocDTO> l = persistenciaBean.obtenerTipoDocTodos(); 
		tipoDocsItem.clear();
		for(int i=0; i<l.size(); i++) {
			SelectItem item = new SelectItem(l.get(i).getId(),l.get(i).getNombre());
			tipoDocsItem.add(item);
		}
		return tipoDocsItem;
	}

	public TipoDocBO getPersistenciaBean() {
		return persistenciaBean;
	}

	public void setPersistenciaBean(TipoDocBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}

	public List<TipoDocDTO> getTipoDocSeleccionados() {
		return tipoDocSeleccionados;
	}

	public void setTipoDocSeleccionados(List<TipoDocDTO> tipoDocSeleccionados) {
		this.tipoDocSeleccionados = tipoDocSeleccionados;
	}

	public TipoDocDTO getTipoDocSeleccionado() {
		return tipoDocSeleccionado;
	}

	public void setTipoDocSeleccionado(TipoDocDTO tipoDocSeleccionado) {
		this.tipoDocSeleccionado = tipoDocSeleccionado;
	}


	public void setTipoDocsItem(List<SelectItem> tipoDocsItem) {
		this.tipoDocsItem = tipoDocsItem;
	}

	public void settipoDocsItem(List<SelectItem> tipoDocsItem) {
		this.tipoDocsItem = tipoDocsItem;
	}
	
	
	
	
}
