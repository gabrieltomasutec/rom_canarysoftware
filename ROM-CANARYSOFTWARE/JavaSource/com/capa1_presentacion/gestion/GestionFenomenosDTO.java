package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa1_presentacion.bean.TipoCaractDTO;
import com.capa2_businessLogic.services.FenomenosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Named(value="gestionFenomenos")		//JEE8
@SessionScoped
public class GestionFenomenosDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	FenomenosBO persistenciaBean;
	
	private List<FenomenosDTO> fenomenosSeleccionados;
	private FenomenosDTO fenomenoSeleccionado;
	private String criterioNombre;
	private List<SelectItem> fenomenosItem = new ArrayList<SelectItem>();
	
	public GestionFenomenosDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public FenomenosDTO getFenomenoSeleccionado() {
		return fenomenoSeleccionado;
	}
	public List<FenomenosDTO> getFenomenosSeleccionados() {
		return fenomenosSeleccionados;
	}

	public FenomenosBO getPersistenciaBean() {
		return persistenciaBean;
	}

	// ********Acciones****************************
	public String seleccionarFenomenos() throws PersistenciaException {
		fenomenosSeleccionados=persistenciaBean.seleccionarFenomenos();
		return "";
	}

	public void setFenomenoSeleccionado(FenomenosDTO fenomenoSeleccionado) {
		this.fenomenoSeleccionado = fenomenoSeleccionado;
	}

	public void setFenomenosSeleccionados(List<FenomenosDTO> fenomenosSeleccionados) {
		this.fenomenosSeleccionados = fenomenosSeleccionados;
	}

	public void setPersistenciaBean(FenomenosBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}

	public String verDatosFenomeno() {
		//Navegamos a datos 
		return "DatosFenomeno";
	}

	public String getCriterioNombre() {
		return criterioNombre;
	}

	public void setCriterioNombre(String criterioNombre) {
		this.criterioNombre = criterioNombre;
	}
	
	public String seleccionarFenomenosPorNombre() throws PersistenciaException {
		fenomenosSeleccionados = persistenciaBean.seleccionarPorNombre(criterioNombre);
		return "";
	}

	public List<SelectItem> getFenomenoItem() throws PersistenciaException {
		List<FenomenosDTO> f = persistenciaBean.obtenerFenomenosTodos(); 
		fenomenosItem.clear();
		for(int i=0; i<f.size(); i++) {
			SelectItem item = new SelectItem(f.get(i).getIdFenomeno(),f.get(i).getNombre());
			fenomenosItem.add(item);
		}
		return fenomenosItem;
	}

	public void setFenomenoItem(List<SelectItem> fenomenoItem) {
		this.fenomenosItem = fenomenoItem;
	}
	
	private List<SelectItem> tipoCaractsItem = new ArrayList<SelectItem>();

	
	public List<SelectItem> gettipoCaractsItem() throws PersistenciaException, ServiciosException {
		List<FenomenosDTO> l = persistenciaBean.obtenerFenomenosTodos(); 
		tipoCaractsItem.clear();
		for(int i=0; i<l.size(); i++) {
			SelectItem item = new SelectItem(l.get(i).getIdFenomeno(),l.get(i).getNombre());
			tipoCaractsItem.add(item);
		}
		return tipoCaractsItem;
	}
	
	
}
