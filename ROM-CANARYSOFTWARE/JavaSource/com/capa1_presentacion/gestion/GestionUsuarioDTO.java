package com.capa1_presentacion.gestion;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import java.io.Serializable;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.UsuarioDTO;
import com.capa2_businessLogic.services.UsuariosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;	//JEE8
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;


//@ManagedBean(name="usuario")

@Named(value="gestionUsuario")		//JEE8
@SessionScoped				        //JEE8
public class GestionUsuarioDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	UsuariosBO persistenciaBean;
	
	private Long id;
	private String modalidad;
	
	private UsuarioDTO usuarioSeleccionado;
	private boolean modoEdicion=false;
	
	public GestionUsuarioDTO() {
		super();
	}
	//acciones
	public String cambiarModalidadUpdate() throws CloneNotSupportedException {
		//this.modalidad="update";
		return "DatosUsuario?faces-redirect=true&includeViewParams=true";
		
	}
	
	

	public Long getId() {
		return id;
	}
	public String getModalidad() {
		return modalidad;
	}
	public boolean getModoEdicion() {
		return modoEdicion;
	}
	
	//getters y setters
	public UsuariosBO getPersistenciaBean() {
		return persistenciaBean;
	}
	public UsuarioDTO getUsuarioSeleccionado() {
		return usuarioSeleccionado;
	}
	@PostConstruct
	public void init() {
		//usuarioSeleccionado=new Usuario();
	}
	public boolean isModoEdicion() {
		return modoEdicion;
	}
	//se ejecuta antes de desplegar la vista
	public void preRenderViewListener() throws Exception {
		
		if (id!=null) {
			try {
				usuarioSeleccionado=persistenciaBean.buscarUsuario(id);
			} catch (ServiciosException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			usuarioSeleccionado=new UsuarioDTO();
		}
		if (modalidad.contentEquals("view")) {
			modoEdicion=false;
		}else if (modalidad.contentEquals("update")) {
			modoEdicion=true;
		}else if (modalidad.contentEquals("insert")) {
			modoEdicion=true;
		}else {
			modoEdicion=false;
			modalidad="view";
	
		}
	}
	//Pasar a modo 
	public String salvarCambios() throws Exception {
		
//		System.out.println ("salvar cambios "+usuarioSeleccionado.toString());
		
		if (usuarioSeleccionado.getId()==0) {
			
			//usuarioSeleccionado.setEstado(1);
			
			UsuarioDTO usuarioNuevo;
			try {
				usuarioNuevo = (UsuarioDTO) persistenciaBean.agregarUsuario(usuarioSeleccionado);
				this.id=usuarioNuevo.getId();
			
				//mensaje de actualizacion correcta
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado un nuevo Usuario", "");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
				this.modalidad="view";
			
				
			} catch (PersistenciaException | ServiciosException e) {
				
				Throwable rootException=ExceptionsTools.getCause(e);
				String msg1=e.getMessage();
				String msg2=ExceptionsTools.formatedMsg(rootException);
				
				if (msg2.contains("DOC_USUARIO")) {
					msg1 = msg1 + " N�mero de Documento ya existente.";
				}
				
				if (msg2.contains("NAME_USUARIO")) {
					msg1 = msg1 + " Usuario ya existente.";
				}
				
				if (msg1.contains("Error")) {
					msg2 = "";
				}
				
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
				this.modalidad="update";
				
				e.printStackTrace();
			}
			
			
			 
		//} else if (modalidad.equals("update")) {
		} else  {
			try {
				String retorno = persistenciaBean.actualizarUsuario(usuarioSeleccionado);
				if(retorno == "OK") {
					FacesContext.getCurrentInstance().addMessage(null, 
							new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado Usuario.",""));
					this.modalidad="view";
				}else {
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,retorno, "");
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				}

				
			} catch (PersistenciaException | ServiciosException e) {
				Throwable rootException=ExceptionsTools.getCause(e);
				String msg1=e.getMessage();
				//String msg2=ExceptionsTools.formatedMsg(e.getCause());
				String msg2="";
				//mensaje de actualizacion correcta
				//FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
				this.modalidad="update";
			
				e.printStackTrace();
			}
		}
		return "";
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}
	public void setModoEdicion(boolean modoEdicion) {
		this.modoEdicion = modoEdicion;
	}
	public void setPersistenciaBean(UsuariosBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}
	public void setUsuarioSeleccionado(UsuarioDTO usuarioSeleccionado) {
		this.usuarioSeleccionado = usuarioSeleccionado;
	}
		
	
	
	
}
