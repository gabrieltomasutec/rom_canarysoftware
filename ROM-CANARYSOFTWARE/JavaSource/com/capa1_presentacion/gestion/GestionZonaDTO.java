package com.capa1_presentacion.gestion;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.ZonasDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.ZonasBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

@Named(value="gestionZona")		//JEE8
@SessionScoped
public class GestionZonaDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	ZonasBO persistenciaBean;
	private Long id;
	private String modalidad;
	private ZonasDTO zonaSeleccionado;
	
	private boolean modoEdicion=false;

	public GestionZonaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String salvarCambios() {
		
		if (zonaSeleccionado.getIdZona()==0) {
			
					
			ZonasDTO zonaNuevo;
			try {
				zonaNuevo = (ZonasDTO) persistenciaBean.agregarZona(zonaSeleccionado);
				this.id=zonaNuevo.getIdZona();
			
				//mensaje de actualizacion correcta
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado una nueva Zona", "");
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
				this.modalidad="view";
			
				
			} catch (PersistenciaException | ServiciosException e) {
				
				Throwable rootException=ExceptionsTools.getCause(e);
				String msg1=e.getMessage();
				String msg2=ExceptionsTools.formatedMsg(rootException);
				//mensaje de actualizacion correcta
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
				this.modalidad="update";
				
				e.printStackTrace();
			}
			
			
			 
		} else if (modalidad.equals("update")) {
			
			try {
				persistenciaBean.actualizarZona(zonaSeleccionado);

				FacesContext.getCurrentInstance().addMessage(null, 
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado la Zona.",""));
				
			} catch (PersistenciaException | ServiciosException e) {

				Throwable rootException=ExceptionsTools.getCause(e);
				String msg1=e.getMessage();
				String msg2=ExceptionsTools.formatedMsg(e.getCause());
				//mensaje de actualizacion correcta
				FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
				FacesContext.getCurrentInstance().addMessage(null, facesMsg);
			
				this.modalidad="update";
			
				e.printStackTrace();
			}
		}
		return "";
	}
	public void preRenderViewListener() {
		
		if (id!=null) {
			try {
				zonaSeleccionado=persistenciaBean.buscarZona(id);
			} catch (ServiciosException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			zonaSeleccionado=new ZonasDTO();
		}
		if (modalidad.contentEquals("view")) {
			modoEdicion=false;
		}else if (modalidad.contentEquals("update")) {
			modoEdicion=true;
		}else if (modalidad.contentEquals("insert")) {
			modoEdicion=true;
		}else {
			modoEdicion=false;
			modalidad="view";
	
		}
	}
	public String cambiarModalidadUpdate() throws CloneNotSupportedException {
		
		return "DatosZona?faces-redirect=true&includeViewParams=true";
		
	}

	public ZonasBO getPersistenciaBean() {
		return persistenciaBean;
	}

	public void setPersistenciaBean(ZonasBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public ZonasDTO getZonaSeleccionado() {
		return zonaSeleccionado;
	}

	public void setZonaSeleccionado(ZonasDTO zonaSeleccionado) {
		this.zonaSeleccionado = zonaSeleccionado;
	}

	public boolean isModoEdicion() {
		return modoEdicion;
	}

	public void setModoEdicion(boolean modoEdicion) {
		this.modoEdicion = modoEdicion;
	}
	
	

}
