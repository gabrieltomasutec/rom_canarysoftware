package com.capa1_presentacion.gestion;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.UsuariosBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;
import com.utils.ExceptionsTools;

@Named(value="gestionCaracteristica")		//JEE8
@SessionScoped
public class GestionCaracteristicaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Inject
	CaracteristicasBO persistenciaBean;
	private Long id;
	private String modalidad;
	private CaracteristicasDTO caracteristicaSeleccionado;
	
	private boolean modoEdicion=false;
	public GestionCaracteristicaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	//Pasar a modo 
		public String salvarCambios() {
			
			if (caracteristicaSeleccionado.getId()==0) {
				
				//caracteristicaSeleccionado.setEstado("Activo");
				
				CaracteristicasDTO caracteristicaNuevo;
				try {
					caracteristicaNuevo = (CaracteristicasDTO) persistenciaBean.agregarCaracteristica(caracteristicaSeleccionado);
					this.id=caracteristicaNuevo.getId();
				
					//mensaje de actualizacion correcta
					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha agregado una nueva caracteristica", "");
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
					
					this.modalidad="view";
				
					
				} catch (PersistenciaException | ServiciosException e) {
					
					Throwable rootException=ExceptionsTools.getCause(e);
					String msg1=e.getMessage();
					String msg2=ExceptionsTools.formatedMsg(rootException);
					
					if (msg2.contains("NAME_CARACT")) {
						msg1 = "No se pudo agregar la CaracterÝstica. CaracterÝstica ya existente.";
					}

					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
					this.modalidad="update";
					
					e.printStackTrace();
				}
				
				
				 
			} else if (modalidad.equals("update")) {
				
				try {
					persistenciaBean.actualizarCaracteristica(caracteristicaSeleccionado);

					FacesContext.getCurrentInstance().addMessage(null, 
							new FacesMessage(FacesMessage.SEVERITY_INFO, "Se ha modificado la caracteristica.",""));
					
				} catch (PersistenciaException | ServiciosException e) {

					Throwable rootException=ExceptionsTools.getCause(e);
					String msg1=e.getMessage();
					String msg2=ExceptionsTools.formatedMsg(e.getCause());

					FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg1, msg2);
					FacesContext.getCurrentInstance().addMessage(null, facesMsg);
				
					this.modalidad="update";
				
					e.printStackTrace();
				}
			}
			return "";
		}

		//(NO TIENE) SE AGREGO ABM DEFINIDO EN CARACTERISTICASBEAN
		public void preRenderViewListener() {
			
			if (id!=null) {
				try {
					caracteristicaSeleccionado=persistenciaBean.buscarCaracteristica(id);
				} catch (ServiciosException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				caracteristicaSeleccionado=new CaracteristicasDTO();
			}
			if (modalidad.contentEquals("view")) {
				modoEdicion=false;
			}else if (modalidad.contentEquals("update")) {
				modoEdicion=true;
			}else if (modalidad.contentEquals("insert")) {
				modoEdicion=true;
			}else {
				modoEdicion=false;
				modalidad="view";
		
			}
		}
		//acciones
		public String cambiarModalidadUpdate() throws CloneNotSupportedException {
			
			return "DatosCaracteristica?faces-redirect=true&includeViewParams=true";
			
		}

		public CaracteristicasDTO getCaracteristicaSeleccionado() {
			return caracteristicaSeleccionado;
		}

		public Long getId() {
			return id;
		}

		public String getModalidad() {
			return modalidad;
		}

		public CaracteristicasBO getPersistenciaBean() {
			return persistenciaBean;
		}

		public boolean isModoEdicion() {
			return modoEdicion;
		}

		public void setCaracteristicaSeleccionado(CaracteristicasDTO caracteristicaSeleccionado) {
			this.caracteristicaSeleccionado = caracteristicaSeleccionado;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public void setModalidad(String modalidad) {
			this.modalidad = modalidad;
		}

		public void setModoEdicion(boolean modoEdicion) {
			this.modoEdicion = modoEdicion;
		}

		public void setPersistenciaBean(CaracteristicasBO persistenciaBean) {
			this.persistenciaBean = persistenciaBean;
		}
		
		
	
}
