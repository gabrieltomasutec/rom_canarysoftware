package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.FenomenosDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.UsuariosBO;
import com.capa3_persistence.exception.PersistenciaException;

@Named(value="gestionCaracteristicas")		//JEE8
@SessionScoped
public class GestionCaracteristicasDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Inject
	CaracteristicasBO persistenciaBean;
	private List<CaracteristicasDTO> caracteristicasSeleccionados;
	
	private CaracteristicasDTO caracteristicaSeleccionado;
	
	private String criterioNombre;
	
	private List<SelectItem> caractItem = new ArrayList<SelectItem>();
	
	
	// ********Acciones****************************
		public String seleccionarCaracteristicas() throws PersistenciaException {
			caracteristicasSeleccionados=persistenciaBean.seleccionarCaracteristicas();
			return "";
		}
		
		
		public GestionCaracteristicasDTO() {
			super();
			// TODO Auto-generated constructor stub
		}
		public CaracteristicasDTO getCaracteristicaSeleccionado() {
			return caracteristicaSeleccionado;
		}

		public List<CaracteristicasDTO> getCaracteristicasSeleccionados() {
			return caracteristicasSeleccionados;
		}

		//Getters y Setters
		public CaracteristicasBO getPersistenciaBean() {
			return persistenciaBean;
		}

		public void setCaracteristicaSeleccionado(CaracteristicasDTO caracteristicaSeleccionado) {
			this.caracteristicaSeleccionado = caracteristicaSeleccionado;
		}

		public void setCaracteristicasSeleccionados(List<CaracteristicasDTO> caracteristicasSeleccionados) {
			this.caracteristicasSeleccionados = caracteristicasSeleccionados;
		}

		public void setPersistenciaBean(CaracteristicasBO persistenciaBean) {
			this.persistenciaBean = persistenciaBean;
		}

		public String verDatosCaracteristica() {
			//Navegamos a datos 
			return "DatosCaracteristica";
		}
		
		public String getCriterioNombre() {
			return criterioNombre;
		}
		
		public void setCriterioNombre(String criterioNombre) {
			this.criterioNombre = criterioNombre;
		}
		
		public List<SelectItem> getCaractItem() throws PersistenciaException {
			List<CaracteristicasDTO> f = persistenciaBean.obtenerCaracteristicasTodos();
			caractItem.clear();
			for(int i=0; i<f.size(); i++) {
				SelectItem item = new SelectItem(f.get(i).getId(),f.get(i).getNombre());
				caractItem.add(item);
			}
			return caractItem;
		}

		public void setCaractItem(List<SelectItem> caractItem) {
			this.caractItem = caractItem;
		}



		
	
	
}
