package com.capa1_presentacion.gestion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import com.capa1_presentacion.bean.CaracteristicasDTO;
import com.capa1_presentacion.bean.DepartamentosDTO;
import com.capa1_presentacion.bean.ZonasDTO;
import com.capa2_businessLogic.services.CaracteristicasBO;
import com.capa2_businessLogic.services.ZonasBO;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Named(value="gestionZonas")		//JEE8
@SessionScoped
public class GestionZonasDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	ZonasBO persistenciaBean;
	private List<ZonasDTO> zonasSeleccionados;
	
	private ZonasDTO zonaSeleccionado;
	
	private String criterioNombre;
	
	private List<SelectItem> zonasItem = new ArrayList<SelectItem>();
	
	public String verDatosZona() {
		//Navegamos a datos 
		return "DatosZona";
	}
	
	public String seleccionarZonas() throws PersistenciaException {
		zonasSeleccionados=persistenciaBean.seleccionarZonas();
		return "";
	}

	public GestionZonasDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public List<SelectItem> getzonasItem() throws PersistenciaException, ServiciosException {
		List<ZonasDTO> l = persistenciaBean.obtenerZonasTodos(); 
		zonasItem.clear();
		for(int i=0; i<l.size(); i++) {
			SelectItem item = new SelectItem(l.get(i).getIdZona(),l.get(i).getNombre());
			zonasItem.add(item);
		}
		return zonasItem;
	}

	public ZonasBO getPersistenciaBean() {
		return persistenciaBean;
	}

	public void setPersistenciaBean(ZonasBO persistenciaBean) {
		this.persistenciaBean = persistenciaBean;
	}

	public List<ZonasDTO> getZonasSeleccionados() {
		return zonasSeleccionados;
	}

	public void setZonasSeleccionados(List<ZonasDTO> zonasSeleccionados) {
		this.zonasSeleccionados = zonasSeleccionados;
	}

	public ZonasDTO getZonaSeleccionado() {
		return zonaSeleccionado;
	}

	public void setZonaSeleccionado(ZonasDTO zonaSeleccionado) {
		this.zonaSeleccionado = zonaSeleccionado;
	}

	public String getCriterioNombre() {
		return criterioNombre;
	}

	public void setCriterioNombre(String criterioNombre) {
		this.criterioNombre = criterioNombre;
	}
	
	

}
