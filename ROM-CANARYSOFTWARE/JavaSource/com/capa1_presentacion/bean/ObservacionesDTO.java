package com.capa1_presentacion.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class ObservacionesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@NotNull
	private long idObservacion;
	@NotNull
	private String descripcion;
	@NotNull
	private String latitud;
	@NotNull
	private String longitud;
	@NotNull
	private long altitud;
	//@NotNull
	private Date fechaHora;
	private String fechaHoraString;
	//@NotNull
	private Date validFecha;
	private String validFechaString;
	//@NotNull
	private String validComents;
	@NotNull
	private long idCriticidad;
	private String idCriticidadNombre;
	@NotNull
	private long idFenomeno;
	private String idFenomenoNombre;
	@NotNull
	private long idLocalidad;
	private String idLocalidadNombre;
	@NotNull
	private long idUsuarioAlta;
	private String idUsuarioAltaNombre;
	
	//@NotNull
	private long idUsuarioVali;
	private String idUsuarioValiNombre;
	
	private long idCaract;
	private String idCaractNombre;
	
	private ArrayList<ObsCaracteristicasDTO> listaCaract;
	
	private long datoNumeroCaract;  
	private String datoTextoCaract;

	public ObservacionesDTO() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ObservacionesDTO(@NotNull long idObservacion, @NotNull String descripcion, @NotNull String latitud,
			@NotNull String longitud, @NotNull long altitud, Date fechaHora, String fechaHoraString, Date validFecha,
			String validFechaString, String validComents, @NotNull long idCriticidad, String idCriticidadNombre,
			@NotNull long idFenomeno, String idFenomenoNombre, @NotNull long idLocalidad, String idLocalidadNombre,
			@NotNull long idUsuarioAlta, String idUsuarioAltaNombre, long idUsuarioVali, String idUsuarioValiNombre) {
		super();
		this.idObservacion = idObservacion;
		this.descripcion = descripcion;
		this.latitud = latitud;
		this.longitud = longitud;
		this.altitud = altitud;
		this.fechaHora = fechaHora;
		this.fechaHoraString = fechaHoraString;
		this.validFecha = validFecha;
		this.validFechaString = validFechaString;
		this.validComents = validComents;
		this.idCriticidad = idCriticidad;
		this.idCriticidadNombre = idCriticidadNombre;
		this.idFenomeno = idFenomeno;
		this.idFenomenoNombre = idFenomenoNombre;
		this.idLocalidad = idLocalidad;
		this.idLocalidadNombre = idLocalidadNombre;
		this.idUsuarioAlta = idUsuarioAlta;
		this.idUsuarioAltaNombre = idUsuarioAltaNombre;
		this.idUsuarioVali = idUsuarioVali;
		this.idUsuarioValiNombre = idUsuarioValiNombre;
	}







	public long getAltitud() {
		return altitud;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public long getIdCriticidad() {
		return idCriticidad;
	}

	public long getIdFenomeno() {
		return idFenomeno;
	}

	public long getIdLocalidad() {
		return idLocalidad;
	}

	public long getIdObservacion() {
		return idObservacion;
	}

	public long getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public long getIdUsuarioVali() {
		return idUsuarioVali;
	}

	public String getLatitud() {
		return latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public String getValidComents() {
		return validComents;
	}

	public Date getValidFecha() {
		return validFecha;
	}

	public void setAltitud(long altitud) {
		this.altitud = altitud;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public void setIdCriticidad(long idCriticidad) {
		this.idCriticidad = idCriticidad;
	}

	public void setIdFenomeno(long idFenomeno) {
		this.idFenomeno = idFenomeno;
	}

	public void setIdLocalidad(long idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public void setIdObservacion(long idObservacion) {
		this.idObservacion = idObservacion;
	}

	public void setIdUsuarioAlta(long idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public void setIdUsuarioVali(long idUsuarioVali) {
		this.idUsuarioVali = idUsuarioVali;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public void setValidComents(String validComents) {
		this.validComents = validComents;
	}

	public void setValidFecha(Date validFecha) {
		this.validFecha = validFecha;
	}

	public String getFechaHoraString() {
		return fechaHoraString;
	}

	public void setFechaHoraString(String fechaHoraString) {
		this.fechaHoraString = fechaHoraString;
	}

	public String getValidFechaString() {
		return validFechaString;
	}

	public void setValidFechaString(String validFechaString) {
		this.validFechaString = validFechaString;
	}



	public String getIdCriticidadNombre() {
		return idCriticidadNombre;
	}



	public void setIdCriticidadNombre(String idCriticidadNombre) {
		this.idCriticidadNombre = idCriticidadNombre;
	}



	public String getIdFenomenoNombre() {
		return idFenomenoNombre;
	}



	public void setIdFenomenoNombre(String idFenomenoNombre) {
		this.idFenomenoNombre = idFenomenoNombre;
	}



	public String getIdLocalidadNombre() {
		return idLocalidadNombre;
	}



	public void setIdLocalidadNombre(String idLocalidadNombre) {
		this.idLocalidadNombre = idLocalidadNombre;
	}



	public String getIdUsuarioAltaNombre() {
		return idUsuarioAltaNombre;
	}



	public void setIdUsuarioAltaNombre(String idUsuarioAltaNombre) {
		this.idUsuarioAltaNombre = idUsuarioAltaNombre;
	}



	public String getIdUsuarioValiNombre() {
		return idUsuarioValiNombre;
	}



	public void setIdUsuarioValiNombre(String idUsuarioValiNombre) {
		this.idUsuarioValiNombre = idUsuarioValiNombre;
	}


	@Override
	public String toString() {
		return "ObservacionesDTO [idObservacion=" + idObservacion + ", descripcion=" + descripcion + ", latitud="
				+ latitud + ", longitud=" + longitud + ", altitud=" + altitud + ", fechaHora=" + fechaHora
				+ ", fechaHoraString=" + fechaHoraString + ", validFecha=" + validFecha + ", validFechaString="
				+ validFechaString + ", validComents=" + validComents + ", idCriticidad=" + idCriticidad
				+ ", idCriticidadNombre=" + idCriticidadNombre + ", idFenomeno=" + idFenomeno + ", idFenomenoNombre="
				+ idFenomenoNombre + ", idLocalidad=" + idLocalidad + ", idLocalidadNombre=" + idLocalidadNombre
				+ ", idUsuarioAlta=" + idUsuarioAlta + ", idUsuarioAltaNombre=" + idUsuarioAltaNombre
				+ ", idUsuarioVali=" + idUsuarioVali + ", idUsuarioValiNombre=" + idUsuarioValiNombre + ", idCaract="
				+ idCaract + ", idCaractNombre=" + idCaractNombre + ", listaCaract=" + listaCaract
				+ ", datoNumeroCaract=" + datoNumeroCaract + ", datoTextoCaract=" + datoTextoCaract + "]";
	}



	public long getIdCaract() {
		return idCaract;
	}



	public void setIdCaract(long idCcaract) {
		this.idCaract = idCcaract;
	}



	public String getIdCaractNombre() {
		return idCaractNombre;
	}



	public void setIdCaractNombre(String idCaractNombre) {
		this.idCaractNombre = idCaractNombre;
	}



	public ArrayList<ObsCaracteristicasDTO> getListaCaract() {
		if(this.listaCaract == null) {
			this.listaCaract = new ArrayList<ObsCaracteristicasDTO>();
		}
		return listaCaract;
	}



	public void setListaCaract(ArrayList<ObsCaracteristicasDTO> listaCaract) {
		this.listaCaract = listaCaract;
	}



	public long getDatoNumeroCaract() {
		return datoNumeroCaract;
	}



	public void setDatoNumeroCaract(long datoNumeroCaract) {
		this.datoNumeroCaract = datoNumeroCaract;
	}



	public String getDatoTextoCaract() {
		return datoTextoCaract;
	}



	public void setDatoTextoCaract(String datoTextoCaract) {
		this.datoTextoCaract = datoTextoCaract;
	}



	
	
	
	
}
