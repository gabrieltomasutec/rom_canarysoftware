package com.capa1_presentacion.bean;

import java.io.Serializable;

public class CaracteristicasDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private long id;
	private String nombre;
	private String etiqueta;
	private long tipoCaract;
	private String tipoCaractNombre;
	private long fenomeno;
	private String fenomenoNombre;
	
	public CaracteristicasDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CaracteristicasDTO(long id, String nombre, String etiqueta, long tipoCaract, String tipoCaractNombre,
			long fenomeno, String fenomenoNombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.etiqueta = etiqueta;
		this.tipoCaract = tipoCaract;
		this.tipoCaractNombre = tipoCaractNombre;
		this.fenomeno = fenomeno;
		this.fenomenoNombre = fenomenoNombre;
	}



	public String getEtiqueta() {
		return etiqueta;
	}

	public long getFenomeno() {
		return fenomeno;
	}

	public long getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public long getTipoCaract() {
		return tipoCaract;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public void setFenomeno(long fenomeno) {
		this.fenomeno = fenomeno;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTipoCaract(long tipoCaract) {
		this.tipoCaract = tipoCaract;
	}

	public String getTipoCaractNombre() {
		return tipoCaractNombre;
	}

	public void setTipoCaractNombre(String tipoCaractNombre) {
		this.tipoCaractNombre = tipoCaractNombre;
	}

	public String getFenomenoNombre() {
		return fenomenoNombre;
	}

	public void setFenomenoNombre(String fenomenoNombre) {
		this.fenomenoNombre = fenomenoNombre;
	}

	@Override
	public String toString() {
		return "CaracteristicasDTO [id=" + id + ", nombre=" + nombre + ", etiqueta=" + etiqueta + ", tipoCaract="
				+ tipoCaract + ", tipoCaractNombre=" + tipoCaractNombre + ", fenomeno=" + fenomeno + ", fenomenoNombre="
				+ fenomenoNombre + "]";
	}
	
	
	
}
