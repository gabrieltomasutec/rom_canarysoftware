package com.capa1_presentacion.bean;

import java.io.Serializable;

public class CriticidadDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long idCriticidad;
	private String nombre;
	
	public CriticidadDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CriticidadDTO(long idCriticidad, String nombre) {
		super();
		this.idCriticidad = idCriticidad;
		this.nombre = nombre;
	}

	public long getIdCriticidad() {
		return idCriticidad;
	}

	public void setIdCriticidad(long idCriticidad) {
		this.idCriticidad = idCriticidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "CriticidadDTO [idCriticidad=" + idCriticidad + ", nombre=" + nombre + "]";
	}
	
	
	

}
