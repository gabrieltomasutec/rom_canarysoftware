package com.capa1_presentacion.bean;

import java.io.Serializable;

public class EstadoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private long id;
	private String nombre;

	
	public EstadoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public EstadoDTO(long id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	
}
