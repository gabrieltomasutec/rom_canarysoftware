package com.capa1_presentacion.bean;

import java.io.Serializable;

public class ZonasDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long idZona;
	private String nombre;
	public ZonasDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ZonasDTO(long idZona, String nombre) {
		super();
		this.idZona = idZona;
		this.nombre = nombre;
	}
	public long getIdZona() {
		return idZona;
	}
	public void setIdZona(long idZona) {
		this.idZona = idZona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	@Override
	public String toString() {
		return "ZonasDTO [idZona=" + idZona + ", nombre=" + nombre + "]";
	}
	
	
	

}
