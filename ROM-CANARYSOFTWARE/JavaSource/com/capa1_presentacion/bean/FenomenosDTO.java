package com.capa1_presentacion.bean;

import java.io.Serializable;

public class FenomenosDTO  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long idFenomeno;
	private String nombre;
	private String descripcion;
	private String telefono;
	
	public FenomenosDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FenomenosDTO(long idFenomeno, String nombre, String descripcion, String telefono) {
		super();
		this.idFenomeno = idFenomeno;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.telefono = telefono;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public long getIdFenomeno() {
		return idFenomeno;
	}
	public String getNombre() {
		return nombre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void setIdFenomeno(long idFenomeno) {
		this.idFenomeno = idFenomeno;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Override
	public String toString() {
		return "FenomenoDTO [id=" + idFenomeno + ", nombre=" + nombre + ", descripcion=" + descripcion + ", telefono="
				+ telefono + "]";
	}
	
}
