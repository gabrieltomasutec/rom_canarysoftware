package com.capa1_presentacion.bean;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.capa3_persistence.entities.Estado;
import com.capa3_persistence.entities.TipoDoc;
import com.capa3_persistence.entities.TipoUsuario;

public class UsuarioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotNull
	private long id;
	@NotNull
	private String nombreUsuario;
	@NotNull
	private String password;
	@NotNull
	private long estado;
	@NotNull
	private String estadoNombre;
	@NotNull
	private long tipoUsuario;
	@NotNull
	private String tipoUsuarioNombre;
	@NotNull
	private String nombre;	
	@NotNull
	private String apellido;
	@NotNull
	private long tipoDocumento;
	@NotNull
	private String tipoDocumentoNombre;
	@NotNull
	private String numeroDocumento;
	@NotNull
	private String direccion;
	@NotNull
	private String correo;
//	--------------
	

	public UsuarioDTO() {
		super();
	}
	public UsuarioDTO(@NotNull long id, @NotNull String nombreUsuario, @NotNull String password, @NotNull long estado,
			@NotNull String estadoNombre, @NotNull long tipoUsuario, @NotNull String tipoUsuarioNombre,
			@NotNull String nombre, @NotNull String apellido, @NotNull long tipoDocumento,
			@NotNull String tipoDocumentoNombre, @NotNull String numeroDocumento, @NotNull String direccion,
			@NotNull String correo) {
		super();
		this.id = id;
		this.nombreUsuario = nombreUsuario;
		this.password = password;
		this.estado = estado;
		this.estadoNombre = estadoNombre;
		this.tipoUsuario = tipoUsuario;
		this.tipoUsuarioNombre = tipoUsuarioNombre;
		this.nombre = nombre;
		this.apellido = apellido;
		this.tipoDocumento = tipoDocumento;
		this.tipoDocumentoNombre = tipoDocumentoNombre;
		this.numeroDocumento = numeroDocumento;
		this.direccion = direccion;
		this.correo = correo;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getEstado() {
		return estado;
	}
	public void setEstado(long estado) {
		this.estado = estado;
	}
	public String getEstadoNombre() {
		return estadoNombre;
	}
	public void setEstadoNombre(String estadoNombre) {
		this.estadoNombre = estadoNombre;
	}
	public long getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(long tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	public String getTipoUsuarioNombre() {
		return tipoUsuarioNombre;
	}
	public void setTipoUsuarioNombre(String tipoUsuarioNombre) {
		this.tipoUsuarioNombre = tipoUsuarioNombre;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public long getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(long tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getTipoDocumentoNombre() {
		return tipoDocumentoNombre;
	}
	public void setTipoDocumentoNombre(String tipoDocumentoNombre) {
		this.tipoDocumentoNombre = tipoDocumentoNombre;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	@Override
	public String toString() {
		return "UsuarioDTO [id=" + id + ", nombreUsuario=" + nombreUsuario + ", password=" + password + ", estado="
				+ estado + ", estadoNombre=" + estadoNombre + ", tipoUsuario=" + tipoUsuario + ", tipoUsuarioNombre="
				+ tipoUsuarioNombre + ", nombre=" + nombre + ", apellido=" + apellido + ", tipoDocumento="
				+ tipoDocumento + ", tipoDocumentoNombre=" + tipoDocumentoNombre + ", numeroDocumento="
				+ numeroDocumento + ", direccion=" + direccion + ", correo=" + correo + "]";
	}
	
	
	
	

}
