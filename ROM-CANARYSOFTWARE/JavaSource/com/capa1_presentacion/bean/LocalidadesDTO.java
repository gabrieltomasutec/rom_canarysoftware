package com.capa1_presentacion.bean;

import java.io.Serializable;

public class LocalidadesDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String nombre;
	private long departamento;
	private String departamentoNombre;
	public LocalidadesDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LocalidadesDTO(long id, String nombre, long departamento, String departamentoNombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.departamento = departamento;
		this.departamentoNombre = departamentoNombre;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public long getDepartamento() {
		return departamento;
	}
	public void setDepartamento(long departamento) {
		this.departamento = departamento;
	}

	public String getDepartamentoNombre() {
		return departamentoNombre;
	}

	public void setDepartamentoNombre(String departamentoNombre) {
		this.departamentoNombre = departamentoNombre;
	}

	@Override
	public String toString() {
		return "LocalidadesDTO [id=" + id + ", nombre=" + nombre + ", departamento=" + departamento
				+ ", departamentoNombre=" + departamentoNombre + "]";
	}

	
	
	

}
