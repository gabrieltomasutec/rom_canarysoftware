package com.capa1_presentacion.bean;

import java.io.Serializable;

public class DepartamentosDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String nombre;
	private long zona;
	public DepartamentosDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DepartamentosDTO(long id, String nombre, long zona) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.zona = zona;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public long getZona() {
		return zona;
	}
	public void setZona(long zona) {
		this.zona = zona;
	}
	@Override
	public String toString() {
		return "DepartamentosDTO [id=" + id + ", nombre=" + nombre + ", zona=" + zona + "]";
	}
	
	

}
