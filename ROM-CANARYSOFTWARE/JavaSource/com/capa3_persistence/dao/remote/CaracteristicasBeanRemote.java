package com.capa3_persistence.dao.remote;

import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface CaracteristicasBeanRemote {

	Caracteristicas buscarCodigo (long idCaracteristica) throws ServiciosException;
	List<Caracteristicas> obtenerTodos();
}
