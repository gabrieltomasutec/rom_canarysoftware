package com.capa3_persistence.dao.remote;

import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface ZonasBeanRemote {
	Zonas buscarCodigo (long idZona) throws ServiciosException;
	List<Zonas> obtenerTodos();

}
