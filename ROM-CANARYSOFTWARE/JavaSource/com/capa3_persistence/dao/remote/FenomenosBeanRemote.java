package com.capa3_persistence.dao.remote;
import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface FenomenosBeanRemote {
	void borrarFenomeno (Long idFenomeno) throws ServiciosException;
	Fenomenos buscarCodigo (long idFenomeno) throws ServiciosException;
	String buscarNombre (String Nombre) throws ServiciosException;
	boolean crearFenomeno (Fenomenos fenomeno) throws ServiciosException;
	boolean modificarFenomeno (Fenomenos f) throws ServiciosException;
//	List<Fenomenos> obtenerPorCodigo (Long idFenomeno);
	Fenomenos obtenerPorNombre (String Nombre);
	List<Fenomenos> obtenerTodos();
	
	
}