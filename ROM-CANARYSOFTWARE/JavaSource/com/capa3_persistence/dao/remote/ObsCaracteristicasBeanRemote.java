package com.capa3_persistence.dao.remote;

import javax.ejb.Remote;

import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface ObsCaracteristicasBeanRemote {
	
	void borrarPorObs (Long idObs) throws ServiciosException;

}
