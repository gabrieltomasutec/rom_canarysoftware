package com.capa3_persistence.dao.remote;

import javax.ejb.Remote;

import com.capa3_persistence.entities.Departamentos;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface DepartamentosBeanRemote {
	Departamentos buscarCodigo (long idDepartamento) throws ServiciosException;

}
