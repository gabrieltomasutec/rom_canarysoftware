package com.capa3_persistence.dao.remote;

import javax.ejb.Remote;

import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface UsuariosBeanRemote {
	Usuarios agregarUsuario (Usuarios usuario) throws ServiciosException, PersistenciaException;
	
	void borrarUsuario (long idUsuario) throws ServiciosException;
	Usuarios buscarUsuario (long idUsuario) throws ServiciosException;
	//	Usuarios buscarUsuario (long idUsuario) throws ServiciosException;
    Usuarios buscarUsuarioByDoc (String sDocumento)  throws ServiciosException;
Usuarios buscarUsuarioByLogin (String sLogin) throws ServiciosException;
    boolean crearUsuario(Usuarios usuario) throws ServiciosException;
    Usuarios loguearUsuario (String usuario, String psw) throws ServiciosException;
    boolean modificarUsuario (Usuarios usuario) throws ServiciosException;

}
