package com.capa3_persistence.dao.remote;

import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.Estado;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface EstadoBeanRemote {
	Estado buscarCodigo (long idEstado) throws ServiciosException;
	List<Estado> obtenerTodos() throws PersistenciaException;
}
