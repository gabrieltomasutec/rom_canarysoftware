package com.capa3_persistence.dao.remote;



import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.ServiciosException;

import java.util.List;
import javax.ejb.Remote;

@Remote
public interface TipoDocBeanRemote {

        TipoDoc obtenerDoc(long nId) throws ServiciosException;
        List<TipoDoc> obtenerDocs() throws ServiciosException;
}
