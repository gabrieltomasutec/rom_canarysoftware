package com.capa3_persistence.dao.remote;
import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface ObservacionesBeanRemote {
	Observaciones agregarObservacion (Observaciones observacion) throws ServiciosException;
	Observaciones buscarCodigo (Long idObs) throws ServiciosException;
	List<Observaciones> obtenerPorFenomeno(long idFenomeno);
	List<Observaciones> obtenerPorFenyFec(long idFenomeno, Timestamp fechaIni, Timestamp fechaFin);
	List<Observaciones> obtenerTodos();
	boolean modificarObservacion (Observaciones observacion) throws ServiciosException;
	void borrarObservacion (Long idObservacion) throws ServiciosException;
}


