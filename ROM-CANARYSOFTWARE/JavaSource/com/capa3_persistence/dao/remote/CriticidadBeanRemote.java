package com.capa3_persistence.dao.remote;

import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.Criticidad;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface CriticidadBeanRemote {
	Criticidad buscarCodigo (long idCriticidad) throws ServiciosException;
	List<Criticidad> obtenerTodos();
}
