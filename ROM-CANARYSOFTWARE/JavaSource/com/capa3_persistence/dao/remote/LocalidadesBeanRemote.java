package com.capa3_persistence.dao.remote;

import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.Localidades;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface LocalidadesBeanRemote {
	Localidades buscarCodigo (long idLocalidad) throws ServiciosException;
	List<Localidades> obtenerTodos();
}
