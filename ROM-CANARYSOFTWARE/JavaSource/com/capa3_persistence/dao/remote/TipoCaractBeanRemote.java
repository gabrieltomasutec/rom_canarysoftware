package com.capa3_persistence.dao.remote;

import java.util.List;

import javax.ejb.Remote;

import com.capa3_persistence.entities.TipoCaract;
import com.capa3_persistence.exception.ServiciosException;

@Remote
public interface TipoCaractBeanRemote {
	TipoCaract buscarCodigo (long idTipoCaract) throws ServiciosException;
	List<TipoCaract> obtenerTodos();
}
