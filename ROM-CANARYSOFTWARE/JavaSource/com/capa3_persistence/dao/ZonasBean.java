package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.ZonasBeanRemote;
import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class ZonasBean
 */
@Stateless
@LocalBean
public class ZonasBean implements ZonasBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ZonasBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Zonas buscarCodigo(long idZona) throws ServiciosException {
		try {
			Zonas zona = em.find(Zonas.class, idZona);
			zona.getIdZona();
			zona.getNombre();

	    	return zona;
	    	
	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar la zona");
	    	}
			return null;
	}
	
	@Override
	public List<Zonas> obtenerTodos() {
		TypedQuery<Zonas> query = em.createQuery("SELECT f FROM Zonas f",Zonas.class);
		return query.getResultList();	
	}
	
	public List<Zonas> seleccionarZonas(String criterioBusqueda) throws PersistenciaException {
		try {
			
			String query= 	"Select e from Zonas e  ";
			String queryCriterio="";
			if (criterioBusqueda!=null && ! criterioBusqueda.contentEquals("")) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.nombre like '%"+criterioBusqueda +"%' ";
			} 
			
			if (!queryCriterio.contentEquals("")) {
				query+=" where "+queryCriterio;
			}
			List<Zonas> resultList = (List<Zonas>) em.createQuery(query,Zonas.class)
								 .getResultList();
			return  resultList;
			}catch(PersistenceException e) {
				throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
			}
	}
	
	public Zonas agregarZona(Zonas zona) throws PersistenciaException {
		
		try {
			
			Zonas f = em.merge(zona);
			em.flush();
			return f;
		} catch (PersistenceException e) {
			throw new PersistenciaException("No se pudo agregar la Zona." + e.getMessage(), e);
		}
	}
	public boolean modificarZona (Zonas f) throws ServiciosException {
		try {
			Zonas zona = em.find(Zonas.class, f.getIdZona());
	
			if(zona!=null) {
				zona.setNombre(f.getNombre());
				
				em.persist(zona);
				em.flush();
			}
		} catch (PersistenceException e) {
			System.out.println ("No se pudo modificar la Zona");
		}
		return false;
	}

}
