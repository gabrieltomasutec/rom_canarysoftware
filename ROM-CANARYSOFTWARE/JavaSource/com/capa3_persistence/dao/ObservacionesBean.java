package com.capa3_persistence.dao;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.ObservacionesBeanRemote;
import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class ObservacionesBean
 */
@Stateless
@LocalBean
public class ObservacionesBean implements ObservacionesBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ObservacionesBean() {
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void borrarObservacion (Long idObservacion) throws ServiciosException {
    	try {
	    	Observaciones obs = em.find(Observaciones.class, idObservacion);
	    	if(obs!=null) {
		    	em.remove(obs);
		    	em.flush();
	    	}
	    } catch (PersistenceException e) {
	    	System.out.println ("No se pudo eliminar la observacion");
	    }
	}
    
    
    @Override
	public Observaciones agregarObservacion(Observaciones observacion) throws ServiciosException {
		try {
			em.persist(observacion);
    		em.flush();
    	} catch (PersistenceException e) {
    		System.out.println ("No se pudo crear la observaci�n" +" "+ e.getMessage());
    	}
		return observacion;
	}

	@Override
    public Observaciones buscarCodigo (Long idObs) throws ServiciosException{
		System.out.println ("en buscarCodigo en ObservacionesBean idObs " + idObs);
    	try {
    		Observaciones observacion = em.find(Observaciones.class, idObs);
    		return observacion;
    	}catch (PersistenceException e) {
    		System.out.println ("No se pudo encontrar la observacion");
    	}
		return null;
		 
    }
	
	@Override
	public List<Observaciones> obtenerPorFenomeno(long idFenomeno) {
		TypedQuery<Observaciones> query = em.createQuery("SELECT o FROM Observaciones o WHERE o.idFenomeno.idFenomeno LIKE :idF",
			Observaciones.class).setParameter("idF", idFenomeno);
		return query.getResultList();
	}

	@Override
	public List<Observaciones> obtenerPorFenyFec(long idFenomeno, Timestamp fechaIni, Timestamp fechaFin) {
		TypedQuery<Observaciones> query = em.createQuery("SELECT o FROM Observaciones o WHERE ((o.idFenomeno.idFenomeno LIKE :idF) AND (o.fechaHora >= :fi) AND (o.fechaHora <= :ff))",
				Observaciones.class);
		query.setParameter("idF", idFenomeno);
		query.setParameter("fi", fechaIni);
		query.setParameter("ff", fechaFin);
		return query.getResultList();
			
	}

	@Override
	public List<Observaciones> obtenerTodos() {
		System.out.println ("obtenerTodos() = en obtener todos observacionesBean" );
		TypedQuery<Observaciones> query = em.createQuery("SELECT o FROM Observaciones o",Observaciones.class);
		return query.getResultList();
	}
	
	public List<Observaciones> seleccionarObservaciones(String criterioBusqueda) throws PersistenciaException {
		try {
			
			String query= 	"Select e from Observaciones e  ";
			String queryCriterio="";
			if (criterioBusqueda!=null && ! criterioBusqueda.contentEquals("")) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.idObservacion like '%"+criterioBusqueda +"%' ";
			} 
			
			if (!queryCriterio.contentEquals("")) {
				//query+=" where "+queryCriterio;
			}
			List<Observaciones> resultList = (List<Observaciones>) em.createQuery(query,Observaciones.class)
								 .getResultList();
			return  resultList;
			}catch(PersistenceException e) {
				throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
			}
	}
	@Override
	public boolean modificarObservacion(Observaciones observacionOld) throws ServiciosException {
		try {
			Observaciones observacion = em.find(Observaciones.class, observacionOld.getIdObservacion());
			if(observacion!= null) {
				observacion.setIdUsuarioVali(observacionOld.getIdUsuarioAlta());
				observacion.setValidFecha(observacionOld.getValidFecha());
				observacion.setValidComents(observacionOld.getValidComents());
			
	            em.persist(observacion);
	            em.flush();
	            return true;
	        }
	    } catch (PersistenceException e) {
	        System.out.println ("No se pudo modificar la Observacion" +" "+ e.getMessage());
	    }
	    return false;
		
	}

	public List<Observaciones> seleccionarPorCriterios(String criterioDescripcion, long criterioFenomeno,
			long criterioCriticidad, Date criterioFechaIni, Date criterioFechaFin, int criterioValidado) throws PersistenciaException, ParseException {
		
		try {
			
			String query = "Select e from Observaciones e ";
			String queryCriterio = "";
			
			if (criterioDescripcion!=null && !criterioDescripcion.contentEquals("")) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.descripcion like '%"+criterioDescripcion +"%' ";
			} 
			
			if (criterioFenomeno != 0) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.idFenomeno.idFenomeno like "+criterioFenomeno +" ";
			}
			
			if (criterioCriticidad != 0) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.idCriticidad.idCriticidad like "+criterioCriticidad +" ";
			}			
			
			if (criterioValidado != 0) {
				if (criterioValidado ==1) {
					queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.validFecha is not null";
				}else {
					queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.validFecha is null";
				}
			}
						
			if (criterioFechaIni !=null) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.fechaHora >= :fi";
			}
			
			if (criterioFechaFin !=null) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.fechaHora <= :ff";
			}		
			
			if (!queryCriterio.contentEquals("")) {
				query+="where"+queryCriterio;				
			}
			
			System.out.println ("Query de buscar por criterio observaciones: "+ query);		
			TypedQuery<Observaciones> consulta = em.createQuery(query, Observaciones.class);
		
			if (criterioFechaIni != null) {
				Timestamp fechaIni = new Timestamp (criterioFechaIni.getTime());
				consulta.setParameter("fi", fechaIni);
			}
			
			if (criterioFechaFin != null) {
				Timestamp fechaFin = new Timestamp (criterioFechaFin.getTime());
				consulta.setParameter("ff", fechaFin);
			}			
			
			return consulta.getResultList();
			
		}catch(PersistenceException e) {
			throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
		}
	}
	
	public Observaciones modificarObservacionCompleta(Observaciones observacionOld) throws ServiciosException, PersistenciaException {
		try {
			Observaciones observaciones = em.merge(observacionOld);
			em.flush();
			return observaciones;
		} catch (PersistenceException e) {
			throw new PersistenciaException("No se pudo modificar la consultora." + e.getMessage(), e);
		}
		
	}
	
	
	// para trabajar con CARACTERISTICAS 
	public Observaciones agregarObsCaract(Observaciones obs, Caracteristicas caract, long num, String texto,
			Date fecha) {
		ObsCaracteristicas ObsCaract = new ObsCaracteristicas(obs,caract,num,texto,null);
		obs.agregarObsCaracteristica(ObsCaract);
		return obs;
	}
	
    public Observaciones buscarPorId(long idObs) throws ServiciosException{
		System.out.println ("en idObservacion en ObservacionesBean idObs " + idObs);
		Observaciones retorno = null;
		TypedQuery<Observaciones> query = em.createQuery("SELECT o FROM Observaciones o WHERE o.idObservacion LIKE :idF",
					Observaciones.class);
		query.setParameter("idF", idObs);
		System.out.println ("en idObservacion en ObservacionesBean query " + query.toString());
		List<Observaciones> emp = query.getResultList();
		for(Observaciones e: emp) {
			retorno = e;
		}
		return retorno;
				
    }
}
