package com.capa3_persistence.dao;

import com.capa3_persistence.dao.remote.TipoDocBeanRemote;
import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.ServiciosException;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 * Session Bean implementation class TipoDocBean
 */
@Stateless
@LocalBean
public class TipoDocBean implements TipoDocBeanRemote {

     @PersistenceContext
     private EntityManager em;
  
    /**
     * Default constructor. 
     */
    public TipoDocBean() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public TipoDoc obtenerDoc(long nId) throws ServiciosException {
    	try {
    		TipoDoc tipoDoc	= em.find(TipoDoc.class, nId);
    		if(tipoDoc!=null) {
    			tipoDoc.getIdTipoDoc();
	    		tipoDoc.getNombre();
	    		return tipoDoc;
    		}else {
    			return null;
    		}
    		
    	}catch (PersistenceException e) {
    		System.out.println ("No se pudo encontrar el Tipo de Documento");
    	}
		return null;
    }

    @Override
    public List<TipoDoc> obtenerDocs() throws ServiciosException {
        TypedQuery<TipoDoc> query =  em.createQuery("SELECT c FROM TipoDoc c", TipoDoc.class);
    	return query.getResultList();
    
    }
    

}

