package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import com.capa3_persistence.dao.remote.EstadoBeanRemote;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Estado;
import com.capa3_persistence.entities.Estado;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class EstadoBean
 */
@Stateless
@LocalBean
public class EstadoBean implements EstadoBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public EstadoBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Estado buscarCodigo(long idEstado) throws ServiciosException {
		try {
			Estado estado = em.find(Estado.class, idEstado);
			estado.getIdEstado();
			estado.getNombre();
			System.out.println ("SE ENCONTRO"+estado.getNombre());
	    	return estado;
	    	
	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar la criticidad");
	    	}
			return null;
	}

	@Override
	public List<Estado> obtenerTodos()  throws PersistenciaException {

			try {
			
			String query= 	"Select e from Estado e";
			List<Estado> resultList = (List<Estado>) em.createQuery(query,Estado.class).getResultList();
			return  resultList;
			}catch(PersistenceException e) {
				throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
			}
			
		}
	public Estado agregarEstado(Estado estado) throws PersistenciaException {
		
		try {
			
			Estado f = em.merge(estado);
			em.flush();
			return f;
		} catch (PersistenceException e) {
			throw new PersistenciaException("No se pudo agregar Estado." + e.getMessage(), e);
		}
	}

	
	
}
