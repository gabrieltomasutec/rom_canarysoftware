package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.TipoCaractBeanRemote;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.TipoCaract;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class TipoCaractBean
 */
@Stateless
@LocalBean
public class TipoCaractBean implements TipoCaractBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public TipoCaractBean() {
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public TipoCaract buscarCodigo(long idTipoCaract) throws ServiciosException {
    	System.out.println ("************** idTipoCaract"+idTipoCaract);
		try {
			TipoCaract caracteristica = em.find(TipoCaract.class, idTipoCaract);
			caracteristica.getIdTipoCaract();
			caracteristica.getNombre();

	    	return caracteristica;
	    	
	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar la Caracteristica");
	    	}
			return null;
	}

	@Override
	public List<TipoCaract> obtenerTodos() {
		TypedQuery<TipoCaract> query = em.createQuery("SELECT f FROM TipoCaract f",TipoCaract.class);
		return query.getResultList();	
	}

}
