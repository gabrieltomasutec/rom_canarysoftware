package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.LocalidadesBeanRemote;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Localidades;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class LocalidadesBean
 */
@Stateless
@LocalBean
public class LocalidadesBean implements LocalidadesBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public LocalidadesBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Localidades buscarCodigo(long idLocalidad) throws ServiciosException {
		try {
			Localidades localidad = em.find(Localidades.class, idLocalidad);
			localidad.getIdLocalidad();
			localidad.getNombre();
			localidad.getIdDepartamento();

	    	return localidad;
	    	
	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar la localidad");
	    	}
			return null;
	}

	@Override
	public List<Localidades> obtenerTodos() {
		TypedQuery<Localidades> query = em.createQuery("SELECT f FROM Localidades f",Localidades.class);
		return query.getResultList();	
	}
	
	 //@Override
	 public Localidades obtenerPorNombre (String nombre){
		 Localidades localidad = new Localidades();
	    	localidad.getNombre();
	    	TypedQuery<Localidades> query = em.createQuery("SELECT f FROM Localidades f WHERE f.Nombre LIKE :nombreF", Localidades.class)
	    			.setParameter("nombreF", nombre);
	    	System.out.println ("CONSULTA query: " +" "+ query);
			try {
				return query.getResultList().get(0);
			} catch (Exception e) {
				return null;
			}
	    	
	  }
	 
	 public List<Localidades> obtenerTodosPorNombre(String nombre) throws PersistenciaException{
	    	try {
	    		Localidades localidad = new Localidades();
		    	localidad.getNombre();
		
		    	String query= 	"SELECT f FROM Localidades f  ";
		    	String queryCriterio="";
		    	queryCriterio = " f.nombre like '%"+nombre +"%' ";
		    	query+=" where "+queryCriterio;
		    	
		    	List<Localidades> resultList = (List<Localidades>) em.createQuery(query,Localidades.class)
						 .getResultList();
		    	return  resultList;
		
		    	
		    	
			} catch (Exception e) {
				throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
			}
	    	
	    }
	 
	 public boolean crearLocalidad(Localidades localidad) throws ServiciosException {
			try {
	            em.persist(localidad);
	            em.flush();
	            return true;
	        } catch (PersistenceException e) {
	            System.out.println ("No se pudo crear la localidad" +" "+ e.getMessage());
	        }
	        return false;
		}
		
	public Localidades agregarLocalidad(Localidades localidad) throws PersistenciaException {
			
					try {
						
						Localidades f = em.merge(localidad);
						em.flush();
						return f;
					} catch (PersistenceException e) {
						throw new PersistenciaException("No se pudo agregar la localidad. " + e.getMessage(), e);
					}
	}
	    
	    
	    public boolean modificarLocalidad (Localidades f) throws ServiciosException {
	    	try {
	    		Localidades localidad = em.find(Localidades.class, f.getIdLocalidad());
	    		
	    		if(localidad!=null) {
		    		localidad.setNombre(f.getNombre());
		    		localidad.setIdDepartamento(f.getIdDepartamento());
		    		
		    		em.persist(localidad);
		    		em.flush();
	    		}
	    	} catch (PersistenceException e) {
	    		System.out.println ("No se pudo modificar la localidad");
	    	}
	    	return false;
	    }

}
