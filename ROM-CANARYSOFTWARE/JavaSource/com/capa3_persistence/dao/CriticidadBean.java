package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.CriticidadBeanRemote;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Criticidad;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class CriticidadBean
 */
@Stateless
@LocalBean
public class CriticidadBean implements CriticidadBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public CriticidadBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Criticidad buscarCodigo(long idCriticidad) throws ServiciosException {
		try {
			Criticidad criticidad = em.find(Criticidad.class, idCriticidad);
			criticidad.getIdCriticidad();
			criticidad.getNombre();

	    	return criticidad;
	    	
	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar la criticidad");
	    	}
			return null;
	}

	@Override
	public List<Criticidad> obtenerTodos() {
		TypedQuery<Criticidad> query = em.createQuery("SELECT f FROM Criticidad f",Criticidad.class);
		return query.getResultList();	
	}
	
	public List<Criticidad> seleccionarCriticidad(String criterioBusqueda) throws PersistenciaException {
		try {
			
			String query= 	"Select e from Criticidad e  ";
			String queryCriterio="";
			if (criterioBusqueda!=null && ! criterioBusqueda.contentEquals("")) {
				queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.nombre like '%"+criterioBusqueda +"%' ";
			} 
			
			if (!queryCriterio.contentEquals("")) {
				query+=" where "+queryCriterio;
			}
			List<Criticidad> resultList = (List<Criticidad>) em.createQuery(query,Criticidad.class)
								 .getResultList();
			return  resultList;
			}catch(PersistenceException e) {
				throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
			}
	}
	
	public Criticidad agregarCriticidad(Criticidad criticidad) throws PersistenciaException {
		
		try {
			
			Criticidad f = em.merge(criticidad);
			em.flush();
			return f;
		} catch (PersistenceException e) {
			throw new PersistenciaException("No se pudo agregar la Criticidad." + e.getMessage(), e);
		}
	}

}
