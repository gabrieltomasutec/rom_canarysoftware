package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.UsuariosBeanRemote;
import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class UsuariosBean
 */
@Stateless
@LocalBean
public class UsuariosBean implements UsuariosBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public UsuariosBean() {
        // TODO Auto-generated constructor stub
    }

	public Usuarios agregarUsuario(Usuarios usuario) throws PersistenciaException {
//System.out.println ("No se pudo crear el usuario y esto es lo que se guarda ***** " +" "+ usuario.toString());
		try {
			
			Usuarios usu = em.merge(usuario);
			em.flush();
			return usu;
		} catch (PersistenceException e) {
			//throw new PersistenciaException("No se pudo agregar el usuario." + e.getMessage(), e);
			throw new PersistenciaException("No se pudo agregar el usuario.", e);
		}
	}
	
	@Override
	public void borrarUsuario(long idFenomeno) throws ServiciosException {
	            // TODO Auto-generated method stub
	}

	@Override
	public Usuarios buscarUsuario(long idUsuario) throws ServiciosException {
		try {
			Usuarios usuario = em.find(Usuarios.class, idUsuario);
			if(usuario!= null) {
				usuario.getIdUsuario();
				usuario.getApellido();
				usuario.getCorreo();
				usuario.getDireccion();
				usuario.getEstado();
				usuario.getTipoDoc();
				usuario.getTipoUsuario();
				usuario.getNombre();
				usuario.getNombreUsuario();
				usuario.getNumDoc();
				usuario.getPassword();
		    	return usuario;
			}

			return null;

	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar el usuario");
	    	}
			return null;
	}

        @Override
        public Usuarios buscarUsuarioByDoc(String sDocumento)  throws ServiciosException{

        	TypedQuery<Usuarios> query = em.createQuery("SELECT f FROM Usuarios f WHERE f.numDoc LIKE :nombreF", Usuarios.class)
        			.setParameter("nombreF", sDocumento);
    		try {
    			return query.getResultList().get(0);
    		} catch (Exception e) {
    			return null;
    		}
        }

        @Override
        public Usuarios buscarUsuarioByLogin(String sLogin)  throws ServiciosException{
            TypedQuery<Usuarios> query =  em.createQuery("SELECT c FROM Usuarios c where c.nombreUsuario = :log ", 
                        Usuarios.class).setParameter("log", sLogin);
            try {
            	return query.getSingleResult();
	        } catch (Exception e) {
				return null;
			}
        }

        public List<Usuarios> buscarUsuarios() throws PersistenciaException {
			try {
			
			String query= 	"Select e from Usuarios e";
			List<Usuarios> resultList = (List<Usuarios>) em.createQuery(query,Usuarios.class)
								 .getResultList();
			return  resultList;
			}catch(PersistenceException e) {
				throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
			}
			
		}

		@Override
		public boolean crearUsuario(Usuarios usuario) throws ServiciosException {
			try {
		        em.persist(usuario);
		        em.flush();
		        return true;
		    } catch (PersistenceException e) {
		        System.out.println ("No se pudo crear el usuario" +" "+ e.getMessage());
		    }
		    return false;
		}
		
		//----------------login
		public List<Usuarios> loginUsuario(String usuario, String pwd ) throws PersistenciaException {
			System.out.println(" - ***/////----*********login=[USUARIO BEAN]");
			try {
				String query= 	"Select e from Usuarios e  where e.nombreUsuario='"+usuario+"' and e.password='"+pwd+"'";
				System.out.println("query  "+ query );
				List<Usuarios> resultList = (List<Usuarios>) em.createQuery(query,Usuarios.class)
									 .getResultList();
				for(Usuarios e: resultList) {
					System.out.println(" Usuario logueado***** "+ e.getNombre() +" "+ e.getApellido());
				}
				return  resultList;
				}catch(PersistenceException e) {
					throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
				}
		}
		
		@Override
		public Usuarios loguearUsuario(String usuario, String psw) throws ServiciosException {
			//String password = Usuarios.encriptarContrasena(psw);
			TypedQuery<Usuarios> query = em.createQuery("SELECT o FROM Usuarios o WHERE ((o.nombreUsuario LIKE :usuario) AND (o.password LIKE :password))",
					Usuarios.class);
			query.setParameter("usuario", usuario);
			//query.setParameter("password", password);
			query.setParameter("password", psw);
			System.out.println("loguearUsuario  "+ psw +"--"+Usuarios.encriptarContrasena("psw"));
			try {
				return (Usuarios) query.getSingleResult();
			} catch (Exception e) {
				// TODO: handle exception
			}
			return null;
			
		}
		
		@Override
		public boolean modificarUsuario(Usuarios usuarioOld) throws ServiciosException {
			try {
				Usuarios usuario = em.find(Usuarios.class, usuarioOld.getIdUsuario());
				if(usuario!= null) {
					usuario.setIdUsuario(usuarioOld.getIdUsuario());
					usuario.setApellido(usuarioOld.getApellido());
					usuario.setCorreo(usuarioOld.getCorreo());
					usuario.setDireccion(usuarioOld.getDireccion());
					usuario.setEstado(usuarioOld.getEstado());
					usuario.setTipoDoc(usuarioOld.getTipoDoc());
					usuario.setTipoUsuario(usuarioOld.getTipoUsuario());
					usuario.setNombre(usuarioOld.getNombre());
					usuario.setNombreUsuario(usuarioOld.getNombreUsuario());
					usuario.setNumDoc(usuarioOld.getNumDoc());
					if(usuarioOld.getPassword()==null ||usuarioOld.getPassword()==""||usuarioOld.getPassword().isEmpty()) {
						usuario.setPassword(usuario.getPassword());
					}else {
						usuario.setPassword(usuarioOld.getPassword());
					}
					
		            em.persist(usuario);
		            em.flush();
		            return true;
		        }
		    } catch (PersistenceException e) {
		        System.out.println ("No se pudo modificar el usuario" +" "+ e.getMessage());
		    }
		    return false;
			
		}
		
		public List<Usuarios> seleccionarUsuarios(String criterioNombre,
				String criterioDepartamento, Boolean criterioActivo) throws PersistenciaException {
			try {
				
				String query= 	"Select e from Usuarios e  ";
				String queryCriterio="";
				if (criterioNombre!=null && ! criterioNombre.contentEquals("")) {
					queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+ " e.nombre like '%"+criterioNombre +"%' ";
				} 
				if (criterioDepartamento!=null && ! criterioDepartamento.equals("")) {
					queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+" e.departamento='"+criterioDepartamento+"'  " ;
				}
				if (criterioActivo!=null) {
					queryCriterio+=(!queryCriterio.isEmpty()?" and ":"")+" e.activo  " ;
				}
				if (!queryCriterio.contentEquals("")) {
					query+=" where "+queryCriterio;
				}
				List<Usuarios> resultList = (List<Usuarios>) em.createQuery(query,Usuarios.class)
									 .getResultList();
				return  resultList;
				}catch(PersistenceException e) {
					throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
				}
		}

		public Usuarios buscarNombreUsuario(String nombreUsuario) {
			// TODO Auto-generated method stub
			try {
				Usuarios usuario = em.find(Usuarios.class, nombreUsuario);
				if(usuario!= null) {
					usuario.getIdUsuario();
					usuario.getApellido();
					usuario.getCorreo();
					usuario.getDireccion();
					usuario.getEstado();
					usuario.getTipoDoc();
					usuario.getTipoUsuario();
					usuario.getNombre();
					usuario.getNombreUsuario();
					usuario.getNumDoc();
					usuario.getPassword();
			    	return usuario;
				}

				return null;

		    	}catch (PersistenceException e) {
		    	System.out.println ("No se pudo encontrar el usuario");
		    	}
				return null;
			
		}

        


}
