package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.ObsCaracteristicasBeanRemote;
import com.capa3_persistence.entities.ObsCaracteristicas;
import com.capa3_persistence.entities.ObsCaracteristicasPK;
import com.capa3_persistence.entities.Observaciones;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class ObsCaracteristicasBean
 */
@Stateless
@LocalBean
public class ObsCaracteristicasBean implements ObsCaracteristicasBeanRemote {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;
	
    public ObsCaracteristicasBean() {
        // TODO Auto-generated constructor stub
    }
    
    public void borrarObsCaract(long idObs, long idCaract) {
    	ObsCaracteristicasPK pk = new ObsCaracteristicasPK(idObs,idCaract);
        ObsCaracteristicasBean obsCaracteristicasBean= em.find( ObsCaracteristicasBean.class, pk);
        em.remove( obsCaracteristicasBean );
        em.getTransaction( ).commit( );
        em.close( );
    }

	@Override
	public void borrarPorObs(Long idObs) throws ServiciosException {
    	try {
    		TypedQuery<ObsCaracteristicas> query = em.createQuery("SELECT o FROM ObsCaracteristicas o WHERE o.observacion.idObservacion LIKE :idO",
    				ObsCaracteristicas.class).setParameter("idO", idObs);
    		List<ObsCaracteristicas> lista = query.getResultList();
    		
    		for (ObsCaracteristicas obsCarac: lista) {
    	    	if(obsCarac!=null) {
    		    	em.remove(obsCarac);
    		    	em.flush();
    	    	}
    		}
    		
	    } catch (PersistenceException e) {
	    	System.out.println ("No se pudo eliminar las ObsCaracteristicas");
	    }
		
	}

}
