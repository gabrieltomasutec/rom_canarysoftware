package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.CaracteristicasBeanRemote;
import com.capa3_persistence.entities.Caracteristicas;
import com.capa3_persistence.entities.Criticidad;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class CaracteristicasBean
 */
@Stateless
@LocalBean
public class CaracteristicasBean implements CaracteristicasBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public CaracteristicasBean() {
        // TODO Auto-generated constructor stub
    }

    @Override
	public Caracteristicas buscarCodigo(long idCaracteristica) throws ServiciosException {
		try {
			Caracteristicas caracteristica = em.find(Caracteristicas.class, idCaracteristica);
			caracteristica.getIdCaracteristica();
			caracteristica.getNombre();

	    	return caracteristica;
	    	
	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar la Caracteristica");
	    	}
			return null;
	}

	@Override
	public List<Caracteristicas> obtenerTodos() {
		TypedQuery<Caracteristicas> query = em.createQuery("SELECT f FROM Caracteristicas f",Caracteristicas.class);
		return query.getResultList();	
	}
	
	public Caracteristicas agregarCaracteristica(Caracteristicas caracteristica) throws PersistenciaException {
		
		try {
			
			Caracteristicas f = em.merge(caracteristica);
			em.flush();
			return f;
		} catch (PersistenceException e) {
			throw new PersistenciaException("No se pudo agregar la caracteristica." + e.getMessage(), e);
		}
	}

//  @Override
	public boolean modificarCaracteristica (Caracteristicas f) throws ServiciosException {
		try {
			Caracteristicas caracteristica = em.find(Caracteristicas.class, f.getIdCaracteristica());
	
			if(caracteristica!=null) {
				caracteristica.setNombre(f.getNombre());
				caracteristica.setEtiqueta(f.getEtiqueta());
				caracteristica.setIdTipoCaract(f.getIdTipoCaract());
				caracteristica.setIdFenomeno(f.getIdFenomeno());
				em.persist(caracteristica);
				em.flush();
			}
		} catch (PersistenceException e) {
			System.out.println ("No se pudo modificar la caracteristica");
		}
		return false;
	}
	
    public List<Caracteristicas> obtenerCaracteristicasPorFenomeno(long idFenomeno)  throws ServiciosException{

    	TypedQuery<Caracteristicas> query = em.createQuery("SELECT c FROM Caracteristicas c WHERE c.idFenomeno.idFenomeno LIKE :idF", Caracteristicas.class)
    			.setParameter("idF", idFenomeno);
		try {
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
    }

	
}
