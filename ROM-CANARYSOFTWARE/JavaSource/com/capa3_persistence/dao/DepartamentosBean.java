package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.DepartamentosBeanRemote;
import com.capa3_persistence.entities.Departamentos;
import com.capa3_persistence.entities.Localidades;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class DepartamentosBean
 */
@Stateless
@LocalBean
public class DepartamentosBean implements DepartamentosBeanRemote {
	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public DepartamentosBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Departamentos buscarCodigo(long idDepartamento) throws ServiciosException {
		try {
			Departamentos departamento = em.find(Departamentos.class, idDepartamento);
			departamento.getIdDepartamento();
			departamento.getNombre();
			departamento.getIdZona();

	    	return departamento;
	    	
	    	}catch (PersistenceException e) {
	    	System.out.println ("No se pudo encontrar el departamento");
	    	}
			return null;
	}
	
	
    public String buscarNombre (String nombre) throws ServiciosException{
    	try {

    		Query query = em.createQuery("SELECT f FROM Departamentos f WHERE f.nombre LIKE :nombre", Departamentos.class)
    				.setParameter("nombre", nombre);
    		return (String) query.getSingleResult();
    		
    	}catch (PersistenceException e) {
    		System.out.println ("No se pudo encontrar el Departamento");
    	}
		return nombre;
    }

    
	public boolean crearDepartamento(Departamentos departamento) throws ServiciosException {
		try {
            em.persist(departamento);
            em.flush();
            return true;
        } catch (PersistenceException e) {
            System.out.println ("No se pudo crear el Departamento" +" "+ e.getMessage());
        }
        return false;
	}
	
	public Departamentos agregarDepartamento(Departamentos departamento) throws PersistenciaException {
		
				try {
					
					Departamentos f = em.merge(departamento);
					em.flush();
					return f;
				} catch (PersistenceException e) {
					throw new PersistenciaException("No se pudo agregar el Departamento. " + e.getMessage(), e);
				}
			}
    
    
    public boolean modificarDepartamento (Departamentos f) throws ServiciosException {
    	try {
    		Departamentos departamento = em.find(Departamentos.class, f.getIdDepartamento());
    		
    		if(departamento!=null) {
	    		departamento.setNombre(f.getNombre());
	    		departamento.setIdZona(f.getIdZona());
	    		em.persist(departamento);
	    		em.flush();
    		}
    	} catch (PersistenceException e) {
    		System.out.println ("No se pudo modificar el Departamento");
    	}
    	return false;
    }
    
    public Departamentos obtenerPorNombre (String nombre){
    	Departamentos departamento = new Departamentos();
    	departamento.getNombre();
    	TypedQuery<Departamentos> query = em.createQuery("SELECT f FROM Departamentos f WHERE f.Nombre LIKE :nombreF", Departamentos.class)
    			.setParameter("nombreF", nombre);
    	System.out.println ("CONSULTA query: " +" "+ query);
		try {
			return query.getResultList().get(0);
		} catch (Exception e) {
			return null;
		}
    	
    }

	
	public List<Departamentos> obtenerTodos() {
		TypedQuery<Departamentos> query = em.createQuery("SELECT f FROM Departamentos f",Departamentos.class);
		return query.getResultList();			
	}
	
	 public List<Departamentos> obtenerTodosPorNombre(String nombre) throws PersistenciaException{
	    	try {
	    		Departamentos departamentos = new Departamentos();
	    		departamentos.getNombre();
		
		    	String query= 	"SELECT f FROM Departamentos f  ";
		    	String queryCriterio="";
		    	queryCriterio = " f.nombre like '%"+nombre +"%' ";
		    	query+=" where "+queryCriterio;
		    	
		    	List<Departamentos> resultList = (List<Departamentos>) em.createQuery(query,Departamentos.class)
						 .getResultList();
		    	return  resultList;
		
		    	
		    	
			} catch (Exception e) {
				throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
			}
	    	
	    }

}
