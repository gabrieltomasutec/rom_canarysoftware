package com.capa3_persistence.dao;

import javax.ejb.Stateless;

import com.capa3_persistence.dao.remote.TipoUsuarioBeanRemote;
import com.capa3_persistence.entities.*;
import com.capa3_persistence.exception.ServiciosException;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Session Bean implementation class TipoUsuario
 */
@Stateless
@LocalBean
public class TipoUsuarioBean implements TipoUsuarioBeanRemote {
    
   @PersistenceContext
   private EntityManager em;
   
    /**
     * Default constructor. 
     * 
     */
    public TipoUsuarioBean() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public TipoUsuario obtenerTipoUsu(long nId) throws ServiciosException {
    	try {	    	
    		TipoUsuario tipoUsu = em.find(TipoUsuario.class, nId);
    		if(tipoUsu!=null) {
    			tipoUsu.getIdTipoUsuario();
	    		tipoUsu.getNombre();
	    		return tipoUsu;
    		}else {
    			return null;
    		}

        	}catch (PersistenceException e) {
        		System.out.println ("No se pudo encontrar el Tipo Usuario");
        	}
    		return null;
    }

    @Override
    public List<TipoUsuario> obtenerTipoUsuarios() throws ServiciosException {
        TypedQuery<TipoUsuario> query =  em.createQuery("SELECT c FROM TipoUsuario c", TipoUsuario.class);
    	return query.getResultList();
    	
    }


}

