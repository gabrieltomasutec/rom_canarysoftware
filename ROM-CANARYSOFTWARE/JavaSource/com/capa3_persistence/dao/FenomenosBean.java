package com.capa3_persistence.dao;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.capa3_persistence.dao.remote.FenomenosBeanRemote;
import com.capa3_persistence.entities.Fenomenos;
import com.capa3_persistence.entities.Usuarios;
import com.capa3_persistence.exception.PersistenciaException;
import com.capa3_persistence.exception.ServiciosException;

/**
 * Session Bean implementation class FenomenosBean
 */
@Stateless
@LocalBean
public class FenomenosBean implements FenomenosBeanRemote {

	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public FenomenosBean() {
        // TODO Auto-generated constructor stub
    }

   // @Override
   /* public void crearFenomeno (String Nombre, String Descripcion, String Telefonos) throws ServiciosException {
    	try {
    		Fenomenos fenomeno = new Fenomenos();
    		fenomeno.setNombre(Nombre);
    		fenomeno.setDescripcion(Descripcion);
    		fenomeno.setTelefono(Telefonos);
    		em.persist(fenomeno);
    		em.flush();
    	} catch (PersistenceException e) {
    		System.out.println ("No se pudo crear el fenomeno" +" "+ e.getMessage());
    	}
    }*/
    
    @Override
    public void borrarFenomeno (Long idFenomeno) throws ServiciosException {
    	try {
    	Fenomenos fenomeno = em.find(Fenomenos.class, idFenomeno);
    	if(fenomeno!=null) {
	    	em.remove(fenomeno);
	    	em.flush();
    	}
    } catch (PersistenceException e) {
    	System.out.println ("No se pudo eliminar el fenomeno");
    }
}
    @Override
    public Fenomenos buscarCodigo (long idFenomeno) throws ServiciosException{
    	try {
	    	
    		return em.find(Fenomenos.class, idFenomeno);

    	}catch (PersistenceException e) {
    	System.out.println ("No se pudo encontrar el fenomeno");
    	}
		return null;
		 
    }
    @Override
    public String buscarNombre (String nombre) throws ServiciosException{
    	try {

    		Query query = em.createQuery("SELECT f FROM Fenomenos f WHERE f.nombre LIKE :nombre", Fenomenos.class)
    				.setParameter("nombre", nombre);
    		return (String) query.getSingleResult();
    		
    	}catch (PersistenceException e) {
    		System.out.println ("No se pudo encontrar el fenomeno");
    	}
		return nombre;
    }

    //@Override
	public boolean crearFenomeno(Fenomenos fenomeno) throws ServiciosException {
		try {
            em.persist(fenomeno);
            em.flush();
            return true;
        } catch (PersistenceException e) {
            System.out.println ("No se pudo crear el fenomeno" +" "+ e.getMessage());
        }
        return false;
	}
	
	public Fenomenos agregarFenomeno(Fenomenos fenomeno) throws PersistenciaException {
		
				try {
					
					Fenomenos f = em.merge(fenomeno);
					em.flush();
					return f;
				} catch (PersistenceException e) {
					throw new PersistenciaException("No se pudo agregar el Fenomeno. ", e);
				}
			}
    
    //  @Override
    public boolean modificarFenomeno (Fenomenos f) throws ServiciosException {
    	try {
    		Fenomenos fenomeno = em.find(Fenomenos.class, f.getIdFenomeno());
    		
    		if(fenomeno!=null) {
	    		fenomeno.setNombre(f.getNombre());
	    		fenomeno.setDescripcion(f.getDescripcion());
	    		fenomeno.setTelefono(f.getTelefono());
	    		em.persist(fenomeno);
	    		em.flush();
    		}
    	} catch (PersistenceException e) {
    		System.out.println ("No se pudo modificar el fenomeno");
    	}
    	return false;
    }
    @Override
    public Fenomenos obtenerPorNombre (String nombre){
    	Fenomenos fenomeno = new Fenomenos();
    	fenomeno.getNombre();
    	TypedQuery<Fenomenos> query = em.createQuery("SELECT f FROM Fenomenos f WHERE f.Nombre LIKE :nombreF", Fenomenos.class)
    			.setParameter("nombreF", nombre);
    	System.out.println ("CONSULTA query: " +" "+ query);
		try {
			return query.getResultList().get(0);
		} catch (Exception e) {
			return null;
		}
    	
    }

	@Override
	public List<Fenomenos> obtenerTodos() {
		TypedQuery<Fenomenos> query = em.createQuery("SELECT f FROM Fenomenos f",Fenomenos.class);
		return query.getResultList();			
	}
	
    public List<Fenomenos> obtenerTodosPorNombre(String nombre) throws PersistenciaException{
    	try {
	    	Fenomenos fenomeno = new Fenomenos();
	    	fenomeno.getNombre();
	//    	TypedQuery<Fenomenos> query = em.createQuery("SELECT f FROM Fenomenos f WHERE f.nombre LIKE :nombreF", Fenomenos.class)
	//    			.setParameter("nombreF", "'%"+nombre+"%'");
	    	String query= 	"SELECT f FROM Fenomenos f  ";
	    	String queryCriterio="";
	    	queryCriterio = " f.nombre like '%"+nombre +"%' ";
	    	query+=" where "+queryCriterio;
	    	
	    	List<Fenomenos> resultList = (List<Fenomenos>) em.createQuery(query,Fenomenos.class)
					 .getResultList();
	    	return  resultList;
	
	    	//ypedQuery<Fenomenos> query = em.createQuery("SELECT f FROM Fenomenos f WHERE f.nombre LIKE : '%"+nombre +"%'", Fenomenos.class);
	    	
		} catch (Exception e) {
			throw new PersistenciaException("No se pudo hacer la consulta." + e.getMessage(),e);
		}
    	
    }
	
}