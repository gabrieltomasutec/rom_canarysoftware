package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Estado
 *
 */
@Entity
@Table (name="ESTADO",indexes = {@Index(name = "name_estado", columnList = "NOMBRE",unique = true)})
public class Estado implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_ESTADO")
	private long idEstado;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	public Estado() {
		super();
	}

	public long getIdEstado() {
		return idEstado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdEstado(long idEstado) {
		this.idEstado = idEstado;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
