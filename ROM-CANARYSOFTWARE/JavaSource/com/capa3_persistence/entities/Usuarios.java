package com.capa3_persistence.entities;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Usuarios
 *
 */
@Entity
@Table (name="USUARIOS",indexes = {@Index(name = "name_Usuario", columnList = "NOMBRE_USUARIO",unique = true),
									@Index(name = "doc_Usuario", columnList = "ID_TIPO_DOC,NUM_DOC",unique = true)})
public class Usuarios implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public static String desencriptarContrasena(String psw) {
				
		char array_des[] = psw.toCharArray();
		
		for( int i=0 ; i<array_des.length;i++	) {	
			array_des[i] = (char)(array_des[i]-(char)5);
		}
		
		String desencriptado = String.valueOf(array_des);
		
		return desencriptado;
		
	}
	
	public static String encriptarContrasena(String psw) {
		
		char array[] = psw.toCharArray();
		
		for( int i=0 ; i<array.length;i++	) {
			array[i] = (char)(array[i]+(char)5);
		}
		String encriptado = String.valueOf(array);
		
		return encriptado;
		
	}
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_USUARIO")
	private long idUsuario;
	
	//@Column (name="NOMBRE_USUARIO",nullable = false, length = 150)
	@Column (name="NOMBRE_USUARIO",nullable = false)
	private String nombreUsuario;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	
	@Column (name="APELLIDO",nullable = false)
	private String apellido;
	
	
	@ManyToOne
	@JoinColumn(name="ID_ESTADO",nullable = false)
	private Estado estado;
	
	@ManyToOne
	@JoinColumn(name="ID_TIPO_DOC",nullable = false)
	private TipoDoc tipoDoc;
	
	@Column (name="NUM_DOC",nullable = false)
	private String numDoc;
	
	@Column (name="DIRECCION",nullable = false)
	private String direccion;
	
	@Column (name="CORREO",nullable = false)
	private String correo;
	
	@Column (name="PASSWORD",nullable = false)
	private String password;

	@ManyToOne
	@JoinColumn(name="ID_TIPO_USUARIO",nullable = false)
	private TipoUsuario tipoUsuario;

	public Usuarios() {
		super();
	}

	public String getApellido() {
		return apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public Estado getEstado() {
		return estado;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public String getNumDoc() {
		return numDoc;
	}

	public String getPassword() {
		return password;
	}

	public TipoDoc getTipoDoc() {
		return tipoDoc;
	}

	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public void setNumDoc(String numDoc) {
		this.numDoc = numDoc;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public void setTipoDoc(TipoDoc tipoDoc) {
		this.tipoDoc = tipoDoc;
	}	
	
	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	@Override
	public String toString() {
		return "Usuarios [idUsuario=" + idUsuario + ", nombreUsuario=" + nombreUsuario + ", nombre=" + nombre
				+ ", apellido=" + apellido + ", estado=" + estado + ", tipoDoc=" + tipoDoc + ", numDoc=" + numDoc
				+ ", direccion=" + direccion + ", correo=" + correo + ", password=" + password + ", tipoUsuario="
				+ tipoUsuario + "]";
	}
   
	
	
}
