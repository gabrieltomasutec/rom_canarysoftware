package com.capa3_persistence.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table (name="CARACTERISTICAS",indexes = {@Index(name = "name_caract", columnList = "NOMBRE",unique = true)})
public class Caracteristicas implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="ID_CARACTERISTICA")
	private long idCaracteristica;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	@Column (name="ETIQUETA",nullable = false)
	private String etiqueta;
	
	@ManyToOne
	@JoinColumn(name="ID_TIPO_CARACT",nullable = false)
	private TipoCaract idTipoCaract;
		
	@ManyToOne
	@JoinColumn(name="ID_FENOMENO",nullable = false)
	private Fenomenos idFenomeno;
	
//	@ManyToMany
//	Set<Observaciones> observaciones;
		
	public Caracteristicas() {
		super();
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public long getIdCaracteristica() {
		return idCaracteristica;
	}

	public Fenomenos getIdFenomeno() {
		return idFenomeno;
	}

	public TipoCaract getIdTipoCaract() {
		return idTipoCaract;
	}

	public String getNombre() {
		return nombre;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public void setIdCaracteristica(long idCaracteristica) {
		this.idCaracteristica = idCaracteristica;
	}

	public void setIdFenomeno(Fenomenos idFenomeno) {
		this.idFenomeno = idFenomeno;
	}

	public void setIdTipoCaract(TipoCaract idTipoCaract) {
		this.idTipoCaract = idTipoCaract;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Caracteristicas [idCaracteristica=" + idCaracteristica + ", nombre=" + nombre + ", etiqueta=" + etiqueta
				+ ", idTipoCaract=" + idTipoCaract + ", idFenomeno=" + idFenomeno + "]";
	}
	
	
	
	
   
}
