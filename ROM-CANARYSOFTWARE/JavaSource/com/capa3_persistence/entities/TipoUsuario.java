package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: TipoUsuario
 *
 */
@Entity
@Table (name="TIPO_USUARIO",indexes = {@Index(name = "name_tipoUsu", columnList = "NOMBRE",unique = true)})
public class TipoUsuario implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_TIPO_USUARIO")
	private long idTipoUsuario;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	public TipoUsuario() {
		super();
	}

	public long getIdTipoUsuario() {
		return idTipoUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdTipoUsuario(long idTipoUsuario) {
		this.idTipoUsuario = idTipoUsuario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
