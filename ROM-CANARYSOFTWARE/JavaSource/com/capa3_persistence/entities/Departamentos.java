package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Departamentos
 *
 */
@Entity
@Table (name="DEPARTAMENTOS",indexes = {@Index(name = "name_depto", columnList = "NOMBRE",unique = true)})
public class Departamentos implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_DEPARTAMENTO")
	private long idDepartamento;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	
	@ManyToOne
	@JoinColumn(name="ID_ZONA",nullable = false)
	private Zonas idZona;
	
	
	public Departamentos() {
		super();
	}

	public long getIdDepartamento() {
		return idDepartamento;
	}

	public Zonas getIdZona() {
		return idZona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdDepartamento(long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public void setIdZona(Zonas idZona) {
		this.idZona = idZona;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
