package com.capa3_persistence.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.*;



/**
 * Entity implementation class for Entity: Observaciones
 *
 */
@Entity
@Table (name="OBSERVACIONES")
public class Observaciones implements Serializable {

	
	private static final long serialVersionUID = 1L;


   
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_OBS")
	private long idObservacion;



	@Column (name="DESCRIPCION",nullable = false)
	private String descripcion;
	
	@Column (name="LATITUD",nullable = false)
	private String latitud;
	
	@Column (name="LONGITUD",nullable = false)
	private String longitud;
	
	@Column (name="ALTITUD",nullable = false)
	private long altitud;
	
	@Column (name="FECHA_HORA")
	private Date fechaHora;
	
	@Column (name="VALID_FECHA")
	private Date validFecha;
	
	@Column (name="VALID_COMENTS")
	private String validComents;
	
	@ManyToOne
	@JoinColumn(name="ID_CRITICIDAD",nullable = false)
	private Criticidad idCriticidad;
	
	
	@ManyToOne
	@JoinColumn(name="ID_FENOMENO",nullable = false)
	private Fenomenos idFenomeno;
	
	
	@ManyToOne
	@JoinColumn(name="ID_LOCALIDAD",nullable = false)
	private Localidades idLocalidad;
	
	
	@ManyToOne
	@JoinColumn(name="ID_USUARIO_ALTA",nullable = false)
	private Usuarios idUsuarioAlta;
	
	
	@ManyToOne
	@JoinColumn(name="ID_USUARIO_VALI")
	private Usuarios idUsuarioVali;
	
//	@ManyToMany
//	Set<Caracteristicas> caracteristicas;
	
	public Observaciones() {
		super();
	}

	public  Observaciones(String descripcion, String latitud, String longitud, long altitud, Date fechaHora,
			Date validFecha, String validComents, Criticidad idCriticidad, Fenomenos idFenomeno,
			Localidades idLocalidad, Usuarios idUsuarioAlta, Usuarios idUsuarioVali) {
		super();
		this.descripcion = descripcion;
		this.latitud = latitud;
		this.longitud = longitud;
		this.altitud = altitud;
		this.fechaHora = fechaHora;
		this.validFecha = validFecha;
		this.validComents = validComents;
		this.idCriticidad = idCriticidad;
		this.idFenomeno = idFenomeno;
		this.idLocalidad = idLocalidad;
		this.idUsuarioAlta = idUsuarioAlta;
		this.idUsuarioVali = idUsuarioVali;
	}

	public long getAltitud() {
		return altitud;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public Criticidad getIdCriticidad() {
		return idCriticidad;
	}

	public Fenomenos getIdFenomeno() {
		return idFenomeno;
	}

	public Localidades getIdLocalidad() {
		return idLocalidad;
	}

	public long getIdObservacion() {
		return idObservacion;
	}

	public Usuarios getIdUsuarioAlta() {
		return idUsuarioAlta;
	}

	public Usuarios getIdUsuarioVali() {
		return idUsuarioVali;
	}

	public String getLatitud() {
		return latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public String getValidComents() {
		return validComents;
	}

	public Date getValidFecha() {
		return validFecha;
	}

	public void setAltitud(long altitud) {
		this.altitud = altitud;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public void setIdCriticidad(Criticidad idCriticidad) {
		this.idCriticidad = idCriticidad;
	}

	public void setIdFenomeno(Fenomenos idFenomeno) {
		this.idFenomeno = idFenomeno;
	}

	public void setIdLocalidad(Localidades idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public void setIdObservacion(long idObservacion) {
		this.idObservacion = idObservacion;
	}

	public void setIdUsuarioAlta(Usuarios idUsuarioAlta) {
		this.idUsuarioAlta = idUsuarioAlta;
	}

	public void setIdUsuarioVali(Usuarios idUsuarioVali) {
		this.idUsuarioVali = idUsuarioVali;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public void setValidComents(String validComents) {
		this.validComents = validComents;
	}
	

	
	public void setValidFecha(Date validFecha) {
		this.validFecha = validFecha;
	}
	
	@Override
	public String toString() {
		return "Observaciones [idObservacion=" + idObservacion + ", descripcion=" + descripcion + ", latitud=" + latitud
				+ ", longitud=" + longitud + ", altitud=" + altitud + ", fechaHora=" + fechaHora + ", validFecha="
				+ validFecha + ", validComents=" + validComents + ", idCriticidad=" + idCriticidad + ", idFenomeno="
				+ idFenomeno + ", idLocalidad=" + idLocalidad + ", idUsuarioAlta=" + idUsuarioAlta + ", idUsuarioVali="
				+ idUsuarioVali + "]";
	}

	public boolean validarCoordenadas(Observaciones observacion) throws com.capa3_persistence.exception.ServiciosException {
		String longitud = observacion.getLongitud();
		  System.out.println("longitud: " + longitud); 
		String latitud = observacion.getLatitud();
		  System.out.println("latitud: " + latitud); 
		  System.out.println("Indica paso por el que salió: verificar_latitud"); 
		return verificar_latitud(latitud) && verificar_longitud(longitud);
	}


	public boolean verificar_latitud(String latitud) {
		
		String[] lat = latitud.split("°");
		
		//verifico GRADOS
		int grados_lat= Integer.parseInt(lat[0]);
		if(grados_lat < 30 || grados_lat > 34) {
			  System.out.println("9"); 
			return false;
		}
		
		//verifico MINUTOS
		String[] m_lat = lat[1].split("'");
		int minutos_lat= Integer.parseInt(m_lat[0]);
		if(grados_lat == 30 && minutos_lat < 5) {
			  System.out.println("10"); 
			return false;
		}else if (grados_lat == 34 && minutos_lat > 58) {
			System.out.println("11"); 
			return false;
		}else if ( minutos_lat < 0 || minutos_lat > 60) {
			System.out.println("12");
			return false;
		}

		//verifico SEGUNDOS
		String[] s_lat = m_lat[1].split("/");
		int segundos_lat= Integer.parseInt(s_lat[0]);
		if(grados_lat == 30 && minutos_lat == 5 && segundos_lat < 8) {
			System.out.println("13");
			return false;
		}else if (grados_lat == 34 && minutos_lat == 58 && segundos_lat > 27) {
			System.out.println("14");
			return false;
		}else if ( segundos_lat < 0 || segundos_lat > 60) {
			System.out.println("15");
			return false;
		}
		
		String punt_card_long = s_lat[1].trim();
		System.out.println("-"+punt_card_long+"-");
		if(punt_card_long.equals("S")) {
			System.out.println("16");
			return true;
		}
		 
		System.out.println("17");
		return false;
	}

	
	public static boolean verificar_longitud(String longitud) {
		
		String[] lon = longitud.split("°");
		
		//verifico GRADOS
		int grados_long= Integer.parseInt(lon[0]);
		if(grados_long < 53 || grados_long > 58) {
			  System.out.println("1"); 
			return false;
		}
		
		//verifico MINUTOS
		String[] m_long = lon[1].split("'");
		int minutos_long= Integer.parseInt(m_long[0]);
		if(grados_long == 53 && minutos_long < 10) {
			  System.out.println("2"); 
			return false;
		}else if (grados_long == 58 && minutos_long > 26) {
			  System.out.println("3"); 
			return false;
		}else if ( minutos_long < 0 || minutos_long > 60) {
			  System.out.println("4"); 
			return false;
		}

		//verifico SEGUNDOS
		String[] s_long = m_long[1].split("/");
		int segundos_long= Integer.parseInt(s_long[0]);
		if(grados_long == 53 && minutos_long == 10 && segundos_long < 58) {
			  System.out.println("5"); 
			return false;
		}else if (grados_long == 58 && minutos_long == 26 && segundos_long > 01) {
			  System.out.println("6"); 
			return false;
		}else if ( segundos_long < 0 || segundos_long > 60) {
			  System.out.println("7"); 
			return false;
		}
		
		String punt_card_long = s_long[1].trim();
		System.out.println("-"+punt_card_long+"-");
		if(punt_card_long.equals("W")) {
			  System.out.println("8"); 
			return true;
		}
		 
		System.out.println("18");
		return false;
	}
	
	
	   @OneToMany(cascade = CascadeType.ALL, orphanRemoval=true, mappedBy = "observacion",fetch = FetchType.EAGER )
	   private List<ObsCaracteristicas> obsCaracteristica = new ArrayList<ObsCaracteristicas>();

	   public void agregarObsCaracteristica(ObsCaracteristicas cbc) {
	      cbc.setObservacion(this);
	      this.obsCaracteristica.add(cbc);
	      
	   }
	   public void eliminarObsCaracteristica(ObsCaracteristicasPK cbcPk) {
		    Iterator<ObsCaracteristicas> iterator = obsCaracteristica.iterator();
		    while (iterator.hasNext()) {
		    	ObsCaracteristicas obsCaracteristicas = iterator.next();
		        if (obsCaracteristicas.getId().equals(cbcPk)) {
		        	obsCaracteristicas.setObservacion(null);
		            iterator.remove();
		        };
		    }
	   }

	public List<ObsCaracteristicas> getObsCaracteristica() {
		return obsCaracteristica;
	}

	public void setObsCaracteristica(List<ObsCaracteristicas> obsCaracteristica) {
		this.obsCaracteristica = obsCaracteristica;
	}

	   

}
