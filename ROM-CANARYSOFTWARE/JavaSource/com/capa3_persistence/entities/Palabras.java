package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Palabras
 *
 */
@Entity
@Table (name="PALABRAS",indexes = {@Index(name = "name_palabra", columnList = "NOMBRE",unique = true)})
public class Palabras implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_PALABRA")
	private long idPalabra;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name="ID_USUARIO",nullable = false)
	private Usuarios idUsuario;
	
	
	public Palabras() {
		super();
	}

	public long getIdPalabra() {
		return idPalabra;
	}

	public Usuarios getIdUsuario() {
		return idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdPalabra(long idPalabra) {
		this.idPalabra = idPalabra;
	}

	public void setIdUsuario(Usuarios idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
