package com.capa3_persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class ObsCaracteristicasPK implements Serializable {
		    
	@Column (name="ID_OBS")
	private long idObs;	
	@Column (name="ID_CARACTERISTICA")
	private long idCarac;

	public ObsCaracteristicasPK() {
        super();

    }

	public ObsCaracteristicasPK(long idObs, long idCarac) {
        super();
        this.idObs = idObs;
        this.idCarac = idCarac;
    }

	@SuppressWarnings("unused")
	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ObsCaracteristicasPK other = (ObsCaracteristicasPK) obj;
        if (idObs != other.idObs)
            return false;
        if ((Long)idCarac == null) {
            if ((Long)other.idCarac != null)
                return false;
        } else if (!((Long)idCarac).equals(other.idCarac))
            return false;
        return true;
    }

	public long getIdCarac() {
		return idCarac;
	}
	
    public long getIdObs() {
		return idObs;
	}
    
    
//    @Override
//    public int hashCode() {
//        int result = 1;
//        return result;
//    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (((Long)idObs == null) ? 0 : ((Long)idObs).hashCode());
		result = prime * result + (((Long)idCarac == null) ? 0 : ((Long)idCarac).hashCode());
		return result;
	}
    
    public void setIdCarac(long idCarac) {
		this.idCarac = idCarac;
	}
    
    public void setIdObs(long idObs) {
		this.idObs = idObs;
	}
	    
	    
}
