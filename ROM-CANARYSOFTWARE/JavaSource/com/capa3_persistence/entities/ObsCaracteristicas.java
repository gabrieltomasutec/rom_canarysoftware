package com.capa3_persistence.entities;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ObsCaracteristicas
 *
 */
@Entity
@Table (name="OBS_CARACTERISTICAS")
public class ObsCaracteristicas implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
    private ObsCaracteristicasPK id = new ObsCaracteristicasPK();
	
	@MapsId("idObs")
	@ManyToOne
	@JoinColumn(name="ID_OBS",nullable = false)
	private Observaciones observacion;

	@MapsId("idCarac")
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_CARACTERISTICA",nullable=false)
	private Caracteristicas caracteristica;
		
    @Column (name="NUMERICO")
	private long numerico;
	
	@Column (name="TEXTO")
	private String texto;
	
	@Column (name="FECHA_HORA")
	private Date fechaHora;

	public ObsCaracteristicas() {
		super();
	}

	public ObsCaracteristicasPK getId() {
		return id;
	}

	public void setId(ObsCaracteristicasPK obsCaracteristicasPK) {
		this.id = obsCaracteristicasPK;
	}

	public Observaciones getObservacion() {
		return observacion;
	}

	public void setObservacion(Observaciones observacion) {
		this.observacion = observacion;
	}

	public Caracteristicas getCaracteristica() {
		return caracteristica;
	}

	public void setCaracteristica(Caracteristicas caracteristica) {
		this.caracteristica = caracteristica;
	}

	public long getNumerico() {
		return numerico;
	}

	public void setNumerico(long numerico) {
		this.numerico = numerico;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public ObsCaracteristicas(Observaciones observacion, Caracteristicas caracteristica, long numerico, String texto,
			Date fechaHora) {
		super();
		this.observacion = observacion;
		this.caracteristica = caracteristica;
		this.numerico = numerico;
		this.texto = texto;
		this.fechaHora = fechaHora;
	}

	@Override
	public String toString() {
		return "ObsCaracteristicas [id=" + id + ", observacion=" + observacion + ", caracteristica=" + caracteristica
				+ ", numerico=" + numerico + ", texto=" + texto + ", fechaHora=" + fechaHora + "]";
	}

   
}
