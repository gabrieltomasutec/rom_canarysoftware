package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: TipoCaract
 *
 */
@Entity
@Table (name="TIPO_CARACT",indexes = {@Index(name = "name_tipoCaract", columnList = "NOMBRE",unique = true)})
public class TipoCaract implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_TIPO_CARACT")
	private long idTipoCaract;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	public TipoCaract() {
		super();
	}

	public long getIdTipoCaract() {
		return idTipoCaract;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdTipoCaract(long idTipoCaract) {
		this.idTipoCaract = idTipoCaract;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
