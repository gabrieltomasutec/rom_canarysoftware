package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Localidades
 *
 */
@Entity
@Table (name="LOCALIDADES")
public class Localidades implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_LOCALIDAD")
	private long idLocalidad;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name="ID_DEPARTAMENTO",nullable = false)
	private Departamentos idDepartamento;
	
	
	public Localidades() {
		super();
	}

	public Departamentos getIdDepartamento() {
		return idDepartamento;
	}

	public long getIdLocalidad() {
		return idLocalidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdDepartamento(Departamentos idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public void setIdLocalidad(long idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
