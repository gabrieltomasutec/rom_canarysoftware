package com.capa3_persistence.entities;

import java.io.Serializable;


import javax.persistence.*;

/**
 * Entity implementation class for Entity: Fenomenos
 *
 */
@Entity
@Table (name="FENOMENOS",indexes = {@Index(name = "name_fenomeno", columnList = "NOMBRE",unique = true)})
public class Fenomenos implements Serializable {

		
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="ID_FENOMENO")
	private long idFenomeno;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	@Column (name="DESCRIPCION",nullable = false)
	private String descripcion;
	
	@Column (name = "TELEFONO",nullable = false)
	private String telefono;
	
	public Fenomenos() {
		super();
	}

	public String getDescripcion() {
		return descripcion;
	}
	public long getIdFenomeno() {
		return idFenomeno;
	}
	public String getNombre() {
		return nombre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public void setIdFenomeno(long idFenomeno) {
		this.idFenomeno = idFenomeno;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	
	
}
