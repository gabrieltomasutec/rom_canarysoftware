package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Criticidad
 *
 */
@Entity
@Table (name="CRITICIDAD",indexes = {@Index(name = "name_criticidad", columnList = "NOMBRE",unique = true)})
public class Criticidad implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name="ID_CRITICIDAD")
	private long idCriticidad;
	
	@Column (name="NOMBRE",nullable = false)
	private String Nombre;
	
	public Criticidad() {
		super();
	}

	public long getIdCriticidad() {
		return idCriticidad;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setIdCriticidad(long idCriticidad) {
		this.idCriticidad = idCriticidad;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	
   
}
