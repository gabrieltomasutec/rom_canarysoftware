package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: TipoDoc
 *
 */
@Entity
@Table (name="TIPO_DOC",indexes = {@Index(name = "name_tipoDoc", columnList = "NOMBRE",unique = true)})
public class TipoDoc implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_TIPO_DOC")
	private long idTipoDoc;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	public TipoDoc() {
		super();
	}

	public long getIdTipoDoc() {
		return idTipoDoc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdTipoDoc(long idTipoDoc) {
		this.idTipoDoc = idTipoDoc;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
