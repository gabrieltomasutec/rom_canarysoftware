package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Zonas
 *
 */
@Entity
@Table (name="ZONAS",indexes = {@Index(name = "name_zona", columnList = "NOMBRE",unique = true)})
public class Zonas implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_ZONA")
	private long idZona;
	
	@Column (name="NOMBRE",nullable = false)
	private String nombre;
	
	public Zonas() {
		super();
	}

	public long getIdZona() {
		return idZona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setIdZona(long idZona) {
		this.idZona = idZona;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
   
}
