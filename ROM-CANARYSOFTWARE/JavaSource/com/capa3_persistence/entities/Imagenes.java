package com.capa3_persistence.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Blob;

/**
 * Entity implementation class for Entity: Imagenes
 *
 */
@Entity
@Table (name="IMAGENES")
public class Imagenes implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID_IMAGEN")
	private long idImagen;
	
	@Column (name="IMAGEN",nullable = false)
	private Blob imagen;
	
	@ManyToOne
	@JoinColumn(name="ID_OBS",nullable = false)
	private Observaciones idObservacion;
	
	
	public Imagenes() {
		super();
	}

	public long getIdImagen() {
		return idImagen;
	}

	public Observaciones getIdObservacion() {
		return idObservacion;
	}

	public Blob getImagen() {
		return imagen;
	}

	public void setIdImagen(long idImagen) {
		this.idImagen = idImagen;
	}

	public void setIdObservacion(Observaciones idObservacion) {
		this.idObservacion = idObservacion;
	}

	public void setImagen(Blob imagen) {
		this.imagen = imagen;
	}
	
	
   
}
